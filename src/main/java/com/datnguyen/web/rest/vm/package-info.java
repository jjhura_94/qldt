/**
 * View Models used by Spring MVC REST controllers.
 */
package com.datnguyen.web.rest.vm;
