package com.datnguyen.web.rest;

import com.datnguyen.domain.CoursesSubject;
import com.datnguyen.repository.CoursesSubjectRepository;
import com.datnguyen.service.CoursesSubjectService;
import com.datnguyen.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.datnguyen.domain.CoursesSubject}.
 */
@RestController
@RequestMapping("/api")
public class CoursesSubjectResource {

    private final Logger log = LoggerFactory.getLogger(CoursesSubjectResource.class);

    private static final String ENTITY_NAME = "coursesSubject";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CoursesSubjectService coursesSubjectService;

    private final CoursesSubjectRepository coursesSubjectRepository;

    public CoursesSubjectResource(CoursesSubjectService coursesSubjectService, CoursesSubjectRepository coursesSubjectRepository) {
        this.coursesSubjectService = coursesSubjectService;
        this.coursesSubjectRepository = coursesSubjectRepository;
    }

    /**
     * {@code POST  /courses-subjects} : Create a new coursesSubject.
     *
     * @param coursesSubject the coursesSubject to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new coursesSubject, or with status {@code 400 (Bad Request)} if the coursesSubject has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/courses-subjects")
    public ResponseEntity<CoursesSubject> createCoursesSubject(@RequestBody CoursesSubject coursesSubject) throws URISyntaxException {
        log.debug("REST request to save CoursesSubject : {}", coursesSubject);
        if (coursesSubject.getId() != null) {
            throw new BadRequestAlertException("A new coursesSubject cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CoursesSubject result = coursesSubjectService.save(coursesSubject);
        return ResponseEntity
            .created(new URI("/api/courses-subjects/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /courses-subjects/:id} : Updates an existing coursesSubject.
     *
     * @param id the id of the coursesSubject to save.
     * @param coursesSubject the coursesSubject to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated coursesSubject,
     * or with status {@code 400 (Bad Request)} if the coursesSubject is not valid,
     * or with status {@code 500 (Internal Server Error)} if the coursesSubject couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/courses-subjects/{id}")
    public ResponseEntity<CoursesSubject> updateCoursesSubject(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody CoursesSubject coursesSubject
    ) throws URISyntaxException {
        log.debug("REST request to update CoursesSubject : {}, {}", id, coursesSubject);
        if (coursesSubject.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, coursesSubject.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!coursesSubjectRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        CoursesSubject result = coursesSubjectService.save(coursesSubject);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, coursesSubject.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /courses-subjects/:id} : Partial updates given fields of an existing coursesSubject, field will ignore if it is null
     *
     * @param id the id of the coursesSubject to save.
     * @param coursesSubject the coursesSubject to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated coursesSubject,
     * or with status {@code 400 (Bad Request)} if the coursesSubject is not valid,
     * or with status {@code 404 (Not Found)} if the coursesSubject is not found,
     * or with status {@code 500 (Internal Server Error)} if the coursesSubject couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/courses-subjects/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<CoursesSubject> partialUpdateCoursesSubject(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody CoursesSubject coursesSubject
    ) throws URISyntaxException {
        log.debug("REST request to partial update CoursesSubject partially : {}, {}", id, coursesSubject);
        if (coursesSubject.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, coursesSubject.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!coursesSubjectRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<CoursesSubject> result = coursesSubjectService.partialUpdate(coursesSubject);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, coursesSubject.getId().toString())
        );
    }

    /**
     * {@code GET  /courses-subjects} : get all the coursesSubjects.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of coursesSubjects in body.
     */
    @GetMapping("/courses-subjects")
    public ResponseEntity<List<CoursesSubject>> getAllCoursesSubjects(Pageable pageable) {
        log.debug("REST request to get a page of CoursesSubjects");
        Page<CoursesSubject> page = coursesSubjectService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /courses-subjects/:id} : get the "id" coursesSubject.
     *
     * @param id the id of the coursesSubject to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the coursesSubject, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/courses-subjects/{id}")
    public ResponseEntity<CoursesSubject> getCoursesSubject(@PathVariable Long id) {
        log.debug("REST request to get CoursesSubject : {}", id);
        Optional<CoursesSubject> coursesSubject = coursesSubjectService.findOne(id);
        return ResponseUtil.wrapOrNotFound(coursesSubject);
    }

    /**
     * {@code DELETE  /courses-subjects/:id} : delete the "id" coursesSubject.
     *
     * @param id the id of the coursesSubject to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/courses-subjects/{id}")
    public ResponseEntity<Void> deleteCoursesSubject(@PathVariable Long id) {
        log.debug("REST request to delete CoursesSubject : {}", id);
        coursesSubjectService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
