package com.datnguyen.web.rest;

import com.datnguyen.domain.PositionSalary;
import com.datnguyen.repository.PositionSalaryRepository;
import com.datnguyen.service.PositionSalaryService;
import com.datnguyen.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.datnguyen.domain.PositionSalary}.
 */
@RestController
@RequestMapping("/api")
public class PositionSalaryResource {

    private final Logger log = LoggerFactory.getLogger(PositionSalaryResource.class);

    private static final String ENTITY_NAME = "positionSalary";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PositionSalaryService positionSalaryService;

    private final PositionSalaryRepository positionSalaryRepository;

    public PositionSalaryResource(PositionSalaryService positionSalaryService, PositionSalaryRepository positionSalaryRepository) {
        this.positionSalaryService = positionSalaryService;
        this.positionSalaryRepository = positionSalaryRepository;
    }

    /**
     * {@code POST  /position-salaries} : Create a new positionSalary.
     *
     * @param positionSalary the positionSalary to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new positionSalary, or with status {@code 400 (Bad Request)} if the positionSalary has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/position-salaries")
    public ResponseEntity<PositionSalary> createPositionSalary(@RequestBody PositionSalary positionSalary) throws URISyntaxException {
        log.debug("REST request to save PositionSalary : {}", positionSalary);
        if (positionSalary.getId() != null) {
            throw new BadRequestAlertException("A new positionSalary cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PositionSalary result = positionSalaryService.save(positionSalary);
        return ResponseEntity
            .created(new URI("/api/position-salaries/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /position-salaries/:id} : Updates an existing positionSalary.
     *
     * @param id the id of the positionSalary to save.
     * @param positionSalary the positionSalary to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated positionSalary,
     * or with status {@code 400 (Bad Request)} if the positionSalary is not valid,
     * or with status {@code 500 (Internal Server Error)} if the positionSalary couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/position-salaries/{id}")
    public ResponseEntity<PositionSalary> updatePositionSalary(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody PositionSalary positionSalary
    ) throws URISyntaxException {
        log.debug("REST request to update PositionSalary : {}, {}", id, positionSalary);
        if (positionSalary.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, positionSalary.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!positionSalaryRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        PositionSalary result = positionSalaryService.save(positionSalary);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, positionSalary.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /position-salaries/:id} : Partial updates given fields of an existing positionSalary, field will ignore if it is null
     *
     * @param id the id of the positionSalary to save.
     * @param positionSalary the positionSalary to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated positionSalary,
     * or with status {@code 400 (Bad Request)} if the positionSalary is not valid,
     * or with status {@code 404 (Not Found)} if the positionSalary is not found,
     * or with status {@code 500 (Internal Server Error)} if the positionSalary couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/position-salaries/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<PositionSalary> partialUpdatePositionSalary(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody PositionSalary positionSalary
    ) throws URISyntaxException {
        log.debug("REST request to partial update PositionSalary partially : {}, {}", id, positionSalary);
        if (positionSalary.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, positionSalary.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!positionSalaryRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<PositionSalary> result = positionSalaryService.partialUpdate(positionSalary);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, positionSalary.getId().toString())
        );
    }

    /**
     * {@code GET  /position-salaries} : get all the positionSalaries.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of positionSalaries in body.
     */
    @GetMapping("/position-salaries")
    public ResponseEntity<List<PositionSalary>> getAllPositionSalaries(Pageable pageable) {
        log.debug("REST request to get a page of PositionSalaries");
        Page<PositionSalary> page = positionSalaryService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /position-salaries/:id} : get the "id" positionSalary.
     *
     * @param id the id of the positionSalary to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the positionSalary, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/position-salaries/{id}")
    public ResponseEntity<PositionSalary> getPositionSalary(@PathVariable Long id) {
        log.debug("REST request to get PositionSalary : {}", id);
        Optional<PositionSalary> positionSalary = positionSalaryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(positionSalary);
    }

    /**
     * {@code DELETE  /position-salaries/:id} : delete the "id" positionSalary.
     *
     * @param id the id of the positionSalary to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/position-salaries/{id}")
    public ResponseEntity<Void> deletePositionSalary(@PathVariable Long id) {
        log.debug("REST request to delete PositionSalary : {}", id);
        positionSalaryService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
