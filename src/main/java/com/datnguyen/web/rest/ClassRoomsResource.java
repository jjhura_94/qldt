package com.datnguyen.web.rest;

import com.datnguyen.domain.ClassRooms;
import com.datnguyen.repository.ClassRoomsRepository;
import com.datnguyen.service.ClassRoomsService;
import com.datnguyen.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.datnguyen.domain.ClassRooms}.
 */
@RestController
@RequestMapping("/api")
public class ClassRoomsResource {

    private final Logger log = LoggerFactory.getLogger(ClassRoomsResource.class);

    private static final String ENTITY_NAME = "classRooms";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ClassRoomsService classRoomsService;

    private final ClassRoomsRepository classRoomsRepository;

    public ClassRoomsResource(ClassRoomsService classRoomsService, ClassRoomsRepository classRoomsRepository) {
        this.classRoomsService = classRoomsService;
        this.classRoomsRepository = classRoomsRepository;
    }

    /**
     * {@code POST  /class-rooms} : Create a new classRooms.
     *
     * @param classRooms the classRooms to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new classRooms, or with status {@code 400 (Bad Request)} if the classRooms has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/class-rooms")
    public ResponseEntity<ClassRooms> createClassRooms(@RequestBody ClassRooms classRooms) throws URISyntaxException {
        log.debug("REST request to save ClassRooms : {}", classRooms);
        if (classRooms.getId() != null) {
            throw new BadRequestAlertException("A new classRooms cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ClassRooms result = classRoomsService.save(classRooms);
        return ResponseEntity
            .created(new URI("/api/class-rooms/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /class-rooms/:id} : Updates an existing classRooms.
     *
     * @param id the id of the classRooms to save.
     * @param classRooms the classRooms to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated classRooms,
     * or with status {@code 400 (Bad Request)} if the classRooms is not valid,
     * or with status {@code 500 (Internal Server Error)} if the classRooms couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/class-rooms/{id}")
    public ResponseEntity<ClassRooms> updateClassRooms(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ClassRooms classRooms
    ) throws URISyntaxException {
        log.debug("REST request to update ClassRooms : {}, {}", id, classRooms);
        if (classRooms.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, classRooms.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!classRoomsRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ClassRooms result = classRoomsService.save(classRooms);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, classRooms.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /class-rooms/:id} : Partial updates given fields of an existing classRooms, field will ignore if it is null
     *
     * @param id the id of the classRooms to save.
     * @param classRooms the classRooms to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated classRooms,
     * or with status {@code 400 (Bad Request)} if the classRooms is not valid,
     * or with status {@code 404 (Not Found)} if the classRooms is not found,
     * or with status {@code 500 (Internal Server Error)} if the classRooms couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/class-rooms/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<ClassRooms> partialUpdateClassRooms(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ClassRooms classRooms
    ) throws URISyntaxException {
        log.debug("REST request to partial update ClassRooms partially : {}, {}", id, classRooms);
        if (classRooms.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, classRooms.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!classRoomsRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ClassRooms> result = classRoomsService.partialUpdate(classRooms);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, classRooms.getId().toString())
        );
    }

    /**
     * {@code GET  /class-rooms} : get all the classRooms.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of classRooms in body.
     */
    @GetMapping("/class-rooms")
    public ResponseEntity<List<ClassRooms>> getAllClassRooms(Pageable pageable) {
        log.debug("REST request to get a page of ClassRooms");
        Page<ClassRooms> page = classRoomsService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /class-rooms/:id} : get the "id" classRooms.
     *
     * @param id the id of the classRooms to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the classRooms, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/class-rooms/{id}")
    public ResponseEntity<ClassRooms> getClassRooms(@PathVariable Long id) {
        log.debug("REST request to get ClassRooms : {}", id);
        Optional<ClassRooms> classRooms = classRoomsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(classRooms);
    }

    /**
     * {@code DELETE  /class-rooms/:id} : delete the "id" classRooms.
     *
     * @param id the id of the classRooms to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/class-rooms/{id}")
    public ResponseEntity<Void> deleteClassRooms(@PathVariable Long id) {
        log.debug("REST request to delete ClassRooms : {}", id);
        classRoomsService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
