package com.datnguyen.web.rest;

import com.datnguyen.domain.Classes;
import com.datnguyen.repository.ClassesRepository;
import com.datnguyen.service.ClassesService;
import com.datnguyen.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.datnguyen.domain.Classes}.
 */
@RestController
@RequestMapping("/api")
public class ClassesResource {

    private final Logger log = LoggerFactory.getLogger(ClassesResource.class);

    private static final String ENTITY_NAME = "classes";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ClassesService classesService;

    private final ClassesRepository classesRepository;

    public ClassesResource(ClassesService classesService, ClassesRepository classesRepository) {
        this.classesService = classesService;
        this.classesRepository = classesRepository;
    }

    /**
     * {@code POST  /classes} : Create a new classes.
     *
     * @param classes the classes to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new classes, or with status {@code 400 (Bad Request)} if the classes has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/classes")
    public ResponseEntity<Classes> createClasses(@RequestBody Classes classes) throws URISyntaxException {
        log.debug("REST request to save Classes : {}", classes);
        if (classes.getId() != null) {
            throw new BadRequestAlertException("A new classes cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Classes result = classesService.save(classes);
        return ResponseEntity
            .created(new URI("/api/classes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /classes/:id} : Updates an existing classes.
     *
     * @param id the id of the classes to save.
     * @param classes the classes to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated classes,
     * or with status {@code 400 (Bad Request)} if the classes is not valid,
     * or with status {@code 500 (Internal Server Error)} if the classes couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/classes/{id}")
    public ResponseEntity<Classes> updateClasses(@PathVariable(value = "id", required = false) final Long id, @RequestBody Classes classes)
        throws URISyntaxException {
        log.debug("REST request to update Classes : {}, {}", id, classes);
        if (classes.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, classes.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!classesRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Classes result = classesService.save(classes);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, classes.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /classes/:id} : Partial updates given fields of an existing classes, field will ignore if it is null
     *
     * @param id the id of the classes to save.
     * @param classes the classes to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated classes,
     * or with status {@code 400 (Bad Request)} if the classes is not valid,
     * or with status {@code 404 (Not Found)} if the classes is not found,
     * or with status {@code 500 (Internal Server Error)} if the classes couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/classes/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<Classes> partialUpdateClasses(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Classes classes
    ) throws URISyntaxException {
        log.debug("REST request to partial update Classes partially : {}, {}", id, classes);
        if (classes.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, classes.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!classesRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Classes> result = classesService.partialUpdate(classes);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, classes.getId().toString())
        );
    }

    /**
     * {@code GET  /classes} : get all the classes.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of classes in body.
     */
    @GetMapping("/classes")
    public ResponseEntity<List<Classes>> getAllClasses(Pageable pageable) {
        log.debug("REST request to get a page of Classes");
        Page<Classes> page = classesService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /classes/:id} : get the "id" classes.
     *
     * @param id the id of the classes to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the classes, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/classes/{id}")
    public ResponseEntity<Classes> getClasses(@PathVariable Long id) {
        log.debug("REST request to get Classes : {}", id);
        Optional<Classes> classes = classesService.findOne(id);
        return ResponseUtil.wrapOrNotFound(classes);
    }

    /**
     * {@code DELETE  /classes/:id} : delete the "id" classes.
     *
     * @param id the id of the classes to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/classes/{id}")
    public ResponseEntity<Void> deleteClasses(@PathVariable Long id) {
        log.debug("REST request to delete Classes : {}", id);
        classesService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
