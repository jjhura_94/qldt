package com.datnguyen.web.rest;

import com.datnguyen.domain.SubjectResults;
import com.datnguyen.repository.SubjectResultsRepository;
import com.datnguyen.service.SubjectResultsService;
import com.datnguyen.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.datnguyen.domain.SubjectResults}.
 */
@RestController
@RequestMapping("/api")
public class SubjectResultsResource {

    private final Logger log = LoggerFactory.getLogger(SubjectResultsResource.class);

    private static final String ENTITY_NAME = "subjectResults";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SubjectResultsService subjectResultsService;

    private final SubjectResultsRepository subjectResultsRepository;

    public SubjectResultsResource(SubjectResultsService subjectResultsService, SubjectResultsRepository subjectResultsRepository) {
        this.subjectResultsService = subjectResultsService;
        this.subjectResultsRepository = subjectResultsRepository;
    }

    /**
     * {@code POST  /subject-results} : Create a new subjectResults.
     *
     * @param subjectResults the subjectResults to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new subjectResults, or with status {@code 400 (Bad Request)} if the subjectResults has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/subject-results")
    public ResponseEntity<SubjectResults> createSubjectResults(@RequestBody SubjectResults subjectResults) throws URISyntaxException {
        log.debug("REST request to save SubjectResults : {}", subjectResults);
        if (subjectResults.getId() != null) {
            throw new BadRequestAlertException("A new subjectResults cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SubjectResults result = subjectResultsService.save(subjectResults);
        return ResponseEntity
            .created(new URI("/api/subject-results/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /subject-results/:id} : Updates an existing subjectResults.
     *
     * @param id the id of the subjectResults to save.
     * @param subjectResults the subjectResults to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated subjectResults,
     * or with status {@code 400 (Bad Request)} if the subjectResults is not valid,
     * or with status {@code 500 (Internal Server Error)} if the subjectResults couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/subject-results/{id}")
    public ResponseEntity<SubjectResults> updateSubjectResults(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody SubjectResults subjectResults
    ) throws URISyntaxException {
        log.debug("REST request to update SubjectResults : {}, {}", id, subjectResults);
        if (subjectResults.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, subjectResults.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!subjectResultsRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        SubjectResults result = subjectResultsService.save(subjectResults);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, subjectResults.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /subject-results/:id} : Partial updates given fields of an existing subjectResults, field will ignore if it is null
     *
     * @param id the id of the subjectResults to save.
     * @param subjectResults the subjectResults to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated subjectResults,
     * or with status {@code 400 (Bad Request)} if the subjectResults is not valid,
     * or with status {@code 404 (Not Found)} if the subjectResults is not found,
     * or with status {@code 500 (Internal Server Error)} if the subjectResults couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/subject-results/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<SubjectResults> partialUpdateSubjectResults(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody SubjectResults subjectResults
    ) throws URISyntaxException {
        log.debug("REST request to partial update SubjectResults partially : {}, {}", id, subjectResults);
        if (subjectResults.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, subjectResults.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!subjectResultsRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<SubjectResults> result = subjectResultsService.partialUpdate(subjectResults);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, subjectResults.getId().toString())
        );
    }

    /**
     * {@code GET  /subject-results} : get all the subjectResults.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of subjectResults in body.
     */
    @GetMapping("/subject-results")
    public ResponseEntity<List<SubjectResults>> getAllSubjectResults(Pageable pageable) {
        log.debug("REST request to get a page of SubjectResults");
        Page<SubjectResults> page = subjectResultsService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /subject-results/:id} : get the "id" subjectResults.
     *
     * @param id the id of the subjectResults to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the subjectResults, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/subject-results/{id}")
    public ResponseEntity<SubjectResults> getSubjectResults(@PathVariable Long id) {
        log.debug("REST request to get SubjectResults : {}", id);
        Optional<SubjectResults> subjectResults = subjectResultsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(subjectResults);
    }

    /**
     * {@code DELETE  /subject-results/:id} : delete the "id" subjectResults.
     *
     * @param id the id of the subjectResults to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/subject-results/{id}")
    public ResponseEntity<Void> deleteSubjectResults(@PathVariable Long id) {
        log.debug("REST request to delete SubjectResults : {}", id);
        subjectResultsService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
