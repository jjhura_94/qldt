package com.datnguyen.service;

import com.datnguyen.domain.Subjects;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link Subjects}.
 */
public interface SubjectsService {
    /**
     * Save a subjects.
     *
     * @param subjects the entity to save.
     * @return the persisted entity.
     */
    Subjects save(Subjects subjects);

    /**
     * Partially updates a subjects.
     *
     * @param subjects the entity to update partially.
     * @return the persisted entity.
     */
    Optional<Subjects> partialUpdate(Subjects subjects);

    /**
     * Get all the subjects.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Subjects> findAll(Pageable pageable);

    /**
     * Get the "id" subjects.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Subjects> findOne(Long id);

    /**
     * Delete the "id" subjects.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
