package com.datnguyen.service.impl;

import com.datnguyen.domain.Students;
import com.datnguyen.repository.StudentsRepository;
import com.datnguyen.service.StudentsService;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Students}.
 */
@Service
@Transactional
public class StudentsServiceImpl implements StudentsService {

    private final Logger log = LoggerFactory.getLogger(StudentsServiceImpl.class);

    private final StudentsRepository studentsRepository;

    public StudentsServiceImpl(StudentsRepository studentsRepository) {
        this.studentsRepository = studentsRepository;
    }

    @Override
    public Students save(Students students) {
        log.debug("Request to save Students : {}", students);
        return studentsRepository.save(students);
    }

    @Override
    public Optional<Students> partialUpdate(Students students) {
        log.debug("Request to partially update Students : {}", students);

        return studentsRepository
            .findById(students.getId())
            .map(
                existingStudents -> {
                    if (students.getStuCode() != null) {
                        existingStudents.setStuCode(students.getStuCode());
                    }
                    if (students.getStuName() != null) {
                        existingStudents.setStuName(students.getStuName());
                    }
                    if (students.getStuDob() != null) {
                        existingStudents.setStuDob(students.getStuDob());
                    }
                    if (students.getStuEmail() != null) {
                        existingStudents.setStuEmail(students.getStuEmail());
                    }
                    if (students.getStuPhone() != null) {
                        existingStudents.setStuPhone(students.getStuPhone());
                    }
                    if (students.getStuAddress() != null) {
                        existingStudents.setStuAddress(students.getStuAddress());
                    }
                    if (students.getStuGender() != null) {
                        existingStudents.setStuGender(students.getStuGender());
                    }
                    if (students.getStuStatus() != null) {
                        existingStudents.setStuStatus(students.getStuStatus());
                    }
                    if (students.getStuDate() != null) {
                        existingStudents.setStuDate(students.getStuDate());
                    }

                    return existingStudents;
                }
            )
            .map(studentsRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Students> findAll(Pageable pageable) {
        log.debug("Request to get all Students");
        return studentsRepository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Students> findOne(Long id) {
        log.debug("Request to get Students : {}", id);
        return studentsRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Students : {}", id);
        studentsRepository.deleteById(id);
    }
}
