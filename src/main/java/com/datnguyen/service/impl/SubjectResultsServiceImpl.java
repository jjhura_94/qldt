package com.datnguyen.service.impl;

import com.datnguyen.domain.SubjectResults;
import com.datnguyen.repository.SubjectResultsRepository;
import com.datnguyen.service.SubjectResultsService;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link SubjectResults}.
 */
@Service
@Transactional
public class SubjectResultsServiceImpl implements SubjectResultsService {

    private final Logger log = LoggerFactory.getLogger(SubjectResultsServiceImpl.class);

    private final SubjectResultsRepository subjectResultsRepository;

    public SubjectResultsServiceImpl(SubjectResultsRepository subjectResultsRepository) {
        this.subjectResultsRepository = subjectResultsRepository;
    }

    @Override
    public SubjectResults save(SubjectResults subjectResults) {
        log.debug("Request to save SubjectResults : {}", subjectResults);
        return subjectResultsRepository.save(subjectResults);
    }

    @Override
    public Optional<SubjectResults> partialUpdate(SubjectResults subjectResults) {
        log.debug("Request to partially update SubjectResults : {}", subjectResults);

        return subjectResultsRepository
            .findById(subjectResults.getId())
            .map(
                existingSubjectResults -> {
                    if (subjectResults.getIsPass() != null) {
                        existingSubjectResults.setIsPass(subjectResults.getIsPass());
                    }

                    return existingSubjectResults;
                }
            )
            .map(subjectResultsRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<SubjectResults> findAll(Pageable pageable) {
        log.debug("Request to get all SubjectResults");
        return subjectResultsRepository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<SubjectResults> findOne(Long id) {
        log.debug("Request to get SubjectResults : {}", id);
        return subjectResultsRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete SubjectResults : {}", id);
        subjectResultsRepository.deleteById(id);
    }
}
