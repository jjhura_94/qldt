package com.datnguyen.service.impl;

import com.datnguyen.domain.PositionSalary;
import com.datnguyen.repository.PositionSalaryRepository;
import com.datnguyen.service.PositionSalaryService;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link PositionSalary}.
 */
@Service
@Transactional
public class PositionSalaryServiceImpl implements PositionSalaryService {

    private final Logger log = LoggerFactory.getLogger(PositionSalaryServiceImpl.class);

    private final PositionSalaryRepository positionSalaryRepository;

    public PositionSalaryServiceImpl(PositionSalaryRepository positionSalaryRepository) {
        this.positionSalaryRepository = positionSalaryRepository;
    }

    @Override
    public PositionSalary save(PositionSalary positionSalary) {
        log.debug("Request to save PositionSalary : {}", positionSalary);
        return positionSalaryRepository.save(positionSalary);
    }

    @Override
    public Optional<PositionSalary> partialUpdate(PositionSalary positionSalary) {
        log.debug("Request to partially update PositionSalary : {}", positionSalary);

        return positionSalaryRepository
            .findById(positionSalary.getId())
            .map(
                existingPositionSalary -> {
                    if (positionSalary.getPosCode() != null) {
                        existingPositionSalary.setPosCode(positionSalary.getPosCode());
                    }
                    if (positionSalary.getPosName() != null) {
                        existingPositionSalary.setPosName(positionSalary.getPosName());
                    }
                    if (positionSalary.getSalBase() != null) {
                        existingPositionSalary.setSalBase(positionSalary.getSalBase());
                    }
                    if (positionSalary.getSalAllowance() != null) {
                        existingPositionSalary.setSalAllowance(positionSalary.getSalAllowance());
                    }

                    return existingPositionSalary;
                }
            )
            .map(positionSalaryRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<PositionSalary> findAll(Pageable pageable) {
        log.debug("Request to get all PositionSalaries");
        return positionSalaryRepository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<PositionSalary> findOne(Long id) {
        log.debug("Request to get PositionSalary : {}", id);
        return positionSalaryRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete PositionSalary : {}", id);
        positionSalaryRepository.deleteById(id);
    }
}
