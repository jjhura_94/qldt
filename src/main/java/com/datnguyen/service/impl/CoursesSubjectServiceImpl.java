package com.datnguyen.service.impl;

import com.datnguyen.domain.CoursesSubject;
import com.datnguyen.repository.CoursesSubjectRepository;
import com.datnguyen.service.CoursesSubjectService;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link CoursesSubject}.
 */
@Service
@Transactional
public class CoursesSubjectServiceImpl implements CoursesSubjectService {

    private final Logger log = LoggerFactory.getLogger(CoursesSubjectServiceImpl.class);

    private final CoursesSubjectRepository coursesSubjectRepository;

    public CoursesSubjectServiceImpl(CoursesSubjectRepository coursesSubjectRepository) {
        this.coursesSubjectRepository = coursesSubjectRepository;
    }

    @Override
    public CoursesSubject save(CoursesSubject coursesSubject) {
        log.debug("Request to save CoursesSubject : {}", coursesSubject);
        return coursesSubjectRepository.save(coursesSubject);
    }

    @Override
    public Optional<CoursesSubject> partialUpdate(CoursesSubject coursesSubject) {
        log.debug("Request to partially update CoursesSubject : {}", coursesSubject);

        return coursesSubjectRepository
            .findById(coursesSubject.getId())
            .map(
                existingCoursesSubject -> {
                    return existingCoursesSubject;
                }
            )
            .map(coursesSubjectRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<CoursesSubject> findAll(Pageable pageable) {
        log.debug("Request to get all CoursesSubjects");
        return coursesSubjectRepository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<CoursesSubject> findOne(Long id) {
        log.debug("Request to get CoursesSubject : {}", id);
        return coursesSubjectRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete CoursesSubject : {}", id);
        coursesSubjectRepository.deleteById(id);
    }
}
