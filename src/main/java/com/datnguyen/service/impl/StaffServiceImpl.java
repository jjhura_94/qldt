package com.datnguyen.service.impl;

import com.datnguyen.domain.Staff;
import com.datnguyen.repository.StaffRepository;
import com.datnguyen.service.StaffService;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Staff}.
 */
@Service
@Transactional
public class StaffServiceImpl implements StaffService {

    private final Logger log = LoggerFactory.getLogger(StaffServiceImpl.class);

    private final StaffRepository staffRepository;

    public StaffServiceImpl(StaffRepository staffRepository) {
        this.staffRepository = staffRepository;
    }

    @Override
    public Staff save(Staff staff) {
        log.debug("Request to save Staff : {}", staff);
        return staffRepository.save(staff);
    }

    @Override
    public Optional<Staff> partialUpdate(Staff staff) {
        log.debug("Request to partially update Staff : {}", staff);

        return staffRepository
            .findById(staff.getId())
            .map(
                existingStaff -> {
                    if (staff.getStaffCode() != null) {
                        existingStaff.setStaffCode(staff.getStaffCode());
                    }
                    if (staff.getStaffFullname() != null) {
                        existingStaff.setStaffFullname(staff.getStaffFullname());
                    }
                    if (staff.getStaffEmail() != null) {
                        existingStaff.setStaffEmail(staff.getStaffEmail());
                    }
                    if (staff.getStaffPhoneNumber() != null) {
                        existingStaff.setStaffPhoneNumber(staff.getStaffPhoneNumber());
                    }
                    if (staff.getStaffAddress() != null) {
                        existingStaff.setStaffAddress(staff.getStaffAddress());
                    }
                    if (staff.getDateJoin() != null) {
                        existingStaff.setDateJoin(staff.getDateJoin());
                    }

                    return existingStaff;
                }
            )
            .map(staffRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Staff> findAll(Pageable pageable) {
        log.debug("Request to get all Staff");
        return staffRepository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Staff> findOne(Long id) {
        log.debug("Request to get Staff : {}", id);
        Optional<Staff> staff = staffRepository.findById(id);

        if (staff.isPresent()) {
            Staff s = staff.get();
            if ("MANAGER".equals(staff.get().getStaffCode()) || "STAFF".equals(staff.get().getStaffCode())) {
                s.setSalary(staffRepository.getSalaryManager(staff.get().getId()));
                return Optional.of(s);
            }
        }
        return staff;
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Staff : {}", id);
        staffRepository.deleteById(id);
    }
}
