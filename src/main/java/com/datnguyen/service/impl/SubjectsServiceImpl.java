package com.datnguyen.service.impl;

import com.datnguyen.domain.Subjects;
import com.datnguyen.repository.SubjectsRepository;
import com.datnguyen.service.SubjectsService;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Subjects}.
 */
@Service
@Transactional
public class SubjectsServiceImpl implements SubjectsService {

    private final Logger log = LoggerFactory.getLogger(SubjectsServiceImpl.class);

    private final SubjectsRepository subjectsRepository;

    public SubjectsServiceImpl(SubjectsRepository subjectsRepository) {
        this.subjectsRepository = subjectsRepository;
    }

    @Override
    public Subjects save(Subjects subjects) {
        log.debug("Request to save Subjects : {}", subjects);
        return subjectsRepository.save(subjects);
    }

    @Override
    public Optional<Subjects> partialUpdate(Subjects subjects) {
        log.debug("Request to partially update Subjects : {}", subjects);

        return subjectsRepository
            .findById(subjects.getId())
            .map(
                existingSubjects -> {
                    if (subjects.getSubName() != null) {
                        existingSubjects.setSubName(subjects.getSubName());
                    }
                    if (subjects.getSubTotalHours() != null) {
                        existingSubjects.setSubTotalHours(subjects.getSubTotalHours());
                    }
                    if (subjects.getSubScore() != null) {
                        existingSubjects.setSubScore(subjects.getSubScore());
                    }
                    if (subjects.getSubActive() != null) {
                        existingSubjects.setSubActive(subjects.getSubActive());
                    }
                    if (subjects.getSubCode() != null) {
                        existingSubjects.setSubCode(subjects.getSubCode());
                    }
                    if (subjects.getSubDescription() != null) {
                        existingSubjects.setSubDescription(subjects.getSubDescription());
                    }

                    return existingSubjects;
                }
            )
            .map(subjectsRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Subjects> findAll(Pageable pageable) {
        log.debug("Request to get all Subjects");
        return subjectsRepository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Subjects> findOne(Long id) {
        log.debug("Request to get Subjects : {}", id);
        return subjectsRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Subjects : {}", id);
        subjectsRepository.deleteById(id);
    }
}
