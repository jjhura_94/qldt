package com.datnguyen.service.impl;

import com.datnguyen.domain.ClassRooms;
import com.datnguyen.repository.ClassRoomsRepository;
import com.datnguyen.service.ClassRoomsService;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link ClassRooms}.
 */
@Service
@Transactional
public class ClassRoomsServiceImpl implements ClassRoomsService {

    private final Logger log = LoggerFactory.getLogger(ClassRoomsServiceImpl.class);

    private final ClassRoomsRepository classRoomsRepository;

    public ClassRoomsServiceImpl(ClassRoomsRepository classRoomsRepository) {
        this.classRoomsRepository = classRoomsRepository;
    }

    @Override
    public ClassRooms save(ClassRooms classRooms) {
        log.debug("Request to save ClassRooms : {}", classRooms);
        return classRoomsRepository.save(classRooms);
    }

    @Override
    public Optional<ClassRooms> partialUpdate(ClassRooms classRooms) {
        log.debug("Request to partially update ClassRooms : {}", classRooms);

        return classRoomsRepository
            .findById(classRooms.getId())
            .map(
                existingClassRooms -> {
                    if (classRooms.getRoomName() != null) {
                        existingClassRooms.setRoomName(classRooms.getRoomName());
                    }
                    if (classRooms.getRoomAddress() != null) {
                        existingClassRooms.setRoomAddress(classRooms.getRoomAddress());
                    }
                    if (classRooms.getTotalSeats() != null) {
                        existingClassRooms.setTotalSeats(classRooms.getTotalSeats());
                    }
                    if (classRooms.getRoomDescription() != null) {
                        existingClassRooms.setRoomDescription(classRooms.getRoomDescription());
                    }

                    return existingClassRooms;
                }
            )
            .map(classRoomsRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ClassRooms> findAll(Pageable pageable) {
        log.debug("Request to get all ClassRooms");
        return classRoomsRepository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ClassRooms> findOne(Long id) {
        log.debug("Request to get ClassRooms : {}", id);
        return classRoomsRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete ClassRooms : {}", id);
        classRoomsRepository.deleteById(id);
    }
}
