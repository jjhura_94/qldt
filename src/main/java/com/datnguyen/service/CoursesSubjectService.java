package com.datnguyen.service;

import com.datnguyen.domain.CoursesSubject;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link CoursesSubject}.
 */
public interface CoursesSubjectService {
    /**
     * Save a coursesSubject.
     *
     * @param coursesSubject the entity to save.
     * @return the persisted entity.
     */
    CoursesSubject save(CoursesSubject coursesSubject);

    /**
     * Partially updates a coursesSubject.
     *
     * @param coursesSubject the entity to update partially.
     * @return the persisted entity.
     */
    Optional<CoursesSubject> partialUpdate(CoursesSubject coursesSubject);

    /**
     * Get all the coursesSubjects.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CoursesSubject> findAll(Pageable pageable);

    /**
     * Get the "id" coursesSubject.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CoursesSubject> findOne(Long id);

    /**
     * Delete the "id" coursesSubject.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
