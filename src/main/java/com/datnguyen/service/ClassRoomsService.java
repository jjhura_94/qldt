package com.datnguyen.service;

import com.datnguyen.domain.ClassRooms;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link ClassRooms}.
 */
public interface ClassRoomsService {
    /**
     * Save a classRooms.
     *
     * @param classRooms the entity to save.
     * @return the persisted entity.
     */
    ClassRooms save(ClassRooms classRooms);

    /**
     * Partially updates a classRooms.
     *
     * @param classRooms the entity to update partially.
     * @return the persisted entity.
     */
    Optional<ClassRooms> partialUpdate(ClassRooms classRooms);

    /**
     * Get all the classRooms.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ClassRooms> findAll(Pageable pageable);

    /**
     * Get the "id" classRooms.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ClassRooms> findOne(Long id);

    /**
     * Delete the "id" classRooms.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
