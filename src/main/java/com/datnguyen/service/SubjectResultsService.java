package com.datnguyen.service;

import com.datnguyen.domain.SubjectResults;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link SubjectResults}.
 */
public interface SubjectResultsService {
    /**
     * Save a subjectResults.
     *
     * @param subjectResults the entity to save.
     * @return the persisted entity.
     */
    SubjectResults save(SubjectResults subjectResults);

    /**
     * Partially updates a subjectResults.
     *
     * @param subjectResults the entity to update partially.
     * @return the persisted entity.
     */
    Optional<SubjectResults> partialUpdate(SubjectResults subjectResults);

    /**
     * Get all the subjectResults.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<SubjectResults> findAll(Pageable pageable);

    /**
     * Get the "id" subjectResults.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<SubjectResults> findOne(Long id);

    /**
     * Delete the "id" subjectResults.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
