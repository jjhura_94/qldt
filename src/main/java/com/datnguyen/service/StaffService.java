package com.datnguyen.service;

import com.datnguyen.domain.Staff;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link Staff}.
 */
public interface StaffService {
    /**
     * Save a staff.
     *
     * @param staff the entity to save.
     * @return the persisted entity.
     */
    Staff save(Staff staff);

    /**
     * Partially updates a staff.
     *
     * @param staff the entity to update partially.
     * @return the persisted entity.
     */
    Optional<Staff> partialUpdate(Staff staff);

    /**
     * Get all the staff.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Staff> findAll(Pageable pageable);

    /**
     * Get the "id" staff.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Staff> findOne(Long id);

    /**
     * Delete the "id" staff.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
