package com.datnguyen.service;

import com.datnguyen.domain.PositionSalary;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link PositionSalary}.
 */
public interface PositionSalaryService {
    /**
     * Save a positionSalary.
     *
     * @param positionSalary the entity to save.
     * @return the persisted entity.
     */
    PositionSalary save(PositionSalary positionSalary);

    /**
     * Partially updates a positionSalary.
     *
     * @param positionSalary the entity to update partially.
     * @return the persisted entity.
     */
    Optional<PositionSalary> partialUpdate(PositionSalary positionSalary);

    /**
     * Get all the positionSalaries.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PositionSalary> findAll(Pageable pageable);

    /**
     * Get the "id" positionSalary.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PositionSalary> findOne(Long id);

    /**
     * Delete the "id" positionSalary.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
