package com.datnguyen.repository;

import com.datnguyen.domain.CoursesSubject;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the CoursesSubject entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CoursesSubjectRepository extends JpaRepository<CoursesSubject, Long> {}
