package com.datnguyen.repository;

import com.datnguyen.domain.Staff;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Staff entity.
 */
@SuppressWarnings("unused")
@Repository
public interface StaffRepository extends JpaRepository<Staff, Long>, CrudRepository<Staff, Long> {
    @Query(
        value = "select ps.sal_base + ((ps.sal_base * (select count(*) from  staff s1 where s1.manager_id = s.id )*5)/100) as salary from staff s join position_salary ps on s.pos_id  = ps.id " +
        "where (staff_code  = 'MANAGER' or staff_code  = 'STAFF') and s.id = :id ",
        nativeQuery = true
    )
    public Double getSalaryManager(@Param("id") Long id);

    @Query(
        value = "select count(*)*2*300000 as salary from classes c join staff s on c.teacher_id  = s.id where  s.id = 55 and to_char(c.start_date , 'yyyyMM') = :day and s.id = :id ",
        nativeQuery = true
    )
    public Double getSalaryTeacher(@Param("id") Long id, @Param("day") String day);

    @Query(
        value = "select count(*)*300000 as salary from classes c join staff s on c.teacher_id  = s.id where  s.id = 55 and to_char(c.start_date , 'yyyyMM') = :day and s.id = :id ",
        nativeQuery = true
    )
    public Double getSalaryTutors(@Param("id") Long id, @Param("day") String day);
}
