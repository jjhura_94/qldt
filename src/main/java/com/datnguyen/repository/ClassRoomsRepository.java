package com.datnguyen.repository;

import com.datnguyen.domain.ClassRooms;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the ClassRooms entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ClassRoomsRepository extends JpaRepository<ClassRooms, Long> {}
