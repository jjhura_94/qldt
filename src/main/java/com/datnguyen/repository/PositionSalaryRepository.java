package com.datnguyen.repository;

import com.datnguyen.domain.PositionSalary;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the PositionSalary entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PositionSalaryRepository extends JpaRepository<PositionSalary, Long> {}
