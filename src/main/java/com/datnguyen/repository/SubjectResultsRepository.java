package com.datnguyen.repository;

import com.datnguyen.domain.SubjectResults;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the SubjectResults entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SubjectResultsRepository extends JpaRepository<SubjectResults, Long> {}
