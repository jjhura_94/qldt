package com.datnguyen.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;

/**
 * The Exam entity.\n@author A true hipster
 */
@ApiModel(description = "The Exam entity.\n@author A true hipster")
@Entity
@Table(name = "exam")
public class Exam implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * fieldName
     */
    @ApiModelProperty(value = "fieldName")
    @Column(name = "exam_name")
    private String examName;

    @Column(name = "exam_date")
    private Instant examDate;

    @Column(name = "exam_note")
    private String examNote;

    @Column(name = "coefficient")
    private Long coefficient;

    @Column(name = "exam_time")
    private Long examTime;

    @ManyToOne
    @JsonIgnoreProperties(value = { "exams", "course", "sub", "student" }, allowSetters = true)
    private SubjectResults subResult;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Exam id(Long id) {
        this.id = id;
        return this;
    }

    public String getExamName() {
        return this.examName;
    }

    public Exam examName(String examName) {
        this.examName = examName;
        return this;
    }

    public void setExamName(String examName) {
        this.examName = examName;
    }

    public Instant getExamDate() {
        return this.examDate;
    }

    public Exam examDate(Instant examDate) {
        this.examDate = examDate;
        return this;
    }

    public void setExamDate(Instant examDate) {
        this.examDate = examDate;
    }

    public String getExamNote() {
        return this.examNote;
    }

    public Exam examNote(String examNote) {
        this.examNote = examNote;
        return this;
    }

    public void setExamNote(String examNote) {
        this.examNote = examNote;
    }

    public Long getCoefficient() {
        return this.coefficient;
    }

    public Exam coefficient(Long coefficient) {
        this.coefficient = coefficient;
        return this;
    }

    public void setCoefficient(Long coefficient) {
        this.coefficient = coefficient;
    }

    public Long getExamTime() {
        return this.examTime;
    }

    public Exam examTime(Long examTime) {
        this.examTime = examTime;
        return this;
    }

    public void setExamTime(Long examTime) {
        this.examTime = examTime;
    }

    public SubjectResults getSubResult() {
        return this.subResult;
    }

    public Exam subResult(SubjectResults subjectResults) {
        this.setSubResult(subjectResults);
        return this;
    }

    public void setSubResult(SubjectResults subjectResults) {
        this.subResult = subjectResults;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Exam)) {
            return false;
        }
        return id != null && id.equals(((Exam) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Exam{" +
            "id=" + getId() +
            ", examName='" + getExamName() + "'" +
            ", examDate='" + getExamDate() + "'" +
            ", examNote='" + getExamNote() + "'" +
            ", coefficient=" + getCoefficient() +
            ", examTime=" + getExamTime() +
            "}";
    }
}
