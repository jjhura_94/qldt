package com.datnguyen.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * The Staff entity.\n@author A true hipster
 */
@ApiModel(description = "The Staff entity.\n@author A true hipster")
@Entity
@Table(name = "staff")
public class Staff implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * fieldName
     */
    @ApiModelProperty(value = "fieldName")
    @Column(name = "staff_code")
    private String staffCode;

    @Column(name = "staff_fullname")
    private String staffFullname;

    @Column(name = "staff_email")
    private String staffEmail;

    @Column(name = "staff_phone_number")
    private String staffPhoneNumber;

    @Column(name = "staff_address")
    private String staffAddress;

    @Column(name = "date_join")
    private Instant dateJoin;

    @OneToMany(mappedBy = "staffManage")
    @JsonIgnoreProperties(value = { "subjectResults", "classes", "coursesSubjects", "staffManage" }, allowSetters = true)
    private Set<Courses> courses = new HashSet<>();

    @OneToMany(mappedBy = "teacher")
    @JsonIgnoreProperties(value = { "sub", "course", "classroom", "teacher" }, allowSetters = true)
    private Set<Classes> classes = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = { "courses", "classes", "manager", "pos" }, allowSetters = true)
    private Staff manager;

    @ManyToOne
    @JsonIgnoreProperties(value = { "staff" }, allowSetters = true)
    private PositionSalary pos;

    private Double salary;

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Staff id(Long id) {
        this.id = id;
        return this;
    }

    public String getStaffCode() {
        return this.staffCode;
    }

    public Staff staffCode(String staffCode) {
        this.staffCode = staffCode;
        return this;
    }

    public void setStaffCode(String staffCode) {
        this.staffCode = staffCode;
    }

    public String getStaffFullname() {
        return this.staffFullname;
    }

    public Staff staffFullname(String staffFullname) {
        this.staffFullname = staffFullname;
        return this;
    }

    public void setStaffFullname(String staffFullname) {
        this.staffFullname = staffFullname;
    }

    public String getStaffEmail() {
        return this.staffEmail;
    }

    public Staff staffEmail(String staffEmail) {
        this.staffEmail = staffEmail;
        return this;
    }

    public void setStaffEmail(String staffEmail) {
        this.staffEmail = staffEmail;
    }

    public String getStaffPhoneNumber() {
        return this.staffPhoneNumber;
    }

    public Staff staffPhoneNumber(String staffPhoneNumber) {
        this.staffPhoneNumber = staffPhoneNumber;
        return this;
    }

    public void setStaffPhoneNumber(String staffPhoneNumber) {
        this.staffPhoneNumber = staffPhoneNumber;
    }

    public String getStaffAddress() {
        return this.staffAddress;
    }

    public Staff staffAddress(String staffAddress) {
        this.staffAddress = staffAddress;
        return this;
    }

    public void setStaffAddress(String staffAddress) {
        this.staffAddress = staffAddress;
    }

    public Instant getDateJoin() {
        return this.dateJoin;
    }

    public Staff dateJoin(Instant dateJoin) {
        this.dateJoin = dateJoin;
        return this;
    }

    public void setDateJoin(Instant dateJoin) {
        this.dateJoin = dateJoin;
    }

    public Set<Courses> getCourses() {
        return this.courses;
    }

    public Staff courses(Set<Courses> courses) {
        this.setCourses(courses);
        return this;
    }

    public Staff addCourses(Courses courses) {
        this.courses.add(courses);
        courses.setStaffManage(this);
        return this;
    }

    public Staff removeCourses(Courses courses) {
        this.courses.remove(courses);
        courses.setStaffManage(null);
        return this;
    }

    public void setCourses(Set<Courses> courses) {
        if (this.courses != null) {
            this.courses.forEach(i -> i.setStaffManage(null));
        }
        if (courses != null) {
            courses.forEach(i -> i.setStaffManage(this));
        }
        this.courses = courses;
    }

    public Set<Classes> getClasses() {
        return this.classes;
    }

    public Staff classes(Set<Classes> classes) {
        this.setClasses(classes);
        return this;
    }

    public Staff addClasses(Classes classes) {
        this.classes.add(classes);
        classes.setTeacher(this);
        return this;
    }

    public Staff removeClasses(Classes classes) {
        this.classes.remove(classes);
        classes.setTeacher(null);
        return this;
    }

    public void setClasses(Set<Classes> classes) {
        if (this.classes != null) {
            this.classes.forEach(i -> i.setTeacher(null));
        }
        if (classes != null) {
            classes.forEach(i -> i.setTeacher(this));
        }
        this.classes = classes;
    }

    public Staff getManager() {
        return this.manager;
    }

    public Staff manager(Staff staff) {
        this.setManager(staff);
        return this;
    }

    public void setManager(Staff staff) {
        this.manager = staff;
    }

    public PositionSalary getPos() {
        return this.pos;
    }

    public Staff pos(PositionSalary positionSalary) {
        this.setPos(positionSalary);
        return this;
    }

    public void setPos(PositionSalary positionSalary) {
        this.pos = positionSalary;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Staff)) {
            return false;
        }
        return id != null && id.equals(((Staff) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Staff{" +
            "id=" + getId() +
            ", staffCode='" + getStaffCode() + "'" +
            ", staffFullname='" + getStaffFullname() + "'" +
            ", staffEmail='" + getStaffEmail() + "'" +
            ", staffPhoneNumber='" + getStaffPhoneNumber() + "'" +
            ", staffAddress='" + getStaffAddress() + "'" +
            ", dateJoin='" + getDateJoin() + "'" +
            "}";
    }
}
