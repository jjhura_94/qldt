package com.datnguyen.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * The Subjects entity.\n@author A true hipster
 */
@ApiModel(description = "The Subjects entity.\n@author A true hipster")
@Entity
@Table(name = "subjects")
public class Subjects implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * fieldName
     */
    @ApiModelProperty(value = "fieldName")
    @Column(name = "sub_name")
    private String subName;

    @Column(name = "sub_total_hours")
    private Long subTotalHours;

    @Column(name = "sub_score")
    private Long subScore;

    @Column(name = "sub_active")
    private Boolean subActive;

    @Column(name = "sub_code")
    private String subCode;

    @Column(name = "sub_description")
    private String subDescription;

    @OneToMany(mappedBy = "sub")
    @JsonIgnoreProperties(value = { "exams", "course", "sub", "student" }, allowSetters = true)
    private Set<SubjectResults> subjectResults = new HashSet<>();

    @OneToMany(mappedBy = "sub")
    @JsonIgnoreProperties(value = { "sub", "course", "classroom", "teacher" }, allowSetters = true)
    private Set<Classes> classes = new HashSet<>();

    @OneToMany(mappedBy = "sub")
    @JsonIgnoreProperties(value = { "course", "sub" }, allowSetters = true)
    private Set<CoursesSubject> coursesSubjects = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Subjects id(Long id) {
        this.id = id;
        return this;
    }

    public String getSubName() {
        return this.subName;
    }

    public Subjects subName(String subName) {
        this.subName = subName;
        return this;
    }

    public void setSubName(String subName) {
        this.subName = subName;
    }

    public Long getSubTotalHours() {
        return this.subTotalHours;
    }

    public Subjects subTotalHours(Long subTotalHours) {
        this.subTotalHours = subTotalHours;
        return this;
    }

    public void setSubTotalHours(Long subTotalHours) {
        this.subTotalHours = subTotalHours;
    }

    public Long getSubScore() {
        return this.subScore;
    }

    public Subjects subScore(Long subScore) {
        this.subScore = subScore;
        return this;
    }

    public void setSubScore(Long subScore) {
        this.subScore = subScore;
    }

    public Boolean getSubActive() {
        return this.subActive;
    }

    public Subjects subActive(Boolean subActive) {
        this.subActive = subActive;
        return this;
    }

    public void setSubActive(Boolean subActive) {
        this.subActive = subActive;
    }

    public String getSubCode() {
        return this.subCode;
    }

    public Subjects subCode(String subCode) {
        this.subCode = subCode;
        return this;
    }

    public void setSubCode(String subCode) {
        this.subCode = subCode;
    }

    public String getSubDescription() {
        return this.subDescription;
    }

    public Subjects subDescription(String subDescription) {
        this.subDescription = subDescription;
        return this;
    }

    public void setSubDescription(String subDescription) {
        this.subDescription = subDescription;
    }

    public Set<SubjectResults> getSubjectResults() {
        return this.subjectResults;
    }

    public Subjects subjectResults(Set<SubjectResults> subjectResults) {
        this.setSubjectResults(subjectResults);
        return this;
    }

    public Subjects addSubjectResults(SubjectResults subjectResults) {
        this.subjectResults.add(subjectResults);
        subjectResults.setSub(this);
        return this;
    }

    public Subjects removeSubjectResults(SubjectResults subjectResults) {
        this.subjectResults.remove(subjectResults);
        subjectResults.setSub(null);
        return this;
    }

    public void setSubjectResults(Set<SubjectResults> subjectResults) {
        if (this.subjectResults != null) {
            this.subjectResults.forEach(i -> i.setSub(null));
        }
        if (subjectResults != null) {
            subjectResults.forEach(i -> i.setSub(this));
        }
        this.subjectResults = subjectResults;
    }

    public Set<Classes> getClasses() {
        return this.classes;
    }

    public Subjects classes(Set<Classes> classes) {
        this.setClasses(classes);
        return this;
    }

    public Subjects addClasses(Classes classes) {
        this.classes.add(classes);
        classes.setSub(this);
        return this;
    }

    public Subjects removeClasses(Classes classes) {
        this.classes.remove(classes);
        classes.setSub(null);
        return this;
    }

    public void setClasses(Set<Classes> classes) {
        if (this.classes != null) {
            this.classes.forEach(i -> i.setSub(null));
        }
        if (classes != null) {
            classes.forEach(i -> i.setSub(this));
        }
        this.classes = classes;
    }

    public Set<CoursesSubject> getCoursesSubjects() {
        return this.coursesSubjects;
    }

    public Subjects coursesSubjects(Set<CoursesSubject> coursesSubjects) {
        this.setCoursesSubjects(coursesSubjects);
        return this;
    }

    public Subjects addCoursesSubject(CoursesSubject coursesSubject) {
        this.coursesSubjects.add(coursesSubject);
        coursesSubject.setSub(this);
        return this;
    }

    public Subjects removeCoursesSubject(CoursesSubject coursesSubject) {
        this.coursesSubjects.remove(coursesSubject);
        coursesSubject.setSub(null);
        return this;
    }

    public void setCoursesSubjects(Set<CoursesSubject> coursesSubjects) {
        if (this.coursesSubjects != null) {
            this.coursesSubjects.forEach(i -> i.setSub(null));
        }
        if (coursesSubjects != null) {
            coursesSubjects.forEach(i -> i.setSub(this));
        }
        this.coursesSubjects = coursesSubjects;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Subjects)) {
            return false;
        }
        return id != null && id.equals(((Subjects) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Subjects{" +
            "id=" + getId() +
            ", subName='" + getSubName() + "'" +
            ", subTotalHours=" + getSubTotalHours() +
            ", subScore=" + getSubScore() +
            ", subActive='" + getSubActive() + "'" +
            ", subCode='" + getSubCode() + "'" +
            ", subDescription='" + getSubDescription() + "'" +
            "}";
    }
}
