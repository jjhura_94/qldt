package com.datnguyen.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * The Students entity.\n@author A true hipster
 */
@ApiModel(description = "The Students entity.\n@author A true hipster")
@Entity
@Table(name = "students")
public class Students implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * fieldName
     */
    @ApiModelProperty(value = "fieldName")
    @Column(name = "stu_code")
    private String stuCode;

    @Column(name = "stu_name")
    private String stuName;

    @Column(name = "stu_dob")
    private Instant stuDob;

    @Column(name = "stu_email")
    private String stuEmail;

    @Column(name = "stu_phone")
    private String stuPhone;

    @Column(name = "stu_address")
    private String stuAddress;

    @Column(name = "stu_gender")
    private String stuGender;

    @Column(name = "stu_status")
    private String stuStatus;

    @Column(name = "stu_date")
    private Instant stuDate;

    @OneToMany(mappedBy = "student")
    @JsonIgnoreProperties(value = { "exams", "course", "sub", "student" }, allowSetters = true)
    private Set<SubjectResults> subjectResults = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Students id(Long id) {
        this.id = id;
        return this;
    }

    public String getStuCode() {
        return this.stuCode;
    }

    public Students stuCode(String stuCode) {
        this.stuCode = stuCode;
        return this;
    }

    public void setStuCode(String stuCode) {
        this.stuCode = stuCode;
    }

    public String getStuName() {
        return this.stuName;
    }

    public Students stuName(String stuName) {
        this.stuName = stuName;
        return this;
    }

    public void setStuName(String stuName) {
        this.stuName = stuName;
    }

    public Instant getStuDob() {
        return this.stuDob;
    }

    public Students stuDob(Instant stuDob) {
        this.stuDob = stuDob;
        return this;
    }

    public void setStuDob(Instant stuDob) {
        this.stuDob = stuDob;
    }

    public String getStuEmail() {
        return this.stuEmail;
    }

    public Students stuEmail(String stuEmail) {
        this.stuEmail = stuEmail;
        return this;
    }

    public void setStuEmail(String stuEmail) {
        this.stuEmail = stuEmail;
    }

    public String getStuPhone() {
        return this.stuPhone;
    }

    public Students stuPhone(String stuPhone) {
        this.stuPhone = stuPhone;
        return this;
    }

    public void setStuPhone(String stuPhone) {
        this.stuPhone = stuPhone;
    }

    public String getStuAddress() {
        return this.stuAddress;
    }

    public Students stuAddress(String stuAddress) {
        this.stuAddress = stuAddress;
        return this;
    }

    public void setStuAddress(String stuAddress) {
        this.stuAddress = stuAddress;
    }

    public String getStuGender() {
        return this.stuGender;
    }

    public Students stuGender(String stuGender) {
        this.stuGender = stuGender;
        return this;
    }

    public void setStuGender(String stuGender) {
        this.stuGender = stuGender;
    }

    public String getStuStatus() {
        return this.stuStatus;
    }

    public Students stuStatus(String stuStatus) {
        this.stuStatus = stuStatus;
        return this;
    }

    public void setStuStatus(String stuStatus) {
        this.stuStatus = stuStatus;
    }

    public Instant getStuDate() {
        return this.stuDate;
    }

    public Students stuDate(Instant stuDate) {
        this.stuDate = stuDate;
        return this;
    }

    public void setStuDate(Instant stuDate) {
        this.stuDate = stuDate;
    }

    public Set<SubjectResults> getSubjectResults() {
        return this.subjectResults;
    }

    public Students subjectResults(Set<SubjectResults> subjectResults) {
        this.setSubjectResults(subjectResults);
        return this;
    }

    public Students addSubjectResults(SubjectResults subjectResults) {
        this.subjectResults.add(subjectResults);
        subjectResults.setStudent(this);
        return this;
    }

    public Students removeSubjectResults(SubjectResults subjectResults) {
        this.subjectResults.remove(subjectResults);
        subjectResults.setStudent(null);
        return this;
    }

    public void setSubjectResults(Set<SubjectResults> subjectResults) {
        if (this.subjectResults != null) {
            this.subjectResults.forEach(i -> i.setStudent(null));
        }
        if (subjectResults != null) {
            subjectResults.forEach(i -> i.setStudent(this));
        }
        this.subjectResults = subjectResults;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Students)) {
            return false;
        }
        return id != null && id.equals(((Students) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Students{" +
            "id=" + getId() +
            ", stuCode='" + getStuCode() + "'" +
            ", stuName='" + getStuName() + "'" +
            ", stuDob='" + getStuDob() + "'" +
            ", stuEmail='" + getStuEmail() + "'" +
            ", stuPhone='" + getStuPhone() + "'" +
            ", stuAddress='" + getStuAddress() + "'" +
            ", stuGender='" + getStuGender() + "'" +
            ", stuStatus='" + getStuStatus() + "'" +
            ", stuDate='" + getStuDate() + "'" +
            "}";
    }
}
