package com.datnguyen.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;

/**
 * The Classes entity.\n@author A true hipster
 */
@ApiModel(description = "The Classes entity.\n@author A true hipster")
@Entity
@Table(name = "classes")
public class Classes implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * fieldName
     */
    @ApiModelProperty(value = "fieldName")
    @Column(name = "class_name")
    private String className;

    @Column(name = "start_date")
    private Instant startDate;

    @Column(name = "end_date")
    private Instant endDate;

    @Column(name = "start_end_time")
    private String startEndTime;

    @ManyToOne
    @JsonIgnoreProperties(value = { "subjectResults", "classes", "coursesSubjects" }, allowSetters = true)
    private Subjects sub;

    @ManyToOne
    @JsonIgnoreProperties(value = { "subjectResults", "classes", "coursesSubjects", "staffManage" }, allowSetters = true)
    private Courses course;

    @ManyToOne
    @JsonIgnoreProperties(value = { "classes" }, allowSetters = true)
    private ClassRooms classroom;

    @ManyToOne
    @JsonIgnoreProperties(value = { "courses", "classes", "manager", "pos" }, allowSetters = true)
    private Staff teacher;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Classes id(Long id) {
        this.id = id;
        return this;
    }

    public String getClassName() {
        return this.className;
    }

    public Classes className(String className) {
        this.className = className;
        return this;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Instant getStartDate() {
        return this.startDate;
    }

    public Classes startDate(Instant startDate) {
        this.startDate = startDate;
        return this;
    }

    public void setStartDate(Instant startDate) {
        this.startDate = startDate;
    }

    public Instant getEndDate() {
        return this.endDate;
    }

    public Classes endDate(Instant endDate) {
        this.endDate = endDate;
        return this;
    }

    public void setEndDate(Instant endDate) {
        this.endDate = endDate;
    }

    public String getStartEndTime() {
        return this.startEndTime;
    }

    public Classes startEndTime(String startEndTime) {
        this.startEndTime = startEndTime;
        return this;
    }

    public void setStartEndTime(String startEndTime) {
        this.startEndTime = startEndTime;
    }

    public Subjects getSub() {
        return this.sub;
    }

    public Classes sub(Subjects subjects) {
        this.setSub(subjects);
        return this;
    }

    public void setSub(Subjects subjects) {
        this.sub = subjects;
    }

    public Courses getCourse() {
        return this.course;
    }

    public Classes course(Courses courses) {
        this.setCourse(courses);
        return this;
    }

    public void setCourse(Courses courses) {
        this.course = courses;
    }

    public ClassRooms getClassroom() {
        return this.classroom;
    }

    public Classes classroom(ClassRooms classRooms) {
        this.setClassroom(classRooms);
        return this;
    }

    public void setClassroom(ClassRooms classRooms) {
        this.classroom = classRooms;
    }

    public Staff getTeacher() {
        return this.teacher;
    }

    public Classes teacher(Staff staff) {
        this.setTeacher(staff);
        return this;
    }

    public void setTeacher(Staff staff) {
        this.teacher = staff;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Classes)) {
            return false;
        }
        return id != null && id.equals(((Classes) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Classes{" +
            "id=" + getId() +
            ", className='" + getClassName() + "'" +
            ", startDate='" + getStartDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            ", startEndTime='" + getStartEndTime() + "'" +
            "}";
    }
}
