package com.datnguyen.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * The PositionSalary entity.\n@author A true hipster
 */
@ApiModel(description = "The PositionSalary entity.\n@author A true hipster")
@Entity
@Table(name = "position_salary")
public class PositionSalary implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * fieldName
     */
    @ApiModelProperty(value = "fieldName")
    @Column(name = "pos_code")
    private String posCode;

    @Column(name = "pos_name")
    private String posName;

    @Column(name = "sal_base")
    private Long salBase;

    @Column(name = "sal_allowance")
    private Long salAllowance;

    @OneToMany(mappedBy = "pos")
    @JsonIgnoreProperties(value = { "courses", "classes", "manager", "pos" }, allowSetters = true)
    private Set<Staff> staff = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PositionSalary id(Long id) {
        this.id = id;
        return this;
    }

    public String getPosCode() {
        return this.posCode;
    }

    public PositionSalary posCode(String posCode) {
        this.posCode = posCode;
        return this;
    }

    public void setPosCode(String posCode) {
        this.posCode = posCode;
    }

    public String getPosName() {
        return this.posName;
    }

    public PositionSalary posName(String posName) {
        this.posName = posName;
        return this;
    }

    public void setPosName(String posName) {
        this.posName = posName;
    }

    public Long getSalBase() {
        return this.salBase;
    }

    public PositionSalary salBase(Long salBase) {
        this.salBase = salBase;
        return this;
    }

    public void setSalBase(Long salBase) {
        this.salBase = salBase;
    }

    public Long getSalAllowance() {
        return this.salAllowance;
    }

    public PositionSalary salAllowance(Long salAllowance) {
        this.salAllowance = salAllowance;
        return this;
    }

    public void setSalAllowance(Long salAllowance) {
        this.salAllowance = salAllowance;
    }

    public Set<Staff> getStaff() {
        return this.staff;
    }

    public PositionSalary staff(Set<Staff> staff) {
        this.setStaff(staff);
        return this;
    }

    public PositionSalary addStaff(Staff staff) {
        this.staff.add(staff);
        staff.setPos(this);
        return this;
    }

    public PositionSalary removeStaff(Staff staff) {
        this.staff.remove(staff);
        staff.setPos(null);
        return this;
    }

    public void setStaff(Set<Staff> staff) {
        if (this.staff != null) {
            this.staff.forEach(i -> i.setPos(null));
        }
        if (staff != null) {
            staff.forEach(i -> i.setPos(this));
        }
        this.staff = staff;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PositionSalary)) {
            return false;
        }
        return id != null && id.equals(((PositionSalary) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PositionSalary{" +
            "id=" + getId() +
            ", posCode='" + getPosCode() + "'" +
            ", posName='" + getPosName() + "'" +
            ", salBase=" + getSalBase() +
            ", salAllowance=" + getSalAllowance() +
            "}";
    }
}
