package com.datnguyen.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * The SubjectResults entity.\n@author A true hipster
 */
@ApiModel(description = "The SubjectResults entity.\n@author A true hipster")
@Entity
@Table(name = "subject_results")
public class SubjectResults implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * fieldName
     */
    @ApiModelProperty(value = "fieldName")
    @Column(name = "is_pass")
    private Boolean isPass;

    @OneToMany(mappedBy = "subResult")
    @JsonIgnoreProperties(value = { "subResult" }, allowSetters = true)
    private Set<Exam> exams = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = { "subjectResults", "classes", "coursesSubjects", "staffManage" }, allowSetters = true)
    private Courses course;

    @ManyToOne
    @JsonIgnoreProperties(value = { "subjectResults", "classes", "coursesSubjects" }, allowSetters = true)
    private Subjects sub;

    @ManyToOne
    @JsonIgnoreProperties(value = { "subjectResults" }, allowSetters = true)
    private Students student;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SubjectResults id(Long id) {
        this.id = id;
        return this;
    }

    public Boolean getIsPass() {
        return this.isPass;
    }

    public SubjectResults isPass(Boolean isPass) {
        this.isPass = isPass;
        return this;
    }

    public void setIsPass(Boolean isPass) {
        this.isPass = isPass;
    }

    public Set<Exam> getExams() {
        return this.exams;
    }

    public SubjectResults exams(Set<Exam> exams) {
        this.setExams(exams);
        return this;
    }

    public SubjectResults addExam(Exam exam) {
        this.exams.add(exam);
        exam.setSubResult(this);
        return this;
    }

    public SubjectResults removeExam(Exam exam) {
        this.exams.remove(exam);
        exam.setSubResult(null);
        return this;
    }

    public void setExams(Set<Exam> exams) {
        if (this.exams != null) {
            this.exams.forEach(i -> i.setSubResult(null));
        }
        if (exams != null) {
            exams.forEach(i -> i.setSubResult(this));
        }
        this.exams = exams;
    }

    public Courses getCourse() {
        return this.course;
    }

    public SubjectResults course(Courses courses) {
        this.setCourse(courses);
        return this;
    }

    public void setCourse(Courses courses) {
        this.course = courses;
    }

    public Subjects getSub() {
        return this.sub;
    }

    public SubjectResults sub(Subjects subjects) {
        this.setSub(subjects);
        return this;
    }

    public void setSub(Subjects subjects) {
        this.sub = subjects;
    }

    public Students getStudent() {
        return this.student;
    }

    public SubjectResults student(Students students) {
        this.setStudent(students);
        return this;
    }

    public void setStudent(Students students) {
        this.student = students;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SubjectResults)) {
            return false;
        }
        return id != null && id.equals(((SubjectResults) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "SubjectResults{" +
            "id=" + getId() +
            ", isPass='" + getIsPass() + "'" +
            "}";
    }
}
