package com.datnguyen.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;

/**
 * A CoursesSubject.
 */
@Entity
@Table(name = "courses_subject")
public class CoursesSubject implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @ManyToOne
    @JsonIgnoreProperties(value = { "subjectResults", "classes", "coursesSubjects", "staffManage" }, allowSetters = true)
    private Courses course;

    @ManyToOne
    @JsonIgnoreProperties(value = { "subjectResults", "classes", "coursesSubjects" }, allowSetters = true)
    private Subjects sub;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CoursesSubject id(Long id) {
        this.id = id;
        return this;
    }

    public Courses getCourse() {
        return this.course;
    }

    public CoursesSubject course(Courses courses) {
        this.setCourse(courses);
        return this;
    }

    public void setCourse(Courses courses) {
        this.course = courses;
    }

    public Subjects getSub() {
        return this.sub;
    }

    public CoursesSubject sub(Subjects subjects) {
        this.setSub(subjects);
        return this;
    }

    public void setSub(Subjects subjects) {
        this.sub = subjects;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CoursesSubject)) {
            return false;
        }
        return id != null && id.equals(((CoursesSubject) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CoursesSubject{" +
            "id=" + getId() +
            "}";
    }
}
