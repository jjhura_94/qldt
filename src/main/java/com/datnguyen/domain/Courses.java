package com.datnguyen.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * The Courses entity.\n@author A true hipster
 */
@ApiModel(description = "The Courses entity.\n@author A true hipster")
@Entity
@Table(name = "courses")
public class Courses implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * fieldName
     */
    @ApiModelProperty(value = "fieldName")
    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "is_active")
    private Boolean isActive;

    @OneToMany(mappedBy = "course")
    @JsonIgnoreProperties(value = { "exams", "course", "sub", "student" }, allowSetters = true)
    private Set<SubjectResults> subjectResults = new HashSet<>();

    @OneToMany(mappedBy = "course")
    @JsonIgnoreProperties(value = { "sub", "course", "classroom", "teacher" }, allowSetters = true)
    private Set<Classes> classes = new HashSet<>();

    @OneToMany(mappedBy = "course")
    @JsonIgnoreProperties(value = { "course", "sub" }, allowSetters = true)
    private Set<CoursesSubject> coursesSubjects = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = { "courses", "classes", "manager", "pos" }, allowSetters = true)
    private Staff staffManage;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Courses id(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return this.name;
    }

    public Courses name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public Courses description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getIsActive() {
        return this.isActive;
    }

    public Courses isActive(Boolean isActive) {
        this.isActive = isActive;
        return this;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Set<SubjectResults> getSubjectResults() {
        return this.subjectResults;
    }

    public Courses subjectResults(Set<SubjectResults> subjectResults) {
        this.setSubjectResults(subjectResults);
        return this;
    }

    public Courses addSubjectResults(SubjectResults subjectResults) {
        this.subjectResults.add(subjectResults);
        subjectResults.setCourse(this);
        return this;
    }

    public Courses removeSubjectResults(SubjectResults subjectResults) {
        this.subjectResults.remove(subjectResults);
        subjectResults.setCourse(null);
        return this;
    }

    public void setSubjectResults(Set<SubjectResults> subjectResults) {
        if (this.subjectResults != null) {
            this.subjectResults.forEach(i -> i.setCourse(null));
        }
        if (subjectResults != null) {
            subjectResults.forEach(i -> i.setCourse(this));
        }
        this.subjectResults = subjectResults;
    }

    public Set<Classes> getClasses() {
        return this.classes;
    }

    public Courses classes(Set<Classes> classes) {
        this.setClasses(classes);
        return this;
    }

    public Courses addClasses(Classes classes) {
        this.classes.add(classes);
        classes.setCourse(this);
        return this;
    }

    public Courses removeClasses(Classes classes) {
        this.classes.remove(classes);
        classes.setCourse(null);
        return this;
    }

    public void setClasses(Set<Classes> classes) {
        if (this.classes != null) {
            this.classes.forEach(i -> i.setCourse(null));
        }
        if (classes != null) {
            classes.forEach(i -> i.setCourse(this));
        }
        this.classes = classes;
    }

    public Set<CoursesSubject> getCoursesSubjects() {
        return this.coursesSubjects;
    }

    public Courses coursesSubjects(Set<CoursesSubject> coursesSubjects) {
        this.setCoursesSubjects(coursesSubjects);
        return this;
    }

    public Courses addCoursesSubject(CoursesSubject coursesSubject) {
        this.coursesSubjects.add(coursesSubject);
        coursesSubject.setCourse(this);
        return this;
    }

    public Courses removeCoursesSubject(CoursesSubject coursesSubject) {
        this.coursesSubjects.remove(coursesSubject);
        coursesSubject.setCourse(null);
        return this;
    }

    public void setCoursesSubjects(Set<CoursesSubject> coursesSubjects) {
        if (this.coursesSubjects != null) {
            this.coursesSubjects.forEach(i -> i.setCourse(null));
        }
        if (coursesSubjects != null) {
            coursesSubjects.forEach(i -> i.setCourse(this));
        }
        this.coursesSubjects = coursesSubjects;
    }

    public Staff getStaffManage() {
        return this.staffManage;
    }

    public Courses staffManage(Staff staff) {
        this.setStaffManage(staff);
        return this;
    }

    public void setStaffManage(Staff staff) {
        this.staffManage = staff;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Courses)) {
            return false;
        }
        return id != null && id.equals(((Courses) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Courses{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", isActive='" + getIsActive() + "'" +
            "}";
    }
}
