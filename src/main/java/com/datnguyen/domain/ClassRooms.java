package com.datnguyen.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * The ClassRooms entity.\n@author A truroomName
 */
@ApiModel(description = "The ClassRooms entity.\n@author A truroomName")
@Entity
@Table(name = "class_rooms")
public class ClassRooms implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * fieldName
     */
    @ApiModelProperty(value = "fieldName")
    @Column(name = "room_name")
    private String roomName;

    @Column(name = "room_address")
    private String roomAddress;

    @Column(name = "total_seats")
    private Long totalSeats;

    @Column(name = "room_description")
    private String roomDescription;

    @OneToMany(mappedBy = "classroom")
    @JsonIgnoreProperties(value = { "sub", "course", "classroom", "teacher" }, allowSetters = true)
    private Set<Classes> classes = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ClassRooms id(Long id) {
        this.id = id;
        return this;
    }

    public String getRoomName() {
        return this.roomName;
    }

    public ClassRooms roomName(String roomName) {
        this.roomName = roomName;
        return this;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getRoomAddress() {
        return this.roomAddress;
    }

    public ClassRooms roomAddress(String roomAddress) {
        this.roomAddress = roomAddress;
        return this;
    }

    public void setRoomAddress(String roomAddress) {
        this.roomAddress = roomAddress;
    }

    public Long getTotalSeats() {
        return this.totalSeats;
    }

    public ClassRooms totalSeats(Long totalSeats) {
        this.totalSeats = totalSeats;
        return this;
    }

    public void setTotalSeats(Long totalSeats) {
        this.totalSeats = totalSeats;
    }

    public String getRoomDescription() {
        return this.roomDescription;
    }

    public ClassRooms roomDescription(String roomDescription) {
        this.roomDescription = roomDescription;
        return this;
    }

    public void setRoomDescription(String roomDescription) {
        this.roomDescription = roomDescription;
    }

    public Set<Classes> getClasses() {
        return this.classes;
    }

    public ClassRooms classes(Set<Classes> classes) {
        this.setClasses(classes);
        return this;
    }

    public ClassRooms addClasses(Classes classes) {
        this.classes.add(classes);
        classes.setClassroom(this);
        return this;
    }

    public ClassRooms removeClasses(Classes classes) {
        this.classes.remove(classes);
        classes.setClassroom(null);
        return this;
    }

    public void setClasses(Set<Classes> classes) {
        if (this.classes != null) {
            this.classes.forEach(i -> i.setClassroom(null));
        }
        if (classes != null) {
            classes.forEach(i -> i.setClassroom(this));
        }
        this.classes = classes;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ClassRooms)) {
            return false;
        }
        return id != null && id.equals(((ClassRooms) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ClassRooms{" +
            "id=" + getId() +
            ", roomName='" + getRoomName() + "'" +
            ", roomAddress='" + getRoomAddress() + "'" +
            ", totalSeats=" + getTotalSeats() +
            ", roomDescription='" + getRoomDescription() + "'" +
            "}";
    }
}
