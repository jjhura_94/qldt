jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { CoursesService } from '../service/courses.service';
import { ICourses, Courses } from '../courses.model';
import { IStaff } from 'app/entities/staff/staff.model';
import { StaffService } from 'app/entities/staff/service/staff.service';

import { CoursesUpdateComponent } from './courses-update.component';

describe('Component Tests', () => {
  describe('Courses Management Update Component', () => {
    let comp: CoursesUpdateComponent;
    let fixture: ComponentFixture<CoursesUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let coursesService: CoursesService;
    let staffService: StaffService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [CoursesUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(CoursesUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CoursesUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      coursesService = TestBed.inject(CoursesService);
      staffService = TestBed.inject(StaffService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call Staff query and add missing value', () => {
        const courses: ICourses = { id: 456 };
        const staffManage: IStaff = { id: 48141 };
        courses.staffManage = staffManage;

        const staffCollection: IStaff[] = [{ id: 33566 }];
        spyOn(staffService, 'query').and.returnValue(of(new HttpResponse({ body: staffCollection })));
        const additionalStaff = [staffManage];
        const expectedCollection: IStaff[] = [...additionalStaff, ...staffCollection];
        spyOn(staffService, 'addStaffToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ courses });
        comp.ngOnInit();

        expect(staffService.query).toHaveBeenCalled();
        expect(staffService.addStaffToCollectionIfMissing).toHaveBeenCalledWith(staffCollection, ...additionalStaff);
        expect(comp.staffSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const courses: ICourses = { id: 456 };
        const staffManage: IStaff = { id: 51416 };
        courses.staffManage = staffManage;

        activatedRoute.data = of({ courses });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(courses));
        expect(comp.staffSharedCollection).toContain(staffManage);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const courses = { id: 123 };
        spyOn(coursesService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ courses });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: courses }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(coursesService.update).toHaveBeenCalledWith(courses);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const courses = new Courses();
        spyOn(coursesService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ courses });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: courses }));
        saveSubject.complete();

        // THEN
        expect(coursesService.create).toHaveBeenCalledWith(courses);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const courses = { id: 123 };
        spyOn(coursesService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ courses });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(coursesService.update).toHaveBeenCalledWith(courses);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackStaffById', () => {
        it('Should return tracked Staff primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackStaffById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});
