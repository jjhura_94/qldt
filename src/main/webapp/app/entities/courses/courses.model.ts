import { ISubjectResults } from 'app/entities/subject-results/subject-results.model';
import { IClasses } from 'app/entities/classes/classes.model';
import { ICoursesSubject } from 'app/entities/courses-subject/courses-subject.model';
import { IStaff } from 'app/entities/staff/staff.model';

export interface ICourses {
  id?: number;
  name?: string | null;
  description?: string | null;
  isActive?: boolean | null;
  subjectResults?: ISubjectResults[] | null;
  classes?: IClasses[] | null;
  coursesSubjects?: ICoursesSubject[] | null;
  staffManage?: IStaff | null;
}

export class Courses implements ICourses {
  constructor(
    public id?: number,
    public name?: string | null,
    public description?: string | null,
    public isActive?: boolean | null,
    public subjectResults?: ISubjectResults[] | null,
    public classes?: IClasses[] | null,
    public coursesSubjects?: ICoursesSubject[] | null,
    public staffManage?: IStaff | null
  ) {
    this.isActive = this.isActive ?? false;
  }
}

export function getCoursesIdentifier(courses: ICourses): number | undefined {
  return courses.id;
}
