import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { SubjectResultsComponent } from '../list/subject-results.component';
import { SubjectResultsDetailComponent } from '../detail/subject-results-detail.component';
import { SubjectResultsUpdateComponent } from '../update/subject-results-update.component';
import { SubjectResultsRoutingResolveService } from './subject-results-routing-resolve.service';

const subjectResultsRoute: Routes = [
  {
    path: '',
    component: SubjectResultsComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: SubjectResultsDetailComponent,
    resolve: {
      subjectResults: SubjectResultsRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: SubjectResultsUpdateComponent,
    resolve: {
      subjectResults: SubjectResultsRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: SubjectResultsUpdateComponent,
    resolve: {
      subjectResults: SubjectResultsRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(subjectResultsRoute)],
  exports: [RouterModule],
})
export class SubjectResultsRoutingModule {}
