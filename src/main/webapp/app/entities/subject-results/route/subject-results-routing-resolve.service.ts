import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ISubjectResults, SubjectResults } from '../subject-results.model';
import { SubjectResultsService } from '../service/subject-results.service';

@Injectable({ providedIn: 'root' })
export class SubjectResultsRoutingResolveService implements Resolve<ISubjectResults> {
  constructor(protected service: SubjectResultsService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ISubjectResults> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((subjectResults: HttpResponse<SubjectResults>) => {
          if (subjectResults.body) {
            return of(subjectResults.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new SubjectResults());
  }
}
