jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { ISubjectResults, SubjectResults } from '../subject-results.model';
import { SubjectResultsService } from '../service/subject-results.service';

import { SubjectResultsRoutingResolveService } from './subject-results-routing-resolve.service';

describe('Service Tests', () => {
  describe('SubjectResults routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: SubjectResultsRoutingResolveService;
    let service: SubjectResultsService;
    let resultSubjectResults: ISubjectResults | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(SubjectResultsRoutingResolveService);
      service = TestBed.inject(SubjectResultsService);
      resultSubjectResults = undefined;
    });

    describe('resolve', () => {
      it('should return ISubjectResults returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultSubjectResults = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultSubjectResults).toEqual({ id: 123 });
      });

      it('should return new ISubjectResults if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultSubjectResults = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultSubjectResults).toEqual(new SubjectResults());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        spyOn(service, 'find').and.returnValue(of(new HttpResponse({ body: null })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultSubjectResults = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultSubjectResults).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
