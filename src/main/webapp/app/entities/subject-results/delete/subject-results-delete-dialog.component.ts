import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ISubjectResults } from '../subject-results.model';
import { SubjectResultsService } from '../service/subject-results.service';

@Component({
  templateUrl: './subject-results-delete-dialog.component.html',
})
export class SubjectResultsDeleteDialogComponent {
  subjectResults?: ISubjectResults;

  constructor(protected subjectResultsService: SubjectResultsService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.subjectResultsService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
