import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISubjectResults } from '../subject-results.model';

@Component({
  selector: 'jhi-subject-results-detail',
  templateUrl: './subject-results-detail.component.html',
})
export class SubjectResultsDetailComponent implements OnInit {
  subjectResults: ISubjectResults | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ subjectResults }) => {
      this.subjectResults = subjectResults;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
