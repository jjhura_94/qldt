import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { SubjectResultsDetailComponent } from './subject-results-detail.component';

describe('Component Tests', () => {
  describe('SubjectResults Management Detail Component', () => {
    let comp: SubjectResultsDetailComponent;
    let fixture: ComponentFixture<SubjectResultsDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [SubjectResultsDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ subjectResults: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(SubjectResultsDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(SubjectResultsDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load subjectResults on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.subjectResults).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
