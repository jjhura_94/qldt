import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ISubjectResults, SubjectResults } from '../subject-results.model';

import { SubjectResultsService } from './subject-results.service';

describe('Service Tests', () => {
  describe('SubjectResults Service', () => {
    let service: SubjectResultsService;
    let httpMock: HttpTestingController;
    let elemDefault: ISubjectResults;
    let expectedResult: ISubjectResults | ISubjectResults[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(SubjectResultsService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 0,
        isPass: false,
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a SubjectResults', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new SubjectResults()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a SubjectResults', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            isPass: true,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a SubjectResults', () => {
        const patchObject = Object.assign({}, new SubjectResults());

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of SubjectResults', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            isPass: true,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a SubjectResults', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addSubjectResultsToCollectionIfMissing', () => {
        it('should add a SubjectResults to an empty array', () => {
          const subjectResults: ISubjectResults = { id: 123 };
          expectedResult = service.addSubjectResultsToCollectionIfMissing([], subjectResults);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(subjectResults);
        });

        it('should not add a SubjectResults to an array that contains it', () => {
          const subjectResults: ISubjectResults = { id: 123 };
          const subjectResultsCollection: ISubjectResults[] = [
            {
              ...subjectResults,
            },
            { id: 456 },
          ];
          expectedResult = service.addSubjectResultsToCollectionIfMissing(subjectResultsCollection, subjectResults);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a SubjectResults to an array that doesn't contain it", () => {
          const subjectResults: ISubjectResults = { id: 123 };
          const subjectResultsCollection: ISubjectResults[] = [{ id: 456 }];
          expectedResult = service.addSubjectResultsToCollectionIfMissing(subjectResultsCollection, subjectResults);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(subjectResults);
        });

        it('should add only unique SubjectResults to an array', () => {
          const subjectResultsArray: ISubjectResults[] = [{ id: 123 }, { id: 456 }, { id: 11805 }];
          const subjectResultsCollection: ISubjectResults[] = [{ id: 123 }];
          expectedResult = service.addSubjectResultsToCollectionIfMissing(subjectResultsCollection, ...subjectResultsArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const subjectResults: ISubjectResults = { id: 123 };
          const subjectResults2: ISubjectResults = { id: 456 };
          expectedResult = service.addSubjectResultsToCollectionIfMissing([], subjectResults, subjectResults2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(subjectResults);
          expect(expectedResult).toContain(subjectResults2);
        });

        it('should accept null and undefined values', () => {
          const subjectResults: ISubjectResults = { id: 123 };
          expectedResult = service.addSubjectResultsToCollectionIfMissing([], null, subjectResults, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(subjectResults);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
