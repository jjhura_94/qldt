import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ISubjectResults, getSubjectResultsIdentifier } from '../subject-results.model';

export type EntityResponseType = HttpResponse<ISubjectResults>;
export type EntityArrayResponseType = HttpResponse<ISubjectResults[]>;

@Injectable({ providedIn: 'root' })
export class SubjectResultsService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/subject-results');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(subjectResults: ISubjectResults): Observable<EntityResponseType> {
    return this.http.post<ISubjectResults>(this.resourceUrl, subjectResults, { observe: 'response' });
  }

  update(subjectResults: ISubjectResults): Observable<EntityResponseType> {
    return this.http.put<ISubjectResults>(`${this.resourceUrl}/${getSubjectResultsIdentifier(subjectResults) as number}`, subjectResults, {
      observe: 'response',
    });
  }

  partialUpdate(subjectResults: ISubjectResults): Observable<EntityResponseType> {
    return this.http.patch<ISubjectResults>(
      `${this.resourceUrl}/${getSubjectResultsIdentifier(subjectResults) as number}`,
      subjectResults,
      { observe: 'response' }
    );
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ISubjectResults>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ISubjectResults[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addSubjectResultsToCollectionIfMissing(
    subjectResultsCollection: ISubjectResults[],
    ...subjectResultsToCheck: (ISubjectResults | null | undefined)[]
  ): ISubjectResults[] {
    const subjectResults: ISubjectResults[] = subjectResultsToCheck.filter(isPresent);
    if (subjectResults.length > 0) {
      const subjectResultsCollectionIdentifiers = subjectResultsCollection.map(
        subjectResultsItem => getSubjectResultsIdentifier(subjectResultsItem)!
      );
      const subjectResultsToAdd = subjectResults.filter(subjectResultsItem => {
        const subjectResultsIdentifier = getSubjectResultsIdentifier(subjectResultsItem);
        if (subjectResultsIdentifier == null || subjectResultsCollectionIdentifiers.includes(subjectResultsIdentifier)) {
          return false;
        }
        subjectResultsCollectionIdentifiers.push(subjectResultsIdentifier);
        return true;
      });
      return [...subjectResultsToAdd, ...subjectResultsCollection];
    }
    return subjectResultsCollection;
  }
}
