import { Component, OnInit } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ISubjectResults } from '../subject-results.model';

import { ITEMS_PER_PAGE } from 'app/config/pagination.constants';
import { SubjectResultsService } from '../service/subject-results.service';
import { SubjectResultsDeleteDialogComponent } from '../delete/subject-results-delete-dialog.component';
import { ParseLinks } from 'app/core/util/parse-links.service';

@Component({
  selector: 'jhi-subject-results',
  templateUrl: './subject-results.component.html',
})
export class SubjectResultsComponent implements OnInit {
  subjectResults: ISubjectResults[];
  isLoading = false;
  itemsPerPage: number;
  links: { [key: string]: number };
  page: number;
  predicate: string;
  ascending: boolean;

  constructor(protected subjectResultsService: SubjectResultsService, protected modalService: NgbModal, protected parseLinks: ParseLinks) {
    this.subjectResults = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0,
    };
    this.predicate = 'id';
    this.ascending = true;
  }

  loadAll(): void {
    this.isLoading = true;

    this.subjectResultsService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .subscribe(
        (res: HttpResponse<ISubjectResults[]>) => {
          this.isLoading = false;
          this.paginateSubjectResults(res.body, res.headers);
        },
        () => {
          this.isLoading = false;
        }
      );
  }

  reset(): void {
    this.page = 0;
    this.subjectResults = [];
    this.loadAll();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: ISubjectResults): number {
    return item.id!;
  }

  delete(subjectResults: ISubjectResults): void {
    const modalRef = this.modalService.open(SubjectResultsDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.subjectResults = subjectResults;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.reset();
      }
    });
  }

  protected sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateSubjectResults(data: ISubjectResults[] | null, headers: HttpHeaders): void {
    this.links = this.parseLinks.parse(headers.get('link') ?? '');
    if (data) {
      for (const d of data) {
        this.subjectResults.push(d);
      }
    }
  }
}
