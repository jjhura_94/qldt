import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { SubjectResultsComponent } from './list/subject-results.component';
import { SubjectResultsDetailComponent } from './detail/subject-results-detail.component';
import { SubjectResultsUpdateComponent } from './update/subject-results-update.component';
import { SubjectResultsDeleteDialogComponent } from './delete/subject-results-delete-dialog.component';
import { SubjectResultsRoutingModule } from './route/subject-results-routing.module';

@NgModule({
  imports: [SharedModule, SubjectResultsRoutingModule],
  declarations: [
    SubjectResultsComponent,
    SubjectResultsDetailComponent,
    SubjectResultsUpdateComponent,
    SubjectResultsDeleteDialogComponent,
  ],
  entryComponents: [SubjectResultsDeleteDialogComponent],
})
export class SubjectResultsModule {}
