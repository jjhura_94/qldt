import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { ISubjectResults, SubjectResults } from '../subject-results.model';
import { SubjectResultsService } from '../service/subject-results.service';
import { ICourses } from 'app/entities/courses/courses.model';
import { CoursesService } from 'app/entities/courses/service/courses.service';
import { ISubjects } from 'app/entities/subjects/subjects.model';
import { SubjectsService } from 'app/entities/subjects/service/subjects.service';
import { IStudents } from 'app/entities/students/students.model';
import { StudentsService } from 'app/entities/students/service/students.service';

@Component({
  selector: 'jhi-subject-results-update',
  templateUrl: './subject-results-update.component.html',
})
export class SubjectResultsUpdateComponent implements OnInit {
  isSaving = false;

  coursesSharedCollection: ICourses[] = [];
  subjectsSharedCollection: ISubjects[] = [];
  studentsSharedCollection: IStudents[] = [];

  editForm = this.fb.group({
    id: [],
    isPass: [],
    course: [],
    sub: [],
    student: [],
  });

  constructor(
    protected subjectResultsService: SubjectResultsService,
    protected coursesService: CoursesService,
    protected subjectsService: SubjectsService,
    protected studentsService: StudentsService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ subjectResults }) => {
      this.updateForm(subjectResults);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const subjectResults = this.createFromForm();
    if (subjectResults.id !== undefined) {
      this.subscribeToSaveResponse(this.subjectResultsService.update(subjectResults));
    } else {
      this.subscribeToSaveResponse(this.subjectResultsService.create(subjectResults));
    }
  }

  trackCoursesById(index: number, item: ICourses): number {
    return item.id!;
  }

  trackSubjectsById(index: number, item: ISubjects): number {
    return item.id!;
  }

  trackStudentsById(index: number, item: IStudents): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISubjectResults>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(subjectResults: ISubjectResults): void {
    this.editForm.patchValue({
      id: subjectResults.id,
      isPass: subjectResults.isPass,
      course: subjectResults.course,
      sub: subjectResults.sub,
      student: subjectResults.student,
    });

    this.coursesSharedCollection = this.coursesService.addCoursesToCollectionIfMissing(this.coursesSharedCollection, subjectResults.course);
    this.subjectsSharedCollection = this.subjectsService.addSubjectsToCollectionIfMissing(
      this.subjectsSharedCollection,
      subjectResults.sub
    );
    this.studentsSharedCollection = this.studentsService.addStudentsToCollectionIfMissing(
      this.studentsSharedCollection,
      subjectResults.student
    );
  }

  protected loadRelationshipsOptions(): void {
    this.coursesService
      .query()
      .pipe(map((res: HttpResponse<ICourses[]>) => res.body ?? []))
      .pipe(map((courses: ICourses[]) => this.coursesService.addCoursesToCollectionIfMissing(courses, this.editForm.get('course')!.value)))
      .subscribe((courses: ICourses[]) => (this.coursesSharedCollection = courses));

    this.subjectsService
      .query()
      .pipe(map((res: HttpResponse<ISubjects[]>) => res.body ?? []))
      .pipe(
        map((subjects: ISubjects[]) => this.subjectsService.addSubjectsToCollectionIfMissing(subjects, this.editForm.get('sub')!.value))
      )
      .subscribe((subjects: ISubjects[]) => (this.subjectsSharedCollection = subjects));

    this.studentsService
      .query()
      .pipe(map((res: HttpResponse<IStudents[]>) => res.body ?? []))
      .pipe(
        map((students: IStudents[]) => this.studentsService.addStudentsToCollectionIfMissing(students, this.editForm.get('student')!.value))
      )
      .subscribe((students: IStudents[]) => (this.studentsSharedCollection = students));
  }

  protected createFromForm(): ISubjectResults {
    return {
      ...new SubjectResults(),
      id: this.editForm.get(['id'])!.value,
      isPass: this.editForm.get(['isPass'])!.value,
      course: this.editForm.get(['course'])!.value,
      sub: this.editForm.get(['sub'])!.value,
      student: this.editForm.get(['student'])!.value,
    };
  }
}
