jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { SubjectResultsService } from '../service/subject-results.service';
import { ISubjectResults, SubjectResults } from '../subject-results.model';
import { ICourses } from 'app/entities/courses/courses.model';
import { CoursesService } from 'app/entities/courses/service/courses.service';
import { ISubjects } from 'app/entities/subjects/subjects.model';
import { SubjectsService } from 'app/entities/subjects/service/subjects.service';
import { IStudents } from 'app/entities/students/students.model';
import { StudentsService } from 'app/entities/students/service/students.service';

import { SubjectResultsUpdateComponent } from './subject-results-update.component';

describe('Component Tests', () => {
  describe('SubjectResults Management Update Component', () => {
    let comp: SubjectResultsUpdateComponent;
    let fixture: ComponentFixture<SubjectResultsUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let subjectResultsService: SubjectResultsService;
    let coursesService: CoursesService;
    let subjectsService: SubjectsService;
    let studentsService: StudentsService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [SubjectResultsUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(SubjectResultsUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(SubjectResultsUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      subjectResultsService = TestBed.inject(SubjectResultsService);
      coursesService = TestBed.inject(CoursesService);
      subjectsService = TestBed.inject(SubjectsService);
      studentsService = TestBed.inject(StudentsService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call Courses query and add missing value', () => {
        const subjectResults: ISubjectResults = { id: 456 };
        const course: ICourses = { id: 27823 };
        subjectResults.course = course;

        const coursesCollection: ICourses[] = [{ id: 50862 }];
        spyOn(coursesService, 'query').and.returnValue(of(new HttpResponse({ body: coursesCollection })));
        const additionalCourses = [course];
        const expectedCollection: ICourses[] = [...additionalCourses, ...coursesCollection];
        spyOn(coursesService, 'addCoursesToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ subjectResults });
        comp.ngOnInit();

        expect(coursesService.query).toHaveBeenCalled();
        expect(coursesService.addCoursesToCollectionIfMissing).toHaveBeenCalledWith(coursesCollection, ...additionalCourses);
        expect(comp.coursesSharedCollection).toEqual(expectedCollection);
      });

      it('Should call Subjects query and add missing value', () => {
        const subjectResults: ISubjectResults = { id: 456 };
        const sub: ISubjects = { id: 24910 };
        subjectResults.sub = sub;

        const subjectsCollection: ISubjects[] = [{ id: 10460 }];
        spyOn(subjectsService, 'query').and.returnValue(of(new HttpResponse({ body: subjectsCollection })));
        const additionalSubjects = [sub];
        const expectedCollection: ISubjects[] = [...additionalSubjects, ...subjectsCollection];
        spyOn(subjectsService, 'addSubjectsToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ subjectResults });
        comp.ngOnInit();

        expect(subjectsService.query).toHaveBeenCalled();
        expect(subjectsService.addSubjectsToCollectionIfMissing).toHaveBeenCalledWith(subjectsCollection, ...additionalSubjects);
        expect(comp.subjectsSharedCollection).toEqual(expectedCollection);
      });

      it('Should call Students query and add missing value', () => {
        const subjectResults: ISubjectResults = { id: 456 };
        const student: IStudents = { id: 45784 };
        subjectResults.student = student;

        const studentsCollection: IStudents[] = [{ id: 41235 }];
        spyOn(studentsService, 'query').and.returnValue(of(new HttpResponse({ body: studentsCollection })));
        const additionalStudents = [student];
        const expectedCollection: IStudents[] = [...additionalStudents, ...studentsCollection];
        spyOn(studentsService, 'addStudentsToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ subjectResults });
        comp.ngOnInit();

        expect(studentsService.query).toHaveBeenCalled();
        expect(studentsService.addStudentsToCollectionIfMissing).toHaveBeenCalledWith(studentsCollection, ...additionalStudents);
        expect(comp.studentsSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const subjectResults: ISubjectResults = { id: 456 };
        const course: ICourses = { id: 16827 };
        subjectResults.course = course;
        const sub: ISubjects = { id: 42803 };
        subjectResults.sub = sub;
        const student: IStudents = { id: 41151 };
        subjectResults.student = student;

        activatedRoute.data = of({ subjectResults });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(subjectResults));
        expect(comp.coursesSharedCollection).toContain(course);
        expect(comp.subjectsSharedCollection).toContain(sub);
        expect(comp.studentsSharedCollection).toContain(student);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const subjectResults = { id: 123 };
        spyOn(subjectResultsService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ subjectResults });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: subjectResults }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(subjectResultsService.update).toHaveBeenCalledWith(subjectResults);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const subjectResults = new SubjectResults();
        spyOn(subjectResultsService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ subjectResults });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: subjectResults }));
        saveSubject.complete();

        // THEN
        expect(subjectResultsService.create).toHaveBeenCalledWith(subjectResults);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const subjectResults = { id: 123 };
        spyOn(subjectResultsService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ subjectResults });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(subjectResultsService.update).toHaveBeenCalledWith(subjectResults);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackCoursesById', () => {
        it('Should return tracked Courses primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackCoursesById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });

      describe('trackSubjectsById', () => {
        it('Should return tracked Subjects primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackSubjectsById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });

      describe('trackStudentsById', () => {
        it('Should return tracked Students primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackStudentsById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});
