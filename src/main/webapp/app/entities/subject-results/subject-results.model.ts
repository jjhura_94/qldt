import { IExam } from 'app/entities/exam/exam.model';
import { ICourses } from 'app/entities/courses/courses.model';
import { ISubjects } from 'app/entities/subjects/subjects.model';
import { IStudents } from 'app/entities/students/students.model';

export interface ISubjectResults {
  id?: number;
  isPass?: boolean | null;
  exams?: IExam[] | null;
  course?: ICourses | null;
  sub?: ISubjects | null;
  student?: IStudents | null;
}

export class SubjectResults implements ISubjectResults {
  constructor(
    public id?: number,
    public isPass?: boolean | null,
    public exams?: IExam[] | null,
    public course?: ICourses | null,
    public sub?: ISubjects | null,
    public student?: IStudents | null
  ) {
    this.isPass = this.isPass ?? false;
  }
}

export function getSubjectResultsIdentifier(subjectResults: ISubjectResults): number | undefined {
  return subjectResults.id;
}
