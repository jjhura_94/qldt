import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ICoursesSubject } from '../courses-subject.model';
import { CoursesSubjectService } from '../service/courses-subject.service';

@Component({
  templateUrl: './courses-subject-delete-dialog.component.html',
})
export class CoursesSubjectDeleteDialogComponent {
  coursesSubject?: ICoursesSubject;

  constructor(protected coursesSubjectService: CoursesSubjectService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.coursesSubjectService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
