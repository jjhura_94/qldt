import { Component, OnInit } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ICoursesSubject } from '../courses-subject.model';

import { ITEMS_PER_PAGE } from 'app/config/pagination.constants';
import { CoursesSubjectService } from '../service/courses-subject.service';
import { CoursesSubjectDeleteDialogComponent } from '../delete/courses-subject-delete-dialog.component';
import { ParseLinks } from 'app/core/util/parse-links.service';

@Component({
  selector: 'jhi-courses-subject',
  templateUrl: './courses-subject.component.html',
})
export class CoursesSubjectComponent implements OnInit {
  coursesSubjects: ICoursesSubject[];
  isLoading = false;
  itemsPerPage: number;
  links: { [key: string]: number };
  page: number;
  predicate: string;
  ascending: boolean;

  constructor(protected coursesSubjectService: CoursesSubjectService, protected modalService: NgbModal, protected parseLinks: ParseLinks) {
    this.coursesSubjects = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0,
    };
    this.predicate = 'id';
    this.ascending = true;
  }

  loadAll(): void {
    this.isLoading = true;

    this.coursesSubjectService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .subscribe(
        (res: HttpResponse<ICoursesSubject[]>) => {
          this.isLoading = false;
          this.paginateCoursesSubjects(res.body, res.headers);
        },
        () => {
          this.isLoading = false;
        }
      );
  }

  reset(): void {
    this.page = 0;
    this.coursesSubjects = [];
    this.loadAll();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: ICoursesSubject): number {
    return item.id!;
  }

  delete(coursesSubject: ICoursesSubject): void {
    const modalRef = this.modalService.open(CoursesSubjectDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.coursesSubject = coursesSubject;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.reset();
      }
    });
  }

  protected sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateCoursesSubjects(data: ICoursesSubject[] | null, headers: HttpHeaders): void {
    this.links = this.parseLinks.parse(headers.get('link') ?? '');
    if (data) {
      for (const d of data) {
        this.coursesSubjects.push(d);
      }
    }
  }
}
