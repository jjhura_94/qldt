import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ICoursesSubject, CoursesSubject } from '../courses-subject.model';

import { CoursesSubjectService } from './courses-subject.service';

describe('Service Tests', () => {
  describe('CoursesSubject Service', () => {
    let service: CoursesSubjectService;
    let httpMock: HttpTestingController;
    let elemDefault: ICoursesSubject;
    let expectedResult: ICoursesSubject | ICoursesSubject[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(CoursesSubjectService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 0,
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a CoursesSubject', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new CoursesSubject()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a CoursesSubject', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a CoursesSubject', () => {
        const patchObject = Object.assign({}, new CoursesSubject());

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of CoursesSubject', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a CoursesSubject', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addCoursesSubjectToCollectionIfMissing', () => {
        it('should add a CoursesSubject to an empty array', () => {
          const coursesSubject: ICoursesSubject = { id: 123 };
          expectedResult = service.addCoursesSubjectToCollectionIfMissing([], coursesSubject);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(coursesSubject);
        });

        it('should not add a CoursesSubject to an array that contains it', () => {
          const coursesSubject: ICoursesSubject = { id: 123 };
          const coursesSubjectCollection: ICoursesSubject[] = [
            {
              ...coursesSubject,
            },
            { id: 456 },
          ];
          expectedResult = service.addCoursesSubjectToCollectionIfMissing(coursesSubjectCollection, coursesSubject);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a CoursesSubject to an array that doesn't contain it", () => {
          const coursesSubject: ICoursesSubject = { id: 123 };
          const coursesSubjectCollection: ICoursesSubject[] = [{ id: 456 }];
          expectedResult = service.addCoursesSubjectToCollectionIfMissing(coursesSubjectCollection, coursesSubject);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(coursesSubject);
        });

        it('should add only unique CoursesSubject to an array', () => {
          const coursesSubjectArray: ICoursesSubject[] = [{ id: 123 }, { id: 456 }, { id: 34194 }];
          const coursesSubjectCollection: ICoursesSubject[] = [{ id: 123 }];
          expectedResult = service.addCoursesSubjectToCollectionIfMissing(coursesSubjectCollection, ...coursesSubjectArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const coursesSubject: ICoursesSubject = { id: 123 };
          const coursesSubject2: ICoursesSubject = { id: 456 };
          expectedResult = service.addCoursesSubjectToCollectionIfMissing([], coursesSubject, coursesSubject2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(coursesSubject);
          expect(expectedResult).toContain(coursesSubject2);
        });

        it('should accept null and undefined values', () => {
          const coursesSubject: ICoursesSubject = { id: 123 };
          expectedResult = service.addCoursesSubjectToCollectionIfMissing([], null, coursesSubject, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(coursesSubject);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
