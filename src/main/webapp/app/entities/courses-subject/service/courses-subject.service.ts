import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ICoursesSubject, getCoursesSubjectIdentifier } from '../courses-subject.model';

export type EntityResponseType = HttpResponse<ICoursesSubject>;
export type EntityArrayResponseType = HttpResponse<ICoursesSubject[]>;

@Injectable({ providedIn: 'root' })
export class CoursesSubjectService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/courses-subjects');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(coursesSubject: ICoursesSubject): Observable<EntityResponseType> {
    return this.http.post<ICoursesSubject>(this.resourceUrl, coursesSubject, { observe: 'response' });
  }

  update(coursesSubject: ICoursesSubject): Observable<EntityResponseType> {
    return this.http.put<ICoursesSubject>(`${this.resourceUrl}/${getCoursesSubjectIdentifier(coursesSubject) as number}`, coursesSubject, {
      observe: 'response',
    });
  }

  partialUpdate(coursesSubject: ICoursesSubject): Observable<EntityResponseType> {
    return this.http.patch<ICoursesSubject>(
      `${this.resourceUrl}/${getCoursesSubjectIdentifier(coursesSubject) as number}`,
      coursesSubject,
      { observe: 'response' }
    );
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICoursesSubject>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICoursesSubject[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addCoursesSubjectToCollectionIfMissing(
    coursesSubjectCollection: ICoursesSubject[],
    ...coursesSubjectsToCheck: (ICoursesSubject | null | undefined)[]
  ): ICoursesSubject[] {
    const coursesSubjects: ICoursesSubject[] = coursesSubjectsToCheck.filter(isPresent);
    if (coursesSubjects.length > 0) {
      const coursesSubjectCollectionIdentifiers = coursesSubjectCollection.map(
        coursesSubjectItem => getCoursesSubjectIdentifier(coursesSubjectItem)!
      );
      const coursesSubjectsToAdd = coursesSubjects.filter(coursesSubjectItem => {
        const coursesSubjectIdentifier = getCoursesSubjectIdentifier(coursesSubjectItem);
        if (coursesSubjectIdentifier == null || coursesSubjectCollectionIdentifiers.includes(coursesSubjectIdentifier)) {
          return false;
        }
        coursesSubjectCollectionIdentifiers.push(coursesSubjectIdentifier);
        return true;
      });
      return [...coursesSubjectsToAdd, ...coursesSubjectCollection];
    }
    return coursesSubjectCollection;
  }
}
