import { ICourses } from 'app/entities/courses/courses.model';
import { ISubjects } from 'app/entities/subjects/subjects.model';

export interface ICoursesSubject {
  id?: number;
  course?: ICourses | null;
  sub?: ISubjects | null;
}

export class CoursesSubject implements ICoursesSubject {
  constructor(public id?: number, public course?: ICourses | null, public sub?: ISubjects | null) {}
}

export function getCoursesSubjectIdentifier(coursesSubject: ICoursesSubject): number | undefined {
  return coursesSubject.id;
}
