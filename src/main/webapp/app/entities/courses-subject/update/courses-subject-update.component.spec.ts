jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { CoursesSubjectService } from '../service/courses-subject.service';
import { ICoursesSubject, CoursesSubject } from '../courses-subject.model';
import { ICourses } from 'app/entities/courses/courses.model';
import { CoursesService } from 'app/entities/courses/service/courses.service';
import { ISubjects } from 'app/entities/subjects/subjects.model';
import { SubjectsService } from 'app/entities/subjects/service/subjects.service';

import { CoursesSubjectUpdateComponent } from './courses-subject-update.component';

describe('Component Tests', () => {
  describe('CoursesSubject Management Update Component', () => {
    let comp: CoursesSubjectUpdateComponent;
    let fixture: ComponentFixture<CoursesSubjectUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let coursesSubjectService: CoursesSubjectService;
    let coursesService: CoursesService;
    let subjectsService: SubjectsService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [CoursesSubjectUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(CoursesSubjectUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CoursesSubjectUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      coursesSubjectService = TestBed.inject(CoursesSubjectService);
      coursesService = TestBed.inject(CoursesService);
      subjectsService = TestBed.inject(SubjectsService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call Courses query and add missing value', () => {
        const coursesSubject: ICoursesSubject = { id: 456 };
        const course: ICourses = { id: 42162 };
        coursesSubject.course = course;

        const coursesCollection: ICourses[] = [{ id: 90233 }];
        spyOn(coursesService, 'query').and.returnValue(of(new HttpResponse({ body: coursesCollection })));
        const additionalCourses = [course];
        const expectedCollection: ICourses[] = [...additionalCourses, ...coursesCollection];
        spyOn(coursesService, 'addCoursesToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ coursesSubject });
        comp.ngOnInit();

        expect(coursesService.query).toHaveBeenCalled();
        expect(coursesService.addCoursesToCollectionIfMissing).toHaveBeenCalledWith(coursesCollection, ...additionalCourses);
        expect(comp.coursesSharedCollection).toEqual(expectedCollection);
      });

      it('Should call Subjects query and add missing value', () => {
        const coursesSubject: ICoursesSubject = { id: 456 };
        const sub: ISubjects = { id: 37832 };
        coursesSubject.sub = sub;

        const subjectsCollection: ISubjects[] = [{ id: 3478 }];
        spyOn(subjectsService, 'query').and.returnValue(of(new HttpResponse({ body: subjectsCollection })));
        const additionalSubjects = [sub];
        const expectedCollection: ISubjects[] = [...additionalSubjects, ...subjectsCollection];
        spyOn(subjectsService, 'addSubjectsToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ coursesSubject });
        comp.ngOnInit();

        expect(subjectsService.query).toHaveBeenCalled();
        expect(subjectsService.addSubjectsToCollectionIfMissing).toHaveBeenCalledWith(subjectsCollection, ...additionalSubjects);
        expect(comp.subjectsSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const coursesSubject: ICoursesSubject = { id: 456 };
        const course: ICourses = { id: 85721 };
        coursesSubject.course = course;
        const sub: ISubjects = { id: 79356 };
        coursesSubject.sub = sub;

        activatedRoute.data = of({ coursesSubject });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(coursesSubject));
        expect(comp.coursesSharedCollection).toContain(course);
        expect(comp.subjectsSharedCollection).toContain(sub);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const coursesSubject = { id: 123 };
        spyOn(coursesSubjectService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ coursesSubject });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: coursesSubject }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(coursesSubjectService.update).toHaveBeenCalledWith(coursesSubject);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const coursesSubject = new CoursesSubject();
        spyOn(coursesSubjectService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ coursesSubject });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: coursesSubject }));
        saveSubject.complete();

        // THEN
        expect(coursesSubjectService.create).toHaveBeenCalledWith(coursesSubject);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const coursesSubject = { id: 123 };
        spyOn(coursesSubjectService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ coursesSubject });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(coursesSubjectService.update).toHaveBeenCalledWith(coursesSubject);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackCoursesById', () => {
        it('Should return tracked Courses primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackCoursesById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });

      describe('trackSubjectsById', () => {
        it('Should return tracked Subjects primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackSubjectsById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});
