import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { ICoursesSubject, CoursesSubject } from '../courses-subject.model';
import { CoursesSubjectService } from '../service/courses-subject.service';
import { ICourses } from 'app/entities/courses/courses.model';
import { CoursesService } from 'app/entities/courses/service/courses.service';
import { ISubjects } from 'app/entities/subjects/subjects.model';
import { SubjectsService } from 'app/entities/subjects/service/subjects.service';

@Component({
  selector: 'jhi-courses-subject-update',
  templateUrl: './courses-subject-update.component.html',
})
export class CoursesSubjectUpdateComponent implements OnInit {
  isSaving = false;

  coursesSharedCollection: ICourses[] = [];
  subjectsSharedCollection: ISubjects[] = [];

  editForm = this.fb.group({
    id: [],
    course: [],
    sub: [],
  });

  constructor(
    protected coursesSubjectService: CoursesSubjectService,
    protected coursesService: CoursesService,
    protected subjectsService: SubjectsService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ coursesSubject }) => {
      this.updateForm(coursesSubject);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const coursesSubject = this.createFromForm();
    if (coursesSubject.id !== undefined) {
      this.subscribeToSaveResponse(this.coursesSubjectService.update(coursesSubject));
    } else {
      this.subscribeToSaveResponse(this.coursesSubjectService.create(coursesSubject));
    }
  }

  trackCoursesById(index: number, item: ICourses): number {
    return item.id!;
  }

  trackSubjectsById(index: number, item: ISubjects): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICoursesSubject>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(coursesSubject: ICoursesSubject): void {
    this.editForm.patchValue({
      id: coursesSubject.id,
      course: coursesSubject.course,
      sub: coursesSubject.sub,
    });

    this.coursesSharedCollection = this.coursesService.addCoursesToCollectionIfMissing(this.coursesSharedCollection, coursesSubject.course);
    this.subjectsSharedCollection = this.subjectsService.addSubjectsToCollectionIfMissing(
      this.subjectsSharedCollection,
      coursesSubject.sub
    );
  }

  protected loadRelationshipsOptions(): void {
    this.coursesService
      .query()
      .pipe(map((res: HttpResponse<ICourses[]>) => res.body ?? []))
      .pipe(map((courses: ICourses[]) => this.coursesService.addCoursesToCollectionIfMissing(courses, this.editForm.get('course')!.value)))
      .subscribe((courses: ICourses[]) => (this.coursesSharedCollection = courses));

    this.subjectsService
      .query()
      .pipe(map((res: HttpResponse<ISubjects[]>) => res.body ?? []))
      .pipe(
        map((subjects: ISubjects[]) => this.subjectsService.addSubjectsToCollectionIfMissing(subjects, this.editForm.get('sub')!.value))
      )
      .subscribe((subjects: ISubjects[]) => (this.subjectsSharedCollection = subjects));
  }

  protected createFromForm(): ICoursesSubject {
    return {
      ...new CoursesSubject(),
      id: this.editForm.get(['id'])!.value,
      course: this.editForm.get(['course'])!.value,
      sub: this.editForm.get(['sub'])!.value,
    };
  }
}
