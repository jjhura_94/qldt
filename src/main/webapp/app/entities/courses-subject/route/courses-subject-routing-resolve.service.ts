import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ICoursesSubject, CoursesSubject } from '../courses-subject.model';
import { CoursesSubjectService } from '../service/courses-subject.service';

@Injectable({ providedIn: 'root' })
export class CoursesSubjectRoutingResolveService implements Resolve<ICoursesSubject> {
  constructor(protected service: CoursesSubjectService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICoursesSubject> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((coursesSubject: HttpResponse<CoursesSubject>) => {
          if (coursesSubject.body) {
            return of(coursesSubject.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new CoursesSubject());
  }
}
