jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { ICoursesSubject, CoursesSubject } from '../courses-subject.model';
import { CoursesSubjectService } from '../service/courses-subject.service';

import { CoursesSubjectRoutingResolveService } from './courses-subject-routing-resolve.service';

describe('Service Tests', () => {
  describe('CoursesSubject routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: CoursesSubjectRoutingResolveService;
    let service: CoursesSubjectService;
    let resultCoursesSubject: ICoursesSubject | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(CoursesSubjectRoutingResolveService);
      service = TestBed.inject(CoursesSubjectService);
      resultCoursesSubject = undefined;
    });

    describe('resolve', () => {
      it('should return ICoursesSubject returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultCoursesSubject = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultCoursesSubject).toEqual({ id: 123 });
      });

      it('should return new ICoursesSubject if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultCoursesSubject = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultCoursesSubject).toEqual(new CoursesSubject());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        spyOn(service, 'find').and.returnValue(of(new HttpResponse({ body: null })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultCoursesSubject = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultCoursesSubject).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
