import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { CoursesSubjectComponent } from '../list/courses-subject.component';
import { CoursesSubjectDetailComponent } from '../detail/courses-subject-detail.component';
import { CoursesSubjectUpdateComponent } from '../update/courses-subject-update.component';
import { CoursesSubjectRoutingResolveService } from './courses-subject-routing-resolve.service';

const coursesSubjectRoute: Routes = [
  {
    path: '',
    component: CoursesSubjectComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CoursesSubjectDetailComponent,
    resolve: {
      coursesSubject: CoursesSubjectRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CoursesSubjectUpdateComponent,
    resolve: {
      coursesSubject: CoursesSubjectRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CoursesSubjectUpdateComponent,
    resolve: {
      coursesSubject: CoursesSubjectRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(coursesSubjectRoute)],
  exports: [RouterModule],
})
export class CoursesSubjectRoutingModule {}
