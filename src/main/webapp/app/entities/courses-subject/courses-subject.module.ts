import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { CoursesSubjectComponent } from './list/courses-subject.component';
import { CoursesSubjectDetailComponent } from './detail/courses-subject-detail.component';
import { CoursesSubjectUpdateComponent } from './update/courses-subject-update.component';
import { CoursesSubjectDeleteDialogComponent } from './delete/courses-subject-delete-dialog.component';
import { CoursesSubjectRoutingModule } from './route/courses-subject-routing.module';

@NgModule({
  imports: [SharedModule, CoursesSubjectRoutingModule],
  declarations: [
    CoursesSubjectComponent,
    CoursesSubjectDetailComponent,
    CoursesSubjectUpdateComponent,
    CoursesSubjectDeleteDialogComponent,
  ],
  entryComponents: [CoursesSubjectDeleteDialogComponent],
})
export class CoursesSubjectModule {}
