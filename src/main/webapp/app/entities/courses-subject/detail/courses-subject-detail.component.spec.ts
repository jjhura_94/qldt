import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { CoursesSubjectDetailComponent } from './courses-subject-detail.component';

describe('Component Tests', () => {
  describe('CoursesSubject Management Detail Component', () => {
    let comp: CoursesSubjectDetailComponent;
    let fixture: ComponentFixture<CoursesSubjectDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [CoursesSubjectDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ coursesSubject: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(CoursesSubjectDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CoursesSubjectDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load coursesSubject on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.coursesSubject).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
