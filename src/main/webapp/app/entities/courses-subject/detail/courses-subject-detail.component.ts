import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICoursesSubject } from '../courses-subject.model';

@Component({
  selector: 'jhi-courses-subject-detail',
  templateUrl: './courses-subject-detail.component.html',
})
export class CoursesSubjectDetailComponent implements OnInit {
  coursesSubject: ICoursesSubject | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ coursesSubject }) => {
      this.coursesSubject = coursesSubject;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
