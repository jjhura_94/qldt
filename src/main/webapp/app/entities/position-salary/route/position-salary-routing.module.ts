import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { PositionSalaryComponent } from '../list/position-salary.component';
import { PositionSalaryDetailComponent } from '../detail/position-salary-detail.component';
import { PositionSalaryUpdateComponent } from '../update/position-salary-update.component';
import { PositionSalaryRoutingResolveService } from './position-salary-routing-resolve.service';

const positionSalaryRoute: Routes = [
  {
    path: '',
    component: PositionSalaryComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: PositionSalaryDetailComponent,
    resolve: {
      positionSalary: PositionSalaryRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: PositionSalaryUpdateComponent,
    resolve: {
      positionSalary: PositionSalaryRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: PositionSalaryUpdateComponent,
    resolve: {
      positionSalary: PositionSalaryRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(positionSalaryRoute)],
  exports: [RouterModule],
})
export class PositionSalaryRoutingModule {}
