jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IPositionSalary, PositionSalary } from '../position-salary.model';
import { PositionSalaryService } from '../service/position-salary.service';

import { PositionSalaryRoutingResolveService } from './position-salary-routing-resolve.service';

describe('Service Tests', () => {
  describe('PositionSalary routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: PositionSalaryRoutingResolveService;
    let service: PositionSalaryService;
    let resultPositionSalary: IPositionSalary | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(PositionSalaryRoutingResolveService);
      service = TestBed.inject(PositionSalaryService);
      resultPositionSalary = undefined;
    });

    describe('resolve', () => {
      it('should return IPositionSalary returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultPositionSalary = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultPositionSalary).toEqual({ id: 123 });
      });

      it('should return new IPositionSalary if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultPositionSalary = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultPositionSalary).toEqual(new PositionSalary());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        spyOn(service, 'find').and.returnValue(of(new HttpResponse({ body: null })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultPositionSalary = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultPositionSalary).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
