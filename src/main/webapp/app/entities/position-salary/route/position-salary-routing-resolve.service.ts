import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IPositionSalary, PositionSalary } from '../position-salary.model';
import { PositionSalaryService } from '../service/position-salary.service';

@Injectable({ providedIn: 'root' })
export class PositionSalaryRoutingResolveService implements Resolve<IPositionSalary> {
  constructor(protected service: PositionSalaryService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IPositionSalary> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((positionSalary: HttpResponse<PositionSalary>) => {
          if (positionSalary.body) {
            return of(positionSalary.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new PositionSalary());
  }
}
