import { IStaff } from 'app/entities/staff/staff.model';

export interface IPositionSalary {
  id?: number;
  posCode?: string | null;
  posName?: string | null;
  salBase?: number | null;
  salAllowance?: number | null;
  staff?: IStaff[] | null;
}

export class PositionSalary implements IPositionSalary {
  constructor(
    public id?: number,
    public posCode?: string | null,
    public posName?: string | null,
    public salBase?: number | null,
    public salAllowance?: number | null,
    public staff?: IStaff[] | null
  ) {}
}

export function getPositionSalaryIdentifier(positionSalary: IPositionSalary): number | undefined {
  return positionSalary.id;
}
