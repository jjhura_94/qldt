import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IPositionSalary } from '../position-salary.model';
import { PositionSalaryService } from '../service/position-salary.service';

@Component({
  templateUrl: './position-salary-delete-dialog.component.html',
})
export class PositionSalaryDeleteDialogComponent {
  positionSalary?: IPositionSalary;

  constructor(protected positionSalaryService: PositionSalaryService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.positionSalaryService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
