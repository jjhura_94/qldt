import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PositionSalaryDetailComponent } from './position-salary-detail.component';

describe('Component Tests', () => {
  describe('PositionSalary Management Detail Component', () => {
    let comp: PositionSalaryDetailComponent;
    let fixture: ComponentFixture<PositionSalaryDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [PositionSalaryDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ positionSalary: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(PositionSalaryDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(PositionSalaryDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load positionSalary on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.positionSalary).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
