import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPositionSalary } from '../position-salary.model';

@Component({
  selector: 'jhi-position-salary-detail',
  templateUrl: './position-salary-detail.component.html',
})
export class PositionSalaryDetailComponent implements OnInit {
  positionSalary: IPositionSalary | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ positionSalary }) => {
      this.positionSalary = positionSalary;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
