import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { PositionSalaryComponent } from './list/position-salary.component';
import { PositionSalaryDetailComponent } from './detail/position-salary-detail.component';
import { PositionSalaryUpdateComponent } from './update/position-salary-update.component';
import { PositionSalaryDeleteDialogComponent } from './delete/position-salary-delete-dialog.component';
import { PositionSalaryRoutingModule } from './route/position-salary-routing.module';

@NgModule({
  imports: [SharedModule, PositionSalaryRoutingModule],
  declarations: [
    PositionSalaryComponent,
    PositionSalaryDetailComponent,
    PositionSalaryUpdateComponent,
    PositionSalaryDeleteDialogComponent,
  ],
  entryComponents: [PositionSalaryDeleteDialogComponent],
})
export class PositionSalaryModule {}
