import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IPositionSalary, PositionSalary } from '../position-salary.model';

import { PositionSalaryService } from './position-salary.service';

describe('Service Tests', () => {
  describe('PositionSalary Service', () => {
    let service: PositionSalaryService;
    let httpMock: HttpTestingController;
    let elemDefault: IPositionSalary;
    let expectedResult: IPositionSalary | IPositionSalary[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(PositionSalaryService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 0,
        posCode: 'AAAAAAA',
        posName: 'AAAAAAA',
        salBase: 0,
        salAllowance: 0,
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a PositionSalary', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new PositionSalary()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a PositionSalary', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            posCode: 'BBBBBB',
            posName: 'BBBBBB',
            salBase: 1,
            salAllowance: 1,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a PositionSalary', () => {
        const patchObject = Object.assign(
          {
            posName: 'BBBBBB',
          },
          new PositionSalary()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of PositionSalary', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            posCode: 'BBBBBB',
            posName: 'BBBBBB',
            salBase: 1,
            salAllowance: 1,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a PositionSalary', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addPositionSalaryToCollectionIfMissing', () => {
        it('should add a PositionSalary to an empty array', () => {
          const positionSalary: IPositionSalary = { id: 123 };
          expectedResult = service.addPositionSalaryToCollectionIfMissing([], positionSalary);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(positionSalary);
        });

        it('should not add a PositionSalary to an array that contains it', () => {
          const positionSalary: IPositionSalary = { id: 123 };
          const positionSalaryCollection: IPositionSalary[] = [
            {
              ...positionSalary,
            },
            { id: 456 },
          ];
          expectedResult = service.addPositionSalaryToCollectionIfMissing(positionSalaryCollection, positionSalary);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a PositionSalary to an array that doesn't contain it", () => {
          const positionSalary: IPositionSalary = { id: 123 };
          const positionSalaryCollection: IPositionSalary[] = [{ id: 456 }];
          expectedResult = service.addPositionSalaryToCollectionIfMissing(positionSalaryCollection, positionSalary);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(positionSalary);
        });

        it('should add only unique PositionSalary to an array', () => {
          const positionSalaryArray: IPositionSalary[] = [{ id: 123 }, { id: 456 }, { id: 57757 }];
          const positionSalaryCollection: IPositionSalary[] = [{ id: 123 }];
          expectedResult = service.addPositionSalaryToCollectionIfMissing(positionSalaryCollection, ...positionSalaryArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const positionSalary: IPositionSalary = { id: 123 };
          const positionSalary2: IPositionSalary = { id: 456 };
          expectedResult = service.addPositionSalaryToCollectionIfMissing([], positionSalary, positionSalary2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(positionSalary);
          expect(expectedResult).toContain(positionSalary2);
        });

        it('should accept null and undefined values', () => {
          const positionSalary: IPositionSalary = { id: 123 };
          expectedResult = service.addPositionSalaryToCollectionIfMissing([], null, positionSalary, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(positionSalary);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
