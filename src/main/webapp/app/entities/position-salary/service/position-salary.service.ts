import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IPositionSalary, getPositionSalaryIdentifier } from '../position-salary.model';

export type EntityResponseType = HttpResponse<IPositionSalary>;
export type EntityArrayResponseType = HttpResponse<IPositionSalary[]>;

@Injectable({ providedIn: 'root' })
export class PositionSalaryService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/position-salaries');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(positionSalary: IPositionSalary): Observable<EntityResponseType> {
    return this.http.post<IPositionSalary>(this.resourceUrl, positionSalary, { observe: 'response' });
  }

  update(positionSalary: IPositionSalary): Observable<EntityResponseType> {
    return this.http.put<IPositionSalary>(`${this.resourceUrl}/${getPositionSalaryIdentifier(positionSalary) as number}`, positionSalary, {
      observe: 'response',
    });
  }

  partialUpdate(positionSalary: IPositionSalary): Observable<EntityResponseType> {
    return this.http.patch<IPositionSalary>(
      `${this.resourceUrl}/${getPositionSalaryIdentifier(positionSalary) as number}`,
      positionSalary,
      { observe: 'response' }
    );
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IPositionSalary>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IPositionSalary[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addPositionSalaryToCollectionIfMissing(
    positionSalaryCollection: IPositionSalary[],
    ...positionSalariesToCheck: (IPositionSalary | null | undefined)[]
  ): IPositionSalary[] {
    const positionSalaries: IPositionSalary[] = positionSalariesToCheck.filter(isPresent);
    if (positionSalaries.length > 0) {
      const positionSalaryCollectionIdentifiers = positionSalaryCollection.map(
        positionSalaryItem => getPositionSalaryIdentifier(positionSalaryItem)!
      );
      const positionSalariesToAdd = positionSalaries.filter(positionSalaryItem => {
        const positionSalaryIdentifier = getPositionSalaryIdentifier(positionSalaryItem);
        if (positionSalaryIdentifier == null || positionSalaryCollectionIdentifiers.includes(positionSalaryIdentifier)) {
          return false;
        }
        positionSalaryCollectionIdentifiers.push(positionSalaryIdentifier);
        return true;
      });
      return [...positionSalariesToAdd, ...positionSalaryCollection];
    }
    return positionSalaryCollection;
  }
}
