import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IPositionSalary, PositionSalary } from '../position-salary.model';
import { PositionSalaryService } from '../service/position-salary.service';

@Component({
  selector: 'jhi-position-salary-update',
  templateUrl: './position-salary-update.component.html',
})
export class PositionSalaryUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    posCode: [],
    posName: [],
    salBase: [],
    salAllowance: [],
  });

  constructor(
    protected positionSalaryService: PositionSalaryService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ positionSalary }) => {
      this.updateForm(positionSalary);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const positionSalary = this.createFromForm();
    if (positionSalary.id !== undefined) {
      this.subscribeToSaveResponse(this.positionSalaryService.update(positionSalary));
    } else {
      this.subscribeToSaveResponse(this.positionSalaryService.create(positionSalary));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPositionSalary>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(positionSalary: IPositionSalary): void {
    this.editForm.patchValue({
      id: positionSalary.id,
      posCode: positionSalary.posCode,
      posName: positionSalary.posName,
      salBase: positionSalary.salBase,
      salAllowance: positionSalary.salAllowance,
    });
  }

  protected createFromForm(): IPositionSalary {
    return {
      ...new PositionSalary(),
      id: this.editForm.get(['id'])!.value,
      posCode: this.editForm.get(['posCode'])!.value,
      posName: this.editForm.get(['posName'])!.value,
      salBase: this.editForm.get(['salBase'])!.value,
      salAllowance: this.editForm.get(['salAllowance'])!.value,
    };
  }
}
