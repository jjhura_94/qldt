jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { PositionSalaryService } from '../service/position-salary.service';
import { IPositionSalary, PositionSalary } from '../position-salary.model';

import { PositionSalaryUpdateComponent } from './position-salary-update.component';

describe('Component Tests', () => {
  describe('PositionSalary Management Update Component', () => {
    let comp: PositionSalaryUpdateComponent;
    let fixture: ComponentFixture<PositionSalaryUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let positionSalaryService: PositionSalaryService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [PositionSalaryUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(PositionSalaryUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(PositionSalaryUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      positionSalaryService = TestBed.inject(PositionSalaryService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should update editForm', () => {
        const positionSalary: IPositionSalary = { id: 456 };

        activatedRoute.data = of({ positionSalary });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(positionSalary));
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const positionSalary = { id: 123 };
        spyOn(positionSalaryService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ positionSalary });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: positionSalary }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(positionSalaryService.update).toHaveBeenCalledWith(positionSalary);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const positionSalary = new PositionSalary();
        spyOn(positionSalaryService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ positionSalary });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: positionSalary }));
        saveSubject.complete();

        // THEN
        expect(positionSalaryService.create).toHaveBeenCalledWith(positionSalary);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const positionSalary = { id: 123 };
        spyOn(positionSalaryService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ positionSalary });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(positionSalaryService.update).toHaveBeenCalledWith(positionSalary);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });
  });
});
