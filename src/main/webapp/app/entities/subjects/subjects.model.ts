import { ISubjectResults } from 'app/entities/subject-results/subject-results.model';
import { IClasses } from 'app/entities/classes/classes.model';
import { ICoursesSubject } from 'app/entities/courses-subject/courses-subject.model';

export interface ISubjects {
  id?: number;
  subName?: string | null;
  subTotalHours?: number | null;
  subScore?: number | null;
  subActive?: boolean | null;
  subCode?: string | null;
  subDescription?: string | null;
  subjectResults?: ISubjectResults[] | null;
  classes?: IClasses[] | null;
  coursesSubjects?: ICoursesSubject[] | null;
}

export class Subjects implements ISubjects {
  constructor(
    public id?: number,
    public subName?: string | null,
    public subTotalHours?: number | null,
    public subScore?: number | null,
    public subActive?: boolean | null,
    public subCode?: string | null,
    public subDescription?: string | null,
    public subjectResults?: ISubjectResults[] | null,
    public classes?: IClasses[] | null,
    public coursesSubjects?: ICoursesSubject[] | null
  ) {
    this.subActive = this.subActive ?? false;
  }
}

export function getSubjectsIdentifier(subjects: ISubjects): number | undefined {
  return subjects.id;
}
