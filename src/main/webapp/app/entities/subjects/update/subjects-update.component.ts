import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { ISubjects, Subjects } from '../subjects.model';
import { SubjectsService } from '../service/subjects.service';

@Component({
  selector: 'jhi-subjects-update',
  templateUrl: './subjects-update.component.html',
})
export class SubjectsUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    subName: [],
    subTotalHours: [],
    subScore: [],
    subActive: [],
    subCode: [],
    subDescription: [],
  });

  constructor(protected subjectsService: SubjectsService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ subjects }) => {
      this.updateForm(subjects);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const subjects = this.createFromForm();
    if (subjects.id !== undefined) {
      this.subscribeToSaveResponse(this.subjectsService.update(subjects));
    } else {
      this.subscribeToSaveResponse(this.subjectsService.create(subjects));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISubjects>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(subjects: ISubjects): void {
    this.editForm.patchValue({
      id: subjects.id,
      subName: subjects.subName,
      subTotalHours: subjects.subTotalHours,
      subScore: subjects.subScore,
      subActive: subjects.subActive,
      subCode: subjects.subCode,
      subDescription: subjects.subDescription,
    });
  }

  protected createFromForm(): ISubjects {
    return {
      ...new Subjects(),
      id: this.editForm.get(['id'])!.value,
      subName: this.editForm.get(['subName'])!.value,
      subTotalHours: this.editForm.get(['subTotalHours'])!.value,
      subScore: this.editForm.get(['subScore'])!.value,
      subActive: this.editForm.get(['subActive'])!.value,
      subCode: this.editForm.get(['subCode'])!.value,
      subDescription: this.editForm.get(['subDescription'])!.value,
    };
  }
}
