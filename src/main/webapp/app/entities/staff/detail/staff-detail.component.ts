import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { StaffService } from '../service/staff.service';

import { IStaff } from '../staff.model';

@Component({
  selector: 'jhi-staff-detail',
  templateUrl: './staff-detail.component.html',
})
export class StaffDetailComponent implements OnInit {
  staff: IStaff | null = null;
  date = '';
  salary = 0;
  constructor(protected activatedRoute: ActivatedRoute, protected staffService: StaffService) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ staff }) => {
      this.staff = staff;
      this.salary = staff.salary;
    });
  }

  previousState(): void {
    window.history.back();
  }

  getSalary(): void {
    let id = 0;
    if (this.staff?.id) {
      id = this.staff.id;
    }
    this.staffService.salary(id, this.date.replace('-', '')).subscribe(res => {
      this.salary = res.result;
    });
  }
}
