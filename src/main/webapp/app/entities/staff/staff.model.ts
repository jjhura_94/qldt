import * as dayjs from 'dayjs';
import { ICourses } from 'app/entities/courses/courses.model';
import { IClasses } from 'app/entities/classes/classes.model';
import { IPositionSalary } from 'app/entities/position-salary/position-salary.model';

export interface IStaff {
  id?: number;
  staffCode?: string | null;
  staffFullname?: string | null;
  staffEmail?: string | null;
  staffPhoneNumber?: string | null;
  staffAddress?: string | null;
  dateJoin?: dayjs.Dayjs | null;
  courses?: ICourses[] | null;
  classes?: IClasses[] | null;
  manager?: IStaff | null;
  pos?: IPositionSalary | null;
  salary?: number;
}

export class Staff implements IStaff {
  constructor(
    public id?: number,
    public staffCode?: string | null,
    public staffFullname?: string | null,
    public staffEmail?: string | null,
    public staffPhoneNumber?: string | null,
    public staffAddress?: string | null,
    public dateJoin?: dayjs.Dayjs | null,
    public courses?: ICourses[] | null,
    public classes?: IClasses[] | null,
    public manager?: IStaff | null,
    public pos?: IPositionSalary | null,
    public salary?: number
  ) {}
}

export function getStaffIdentifier(staff: IStaff): number | undefined {
  return staff.id;
}
