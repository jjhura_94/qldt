jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { StaffService } from '../service/staff.service';
import { IStaff, Staff } from '../staff.model';
import { IPositionSalary } from 'app/entities/position-salary/position-salary.model';
import { PositionSalaryService } from 'app/entities/position-salary/service/position-salary.service';

import { StaffUpdateComponent } from './staff-update.component';

describe('Component Tests', () => {
  describe('Staff Management Update Component', () => {
    let comp: StaffUpdateComponent;
    let fixture: ComponentFixture<StaffUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let staffService: StaffService;
    let positionSalaryService: PositionSalaryService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [StaffUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(StaffUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(StaffUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      staffService = TestBed.inject(StaffService);
      positionSalaryService = TestBed.inject(PositionSalaryService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call Staff query and add missing value', () => {
        const staff: IStaff = { id: 456 };
        const manager: IStaff = { id: 26310 };
        staff.manager = manager;

        const staffCollection: IStaff[] = [{ id: 33781 }];
        spyOn(staffService, 'query').and.returnValue(of(new HttpResponse({ body: staffCollection })));
        const additionalStaff = [manager];
        const expectedCollection: IStaff[] = [...additionalStaff, ...staffCollection];
        spyOn(staffService, 'addStaffToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ staff });
        comp.ngOnInit();

        expect(staffService.query).toHaveBeenCalled();
        expect(staffService.addStaffToCollectionIfMissing).toHaveBeenCalledWith(staffCollection, ...additionalStaff);
        expect(comp.staffSharedCollection).toEqual(expectedCollection);
      });

      it('Should call PositionSalary query and add missing value', () => {
        const staff: IStaff = { id: 456 };
        const pos: IPositionSalary = { id: 99314 };
        staff.pos = pos;

        const positionSalaryCollection: IPositionSalary[] = [{ id: 59419 }];
        spyOn(positionSalaryService, 'query').and.returnValue(of(new HttpResponse({ body: positionSalaryCollection })));
        const additionalPositionSalaries = [pos];
        const expectedCollection: IPositionSalary[] = [...additionalPositionSalaries, ...positionSalaryCollection];
        spyOn(positionSalaryService, 'addPositionSalaryToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ staff });
        comp.ngOnInit();

        expect(positionSalaryService.query).toHaveBeenCalled();
        expect(positionSalaryService.addPositionSalaryToCollectionIfMissing).toHaveBeenCalledWith(
          positionSalaryCollection,
          ...additionalPositionSalaries
        );
        expect(comp.positionSalariesSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const staff: IStaff = { id: 456 };
        const manager: IStaff = { id: 39140 };
        staff.manager = manager;
        const pos: IPositionSalary = { id: 80700 };
        staff.pos = pos;

        activatedRoute.data = of({ staff });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(staff));
        expect(comp.staffSharedCollection).toContain(manager);
        expect(comp.positionSalariesSharedCollection).toContain(pos);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const staff = { id: 123 };
        spyOn(staffService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ staff });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: staff }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(staffService.update).toHaveBeenCalledWith(staff);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const staff = new Staff();
        spyOn(staffService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ staff });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: staff }));
        saveSubject.complete();

        // THEN
        expect(staffService.create).toHaveBeenCalledWith(staff);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const staff = { id: 123 };
        spyOn(staffService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ staff });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(staffService.update).toHaveBeenCalledWith(staff);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackStaffById', () => {
        it('Should return tracked Staff primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackStaffById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });

      describe('trackPositionSalaryById', () => {
        it('Should return tracked PositionSalary primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackPositionSalaryById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});
