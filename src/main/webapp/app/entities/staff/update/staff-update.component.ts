import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import * as dayjs from 'dayjs';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IStaff, Staff } from '../staff.model';
import { StaffService } from '../service/staff.service';
import { IPositionSalary } from 'app/entities/position-salary/position-salary.model';
import { PositionSalaryService } from 'app/entities/position-salary/service/position-salary.service';

@Component({
  selector: 'jhi-staff-update',
  templateUrl: './staff-update.component.html',
})
export class StaffUpdateComponent implements OnInit {
  isSaving = false;

  staffSharedCollection: IStaff[] = [];
  positionSalariesSharedCollection: IPositionSalary[] = [];

  editForm = this.fb.group({
    id: [],
    staffCode: [],
    staffFullname: [],
    staffEmail: [],
    staffPhoneNumber: [],
    staffAddress: [],
    dateJoin: [],
    manager: [],
    pos: [],
  });

  constructor(
    protected staffService: StaffService,
    protected positionSalaryService: PositionSalaryService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ staff }) => {
      if (staff.id === undefined) {
        const today = dayjs().startOf('day');
        staff.dateJoin = today;
      }

      this.updateForm(staff);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const staff = this.createFromForm();
    if (staff.id !== undefined) {
      this.subscribeToSaveResponse(this.staffService.update(staff));
    } else {
      this.subscribeToSaveResponse(this.staffService.create(staff));
    }
  }

  trackStaffById(index: number, item: IStaff): number {
    return item.id!;
  }

  trackPositionSalaryById(index: number, item: IPositionSalary): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IStaff>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(staff: IStaff): void {
    this.editForm.patchValue({
      id: staff.id,
      staffCode: staff.staffCode,
      staffFullname: staff.staffFullname,
      staffEmail: staff.staffEmail,
      staffPhoneNumber: staff.staffPhoneNumber,
      staffAddress: staff.staffAddress,
      dateJoin: staff.dateJoin ? staff.dateJoin.format(DATE_TIME_FORMAT) : null,
      manager: staff.manager,
      pos: staff.pos,
    });

    this.staffSharedCollection = this.staffService.addStaffToCollectionIfMissing(this.staffSharedCollection, staff.manager);
    this.positionSalariesSharedCollection = this.positionSalaryService.addPositionSalaryToCollectionIfMissing(
      this.positionSalariesSharedCollection,
      staff.pos
    );
  }

  protected loadRelationshipsOptions(): void {
    this.staffService
      .query()
      .pipe(map((res: HttpResponse<IStaff[]>) => res.body ?? []))
      .pipe(map((staff: IStaff[]) => this.staffService.addStaffToCollectionIfMissing(staff, this.editForm.get('manager')!.value)))
      .subscribe((staff: IStaff[]) => (this.staffSharedCollection = staff));

    this.positionSalaryService
      .query()
      .pipe(map((res: HttpResponse<IPositionSalary[]>) => res.body ?? []))
      .pipe(
        map((positionSalaries: IPositionSalary[]) =>
          this.positionSalaryService.addPositionSalaryToCollectionIfMissing(positionSalaries, this.editForm.get('pos')!.value)
        )
      )
      .subscribe((positionSalaries: IPositionSalary[]) => (this.positionSalariesSharedCollection = positionSalaries));
  }

  protected createFromForm(): IStaff {
    return {
      ...new Staff(),
      id: this.editForm.get(['id'])!.value,
      staffCode: this.editForm.get(['staffCode'])!.value,
      staffFullname: this.editForm.get(['staffFullname'])!.value,
      staffEmail: this.editForm.get(['staffEmail'])!.value,
      staffPhoneNumber: this.editForm.get(['staffPhoneNumber'])!.value,
      staffAddress: this.editForm.get(['staffAddress'])!.value,
      dateJoin: this.editForm.get(['dateJoin'])!.value ? dayjs(this.editForm.get(['dateJoin'])!.value, DATE_TIME_FORMAT) : undefined,
      manager: this.editForm.get(['manager'])!.value,
      pos: this.editForm.get(['pos'])!.value,
    };
  }
}
