import { Component, OnInit } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IStaff } from '../staff.model';

import { ITEMS_PER_PAGE } from 'app/config/pagination.constants';
import { StaffService } from '../service/staff.service';
import { StaffDeleteDialogComponent } from '../delete/staff-delete-dialog.component';
import { ParseLinks } from 'app/core/util/parse-links.service';

@Component({
  selector: 'jhi-staff',
  templateUrl: './staff.component.html',
})
export class StaffComponent implements OnInit {
  staff: IStaff[];
  isLoading = false;
  itemsPerPage: number;
  links: { [key: string]: number };
  page: number;
  predicate: string;
  ascending: boolean;

  constructor(protected staffService: StaffService, protected modalService: NgbModal, protected parseLinks: ParseLinks) {
    this.staff = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0,
    };
    this.predicate = 'id';
    this.ascending = true;
  }

  loadAll(): void {
    this.isLoading = true;

    this.staffService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .subscribe(
        (res: HttpResponse<IStaff[]>) => {
          this.isLoading = false;
          this.paginateStaff(res.body, res.headers);
        },
        () => {
          this.isLoading = false;
        }
      );
  }

  reset(): void {
    this.page = 0;
    this.staff = [];
    this.loadAll();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IStaff): number {
    return item.id!;
  }

  delete(staff: IStaff): void {
    const modalRef = this.modalService.open(StaffDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.staff = staff;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.reset();
      }
    });
  }

  protected sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateStaff(data: IStaff[] | null, headers: HttpHeaders): void {
    this.links = this.parseLinks.parse(headers.get('link') ?? '');
    if (data) {
      for (const d of data) {
        this.staff.push(d);
      }
    }
  }
}
