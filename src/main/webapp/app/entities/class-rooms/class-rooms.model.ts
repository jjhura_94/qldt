import { IClasses } from 'app/entities/classes/classes.model';

export interface IClassRooms {
  id?: number;
  roomName?: string | null;
  roomAddress?: string | null;
  totalSeats?: number | null;
  roomDescription?: string | null;
  classes?: IClasses[] | null;
}

export class ClassRooms implements IClassRooms {
  constructor(
    public id?: number,
    public roomName?: string | null,
    public roomAddress?: string | null,
    public totalSeats?: number | null,
    public roomDescription?: string | null,
    public classes?: IClasses[] | null
  ) {}
}

export function getClassRoomsIdentifier(classRooms: IClassRooms): number | undefined {
  return classRooms.id;
}
