import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IClassRooms } from '../class-rooms.model';
import { ClassRoomsService } from '../service/class-rooms.service';

@Component({
  templateUrl: './class-rooms-delete-dialog.component.html',
})
export class ClassRoomsDeleteDialogComponent {
  classRooms?: IClassRooms;

  constructor(protected classRoomsService: ClassRoomsService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.classRoomsService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
