jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IClassRooms, ClassRooms } from '../class-rooms.model';
import { ClassRoomsService } from '../service/class-rooms.service';

import { ClassRoomsRoutingResolveService } from './class-rooms-routing-resolve.service';

describe('Service Tests', () => {
  describe('ClassRooms routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: ClassRoomsRoutingResolveService;
    let service: ClassRoomsService;
    let resultClassRooms: IClassRooms | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(ClassRoomsRoutingResolveService);
      service = TestBed.inject(ClassRoomsService);
      resultClassRooms = undefined;
    });

    describe('resolve', () => {
      it('should return IClassRooms returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultClassRooms = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultClassRooms).toEqual({ id: 123 });
      });

      it('should return new IClassRooms if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultClassRooms = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultClassRooms).toEqual(new ClassRooms());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        spyOn(service, 'find').and.returnValue(of(new HttpResponse({ body: null })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultClassRooms = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultClassRooms).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
