import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ClassRoomsComponent } from '../list/class-rooms.component';
import { ClassRoomsDetailComponent } from '../detail/class-rooms-detail.component';
import { ClassRoomsUpdateComponent } from '../update/class-rooms-update.component';
import { ClassRoomsRoutingResolveService } from './class-rooms-routing-resolve.service';

const classRoomsRoute: Routes = [
  {
    path: '',
    component: ClassRoomsComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ClassRoomsDetailComponent,
    resolve: {
      classRooms: ClassRoomsRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ClassRoomsUpdateComponent,
    resolve: {
      classRooms: ClassRoomsRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ClassRoomsUpdateComponent,
    resolve: {
      classRooms: ClassRoomsRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(classRoomsRoute)],
  exports: [RouterModule],
})
export class ClassRoomsRoutingModule {}
