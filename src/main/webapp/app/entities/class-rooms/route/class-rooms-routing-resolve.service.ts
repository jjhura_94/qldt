import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IClassRooms, ClassRooms } from '../class-rooms.model';
import { ClassRoomsService } from '../service/class-rooms.service';

@Injectable({ providedIn: 'root' })
export class ClassRoomsRoutingResolveService implements Resolve<IClassRooms> {
  constructor(protected service: ClassRoomsService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IClassRooms> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((classRooms: HttpResponse<ClassRooms>) => {
          if (classRooms.body) {
            return of(classRooms.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ClassRooms());
  }
}
