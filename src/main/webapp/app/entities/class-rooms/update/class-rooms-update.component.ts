import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IClassRooms, ClassRooms } from '../class-rooms.model';
import { ClassRoomsService } from '../service/class-rooms.service';

@Component({
  selector: 'jhi-class-rooms-update',
  templateUrl: './class-rooms-update.component.html',
})
export class ClassRoomsUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    roomName: [],
    roomAddress: [],
    totalSeats: [],
    roomDescription: [],
  });

  constructor(protected classRoomsService: ClassRoomsService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ classRooms }) => {
      this.updateForm(classRooms);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const classRooms = this.createFromForm();
    if (classRooms.id !== undefined) {
      this.subscribeToSaveResponse(this.classRoomsService.update(classRooms));
    } else {
      this.subscribeToSaveResponse(this.classRoomsService.create(classRooms));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IClassRooms>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(classRooms: IClassRooms): void {
    this.editForm.patchValue({
      id: classRooms.id,
      roomName: classRooms.roomName,
      roomAddress: classRooms.roomAddress,
      totalSeats: classRooms.totalSeats,
      roomDescription: classRooms.roomDescription,
    });
  }

  protected createFromForm(): IClassRooms {
    return {
      ...new ClassRooms(),
      id: this.editForm.get(['id'])!.value,
      roomName: this.editForm.get(['roomName'])!.value,
      roomAddress: this.editForm.get(['roomAddress'])!.value,
      totalSeats: this.editForm.get(['totalSeats'])!.value,
      roomDescription: this.editForm.get(['roomDescription'])!.value,
    };
  }
}
