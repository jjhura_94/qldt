jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { ClassRoomsService } from '../service/class-rooms.service';
import { IClassRooms, ClassRooms } from '../class-rooms.model';

import { ClassRoomsUpdateComponent } from './class-rooms-update.component';

describe('Component Tests', () => {
  describe('ClassRooms Management Update Component', () => {
    let comp: ClassRoomsUpdateComponent;
    let fixture: ComponentFixture<ClassRoomsUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let classRoomsService: ClassRoomsService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [ClassRoomsUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(ClassRoomsUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ClassRoomsUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      classRoomsService = TestBed.inject(ClassRoomsService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should update editForm', () => {
        const classRooms: IClassRooms = { id: 456 };

        activatedRoute.data = of({ classRooms });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(classRooms));
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const classRooms = { id: 123 };
        spyOn(classRoomsService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ classRooms });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: classRooms }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(classRoomsService.update).toHaveBeenCalledWith(classRooms);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const classRooms = new ClassRooms();
        spyOn(classRoomsService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ classRooms });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: classRooms }));
        saveSubject.complete();

        // THEN
        expect(classRoomsService.create).toHaveBeenCalledWith(classRooms);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const classRooms = { id: 123 };
        spyOn(classRoomsService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ classRooms });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(classRoomsService.update).toHaveBeenCalledWith(classRooms);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });
  });
});
