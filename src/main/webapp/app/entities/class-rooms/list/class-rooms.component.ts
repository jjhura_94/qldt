import { Component, OnInit } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IClassRooms } from '../class-rooms.model';

import { ITEMS_PER_PAGE } from 'app/config/pagination.constants';
import { ClassRoomsService } from '../service/class-rooms.service';
import { ClassRoomsDeleteDialogComponent } from '../delete/class-rooms-delete-dialog.component';
import { ParseLinks } from 'app/core/util/parse-links.service';

@Component({
  selector: 'jhi-class-rooms',
  templateUrl: './class-rooms.component.html',
})
export class ClassRoomsComponent implements OnInit {
  classRooms: IClassRooms[];
  isLoading = false;
  itemsPerPage: number;
  links: { [key: string]: number };
  page: number;
  predicate: string;
  ascending: boolean;

  constructor(protected classRoomsService: ClassRoomsService, protected modalService: NgbModal, protected parseLinks: ParseLinks) {
    this.classRooms = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0,
    };
    this.predicate = 'id';
    this.ascending = true;
  }

  loadAll(): void {
    this.isLoading = true;

    this.classRoomsService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .subscribe(
        (res: HttpResponse<IClassRooms[]>) => {
          this.isLoading = false;
          this.paginateClassRooms(res.body, res.headers);
        },
        () => {
          this.isLoading = false;
        }
      );
  }

  reset(): void {
    this.page = 0;
    this.classRooms = [];
    this.loadAll();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IClassRooms): number {
    return item.id!;
  }

  delete(classRooms: IClassRooms): void {
    const modalRef = this.modalService.open(ClassRoomsDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.classRooms = classRooms;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.reset();
      }
    });
  }

  protected sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateClassRooms(data: IClassRooms[] | null, headers: HttpHeaders): void {
    this.links = this.parseLinks.parse(headers.get('link') ?? '');
    if (data) {
      for (const d of data) {
        this.classRooms.push(d);
      }
    }
  }
}
