import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IClassRooms } from '../class-rooms.model';

@Component({
  selector: 'jhi-class-rooms-detail',
  templateUrl: './class-rooms-detail.component.html',
})
export class ClassRoomsDetailComponent implements OnInit {
  classRooms: IClassRooms | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ classRooms }) => {
      this.classRooms = classRooms;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
