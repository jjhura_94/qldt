import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ClassRoomsDetailComponent } from './class-rooms-detail.component';

describe('Component Tests', () => {
  describe('ClassRooms Management Detail Component', () => {
    let comp: ClassRoomsDetailComponent;
    let fixture: ComponentFixture<ClassRoomsDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [ClassRoomsDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ classRooms: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(ClassRoomsDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ClassRoomsDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load classRooms on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.classRooms).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
