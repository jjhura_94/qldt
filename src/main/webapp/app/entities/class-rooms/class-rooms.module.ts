import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { ClassRoomsComponent } from './list/class-rooms.component';
import { ClassRoomsDetailComponent } from './detail/class-rooms-detail.component';
import { ClassRoomsUpdateComponent } from './update/class-rooms-update.component';
import { ClassRoomsDeleteDialogComponent } from './delete/class-rooms-delete-dialog.component';
import { ClassRoomsRoutingModule } from './route/class-rooms-routing.module';

@NgModule({
  imports: [SharedModule, ClassRoomsRoutingModule],
  declarations: [ClassRoomsComponent, ClassRoomsDetailComponent, ClassRoomsUpdateComponent, ClassRoomsDeleteDialogComponent],
  entryComponents: [ClassRoomsDeleteDialogComponent],
})
export class ClassRoomsModule {}
