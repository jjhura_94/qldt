import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IClassRooms, getClassRoomsIdentifier } from '../class-rooms.model';

export type EntityResponseType = HttpResponse<IClassRooms>;
export type EntityArrayResponseType = HttpResponse<IClassRooms[]>;

@Injectable({ providedIn: 'root' })
export class ClassRoomsService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/class-rooms');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(classRooms: IClassRooms): Observable<EntityResponseType> {
    return this.http.post<IClassRooms>(this.resourceUrl, classRooms, { observe: 'response' });
  }

  update(classRooms: IClassRooms): Observable<EntityResponseType> {
    return this.http.put<IClassRooms>(`${this.resourceUrl}/${getClassRoomsIdentifier(classRooms) as number}`, classRooms, {
      observe: 'response',
    });
  }

  partialUpdate(classRooms: IClassRooms): Observable<EntityResponseType> {
    return this.http.patch<IClassRooms>(`${this.resourceUrl}/${getClassRoomsIdentifier(classRooms) as number}`, classRooms, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IClassRooms>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IClassRooms[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addClassRoomsToCollectionIfMissing(
    classRoomsCollection: IClassRooms[],
    ...classRoomsToCheck: (IClassRooms | null | undefined)[]
  ): IClassRooms[] {
    const classRooms: IClassRooms[] = classRoomsToCheck.filter(isPresent);
    if (classRooms.length > 0) {
      const classRoomsCollectionIdentifiers = classRoomsCollection.map(classRoomsItem => getClassRoomsIdentifier(classRoomsItem)!);
      const classRoomsToAdd = classRooms.filter(classRoomsItem => {
        const classRoomsIdentifier = getClassRoomsIdentifier(classRoomsItem);
        if (classRoomsIdentifier == null || classRoomsCollectionIdentifiers.includes(classRoomsIdentifier)) {
          return false;
        }
        classRoomsCollectionIdentifiers.push(classRoomsIdentifier);
        return true;
      });
      return [...classRoomsToAdd, ...classRoomsCollection];
    }
    return classRoomsCollection;
  }
}
