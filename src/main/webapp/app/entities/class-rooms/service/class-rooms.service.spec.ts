import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IClassRooms, ClassRooms } from '../class-rooms.model';

import { ClassRoomsService } from './class-rooms.service';

describe('Service Tests', () => {
  describe('ClassRooms Service', () => {
    let service: ClassRoomsService;
    let httpMock: HttpTestingController;
    let elemDefault: IClassRooms;
    let expectedResult: IClassRooms | IClassRooms[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(ClassRoomsService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 0,
        roomName: 'AAAAAAA',
        roomAddress: 'AAAAAAA',
        totalSeats: 0,
        roomDescription: 'AAAAAAA',
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a ClassRooms', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new ClassRooms()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a ClassRooms', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            roomName: 'BBBBBB',
            roomAddress: 'BBBBBB',
            totalSeats: 1,
            roomDescription: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a ClassRooms', () => {
        const patchObject = Object.assign(
          {
            roomAddress: 'BBBBBB',
          },
          new ClassRooms()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of ClassRooms', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            roomName: 'BBBBBB',
            roomAddress: 'BBBBBB',
            totalSeats: 1,
            roomDescription: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a ClassRooms', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addClassRoomsToCollectionIfMissing', () => {
        it('should add a ClassRooms to an empty array', () => {
          const classRooms: IClassRooms = { id: 123 };
          expectedResult = service.addClassRoomsToCollectionIfMissing([], classRooms);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(classRooms);
        });

        it('should not add a ClassRooms to an array that contains it', () => {
          const classRooms: IClassRooms = { id: 123 };
          const classRoomsCollection: IClassRooms[] = [
            {
              ...classRooms,
            },
            { id: 456 },
          ];
          expectedResult = service.addClassRoomsToCollectionIfMissing(classRoomsCollection, classRooms);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a ClassRooms to an array that doesn't contain it", () => {
          const classRooms: IClassRooms = { id: 123 };
          const classRoomsCollection: IClassRooms[] = [{ id: 456 }];
          expectedResult = service.addClassRoomsToCollectionIfMissing(classRoomsCollection, classRooms);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(classRooms);
        });

        it('should add only unique ClassRooms to an array', () => {
          const classRoomsArray: IClassRooms[] = [{ id: 123 }, { id: 456 }, { id: 87938 }];
          const classRoomsCollection: IClassRooms[] = [{ id: 123 }];
          expectedResult = service.addClassRoomsToCollectionIfMissing(classRoomsCollection, ...classRoomsArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const classRooms: IClassRooms = { id: 123 };
          const classRooms2: IClassRooms = { id: 456 };
          expectedResult = service.addClassRoomsToCollectionIfMissing([], classRooms, classRooms2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(classRooms);
          expect(expectedResult).toContain(classRooms2);
        });

        it('should accept null and undefined values', () => {
          const classRooms: IClassRooms = { id: 123 };
          expectedResult = service.addClassRoomsToCollectionIfMissing([], null, classRooms, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(classRooms);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
