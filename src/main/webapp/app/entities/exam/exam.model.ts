import * as dayjs from 'dayjs';
import { ISubjectResults } from 'app/entities/subject-results/subject-results.model';

export interface IExam {
  id?: number;
  examName?: string | null;
  examDate?: dayjs.Dayjs | null;
  examNote?: string | null;
  coefficient?: number | null;
  examTime?: number | null;
  subResult?: ISubjectResults | null;
}

export class Exam implements IExam {
  constructor(
    public id?: number,
    public examName?: string | null,
    public examDate?: dayjs.Dayjs | null,
    public examNote?: string | null,
    public coefficient?: number | null,
    public examTime?: number | null,
    public subResult?: ISubjectResults | null
  ) {}
}

export function getExamIdentifier(exam: IExam): number | undefined {
  return exam.id;
}
