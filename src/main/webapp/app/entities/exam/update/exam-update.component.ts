import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import * as dayjs from 'dayjs';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IExam, Exam } from '../exam.model';
import { ExamService } from '../service/exam.service';
import { ISubjectResults } from 'app/entities/subject-results/subject-results.model';
import { SubjectResultsService } from 'app/entities/subject-results/service/subject-results.service';

@Component({
  selector: 'jhi-exam-update',
  templateUrl: './exam-update.component.html',
})
export class ExamUpdateComponent implements OnInit {
  isSaving = false;

  subjectResultsSharedCollection: ISubjectResults[] = [];

  editForm = this.fb.group({
    id: [],
    examName: [],
    examDate: [],
    examNote: [],
    coefficient: [],
    examTime: [],
    subResult: [],
  });

  constructor(
    protected examService: ExamService,
    protected subjectResultsService: SubjectResultsService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ exam }) => {
      if (exam.id === undefined) {
        const today = dayjs().startOf('day');
        exam.examDate = today;
      }

      this.updateForm(exam);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const exam = this.createFromForm();
    if (exam.id !== undefined) {
      this.subscribeToSaveResponse(this.examService.update(exam));
    } else {
      this.subscribeToSaveResponse(this.examService.create(exam));
    }
  }

  trackSubjectResultsById(index: number, item: ISubjectResults): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IExam>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(exam: IExam): void {
    this.editForm.patchValue({
      id: exam.id,
      examName: exam.examName,
      examDate: exam.examDate ? exam.examDate.format(DATE_TIME_FORMAT) : null,
      examNote: exam.examNote,
      coefficient: exam.coefficient,
      examTime: exam.examTime,
      subResult: exam.subResult,
    });

    this.subjectResultsSharedCollection = this.subjectResultsService.addSubjectResultsToCollectionIfMissing(
      this.subjectResultsSharedCollection,
      exam.subResult
    );
  }

  protected loadRelationshipsOptions(): void {
    this.subjectResultsService
      .query()
      .pipe(map((res: HttpResponse<ISubjectResults[]>) => res.body ?? []))
      .pipe(
        map((subjectResults: ISubjectResults[]) =>
          this.subjectResultsService.addSubjectResultsToCollectionIfMissing(subjectResults, this.editForm.get('subResult')!.value)
        )
      )
      .subscribe((subjectResults: ISubjectResults[]) => (this.subjectResultsSharedCollection = subjectResults));
  }

  protected createFromForm(): IExam {
    return {
      ...new Exam(),
      id: this.editForm.get(['id'])!.value,
      examName: this.editForm.get(['examName'])!.value,
      examDate: this.editForm.get(['examDate'])!.value ? dayjs(this.editForm.get(['examDate'])!.value, DATE_TIME_FORMAT) : undefined,
      examNote: this.editForm.get(['examNote'])!.value,
      coefficient: this.editForm.get(['coefficient'])!.value,
      examTime: this.editForm.get(['examTime'])!.value,
      subResult: this.editForm.get(['subResult'])!.value,
    };
  }
}
