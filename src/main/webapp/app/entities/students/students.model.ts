import * as dayjs from 'dayjs';
import { ISubjectResults } from 'app/entities/subject-results/subject-results.model';

export interface IStudents {
  id?: number;
  stuCode?: string | null;
  stuName?: string | null;
  stuDob?: dayjs.Dayjs | null;
  stuEmail?: string | null;
  stuPhone?: string | null;
  stuAddress?: string | null;
  stuGender?: string | null;
  stuStatus?: string | null;
  stuDate?: dayjs.Dayjs | null;
  subjectResults?: ISubjectResults[] | null;
}

export class Students implements IStudents {
  constructor(
    public id?: number,
    public stuCode?: string | null,
    public stuName?: string | null,
    public stuDob?: dayjs.Dayjs | null,
    public stuEmail?: string | null,
    public stuPhone?: string | null,
    public stuAddress?: string | null,
    public stuGender?: string | null,
    public stuStatus?: string | null,
    public stuDate?: dayjs.Dayjs | null,
    public subjectResults?: ISubjectResults[] | null
  ) {}
}

export function getStudentsIdentifier(students: IStudents): number | undefined {
  return students.id;
}
