import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IStudents, getStudentsIdentifier } from '../students.model';

export type EntityResponseType = HttpResponse<IStudents>;
export type EntityArrayResponseType = HttpResponse<IStudents[]>;

@Injectable({ providedIn: 'root' })
export class StudentsService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/students');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(students: IStudents): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(students);
    return this.http
      .post<IStudents>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(students: IStudents): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(students);
    return this.http
      .put<IStudents>(`${this.resourceUrl}/${getStudentsIdentifier(students) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(students: IStudents): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(students);
    return this.http
      .patch<IStudents>(`${this.resourceUrl}/${getStudentsIdentifier(students) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IStudents>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IStudents[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addStudentsToCollectionIfMissing(studentsCollection: IStudents[], ...studentsToCheck: (IStudents | null | undefined)[]): IStudents[] {
    const students: IStudents[] = studentsToCheck.filter(isPresent);
    if (students.length > 0) {
      const studentsCollectionIdentifiers = studentsCollection.map(studentsItem => getStudentsIdentifier(studentsItem)!);
      const studentsToAdd = students.filter(studentsItem => {
        const studentsIdentifier = getStudentsIdentifier(studentsItem);
        if (studentsIdentifier == null || studentsCollectionIdentifiers.includes(studentsIdentifier)) {
          return false;
        }
        studentsCollectionIdentifiers.push(studentsIdentifier);
        return true;
      });
      return [...studentsToAdd, ...studentsCollection];
    }
    return studentsCollection;
  }

  protected convertDateFromClient(students: IStudents): IStudents {
    return Object.assign({}, students, {
      stuDob: students.stuDob?.isValid() ? students.stuDob.toJSON() : undefined,
      stuDate: students.stuDate?.isValid() ? students.stuDate.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.stuDob = res.body.stuDob ? dayjs(res.body.stuDob) : undefined;
      res.body.stuDate = res.body.stuDate ? dayjs(res.body.stuDate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((students: IStudents) => {
        students.stuDob = students.stuDob ? dayjs(students.stuDob) : undefined;
        students.stuDate = students.stuDate ? dayjs(students.stuDate) : undefined;
      });
    }
    return res;
  }
}
