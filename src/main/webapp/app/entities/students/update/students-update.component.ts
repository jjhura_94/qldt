import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import * as dayjs from 'dayjs';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IStudents, Students } from '../students.model';
import { StudentsService } from '../service/students.service';

@Component({
  selector: 'jhi-students-update',
  templateUrl: './students-update.component.html',
})
export class StudentsUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    stuCode: [],
    stuName: [],
    stuDob: [],
    stuEmail: [],
    stuPhone: [],
    stuAddress: [],
    stuGender: [],
    stuStatus: [],
    stuDate: [],
  });

  constructor(protected studentsService: StudentsService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ students }) => {
      if (students.id === undefined) {
        const today = dayjs().startOf('day');
        students.stuDob = today;
        students.stuDate = today;
      }

      this.updateForm(students);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const students = this.createFromForm();
    if (students.id !== undefined) {
      this.subscribeToSaveResponse(this.studentsService.update(students));
    } else {
      this.subscribeToSaveResponse(this.studentsService.create(students));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IStudents>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(students: IStudents): void {
    this.editForm.patchValue({
      id: students.id,
      stuCode: students.stuCode,
      stuName: students.stuName,
      stuDob: students.stuDob ? students.stuDob.format(DATE_TIME_FORMAT) : null,
      stuEmail: students.stuEmail,
      stuPhone: students.stuPhone,
      stuAddress: students.stuAddress,
      stuGender: students.stuGender,
      stuStatus: students.stuStatus,
      stuDate: students.stuDate ? students.stuDate.format(DATE_TIME_FORMAT) : null,
    });
  }

  protected createFromForm(): IStudents {
    return {
      ...new Students(),
      id: this.editForm.get(['id'])!.value,
      stuCode: this.editForm.get(['stuCode'])!.value,
      stuName: this.editForm.get(['stuName'])!.value,
      stuDob: this.editForm.get(['stuDob'])!.value ? dayjs(this.editForm.get(['stuDob'])!.value, DATE_TIME_FORMAT) : undefined,
      stuEmail: this.editForm.get(['stuEmail'])!.value,
      stuPhone: this.editForm.get(['stuPhone'])!.value,
      stuAddress: this.editForm.get(['stuAddress'])!.value,
      stuGender: this.editForm.get(['stuGender'])!.value,
      stuStatus: this.editForm.get(['stuStatus'])!.value,
      stuDate: this.editForm.get(['stuDate'])!.value ? dayjs(this.editForm.get(['stuDate'])!.value, DATE_TIME_FORMAT) : undefined,
    };
  }
}
