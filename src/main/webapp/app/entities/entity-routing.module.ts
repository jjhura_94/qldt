import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'exam',
        data: { pageTitle: 'Exams' },
        loadChildren: () => import('./exam/exam.module').then(m => m.ExamModule),
      },
      {
        path: 'subject-results',
        data: { pageTitle: 'SubjectResults' },
        loadChildren: () => import('./subject-results/subject-results.module').then(m => m.SubjectResultsModule),
      },
      {
        path: 'subjects',
        data: { pageTitle: 'Subjects' },
        loadChildren: () => import('./subjects/subjects.module').then(m => m.SubjectsModule),
      },
      {
        path: 'students',
        data: { pageTitle: 'Students' },
        loadChildren: () => import('./students/students.module').then(m => m.StudentsModule),
      },
      {
        path: 'classes',
        data: { pageTitle: 'Classes' },
        loadChildren: () => import('./classes/classes.module').then(m => m.ClassesModule),
      },
      {
        path: 'class-rooms',
        data: { pageTitle: 'ClassRooms' },
        loadChildren: () => import('./class-rooms/class-rooms.module').then(m => m.ClassRoomsModule),
      },
      {
        path: 'staff',
        data: { pageTitle: 'Staff' },
        loadChildren: () => import('./staff/staff.module').then(m => m.StaffModule),
      },
      {
        path: 'position-salary',
        data: { pageTitle: 'PositionSalaries' },
        loadChildren: () => import('./position-salary/position-salary.module').then(m => m.PositionSalaryModule),
      },
      {
        path: 'courses',
        data: { pageTitle: 'Courses' },
        loadChildren: () => import('./courses/courses.module').then(m => m.CoursesModule),
      },
      {
        path: 'courses-subject',
        data: { pageTitle: 'CoursesSubjects' },
        loadChildren: () => import('./courses-subject/courses-subject.module').then(m => m.CoursesSubjectModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}
