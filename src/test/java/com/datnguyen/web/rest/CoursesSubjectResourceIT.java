package com.datnguyen.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.datnguyen.IntegrationTest;
import com.datnguyen.domain.CoursesSubject;
import com.datnguyen.repository.CoursesSubjectRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link CoursesSubjectResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class CoursesSubjectResourceIT {

    private static final String ENTITY_API_URL = "/api/courses-subjects";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CoursesSubjectRepository coursesSubjectRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCoursesSubjectMockMvc;

    private CoursesSubject coursesSubject;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CoursesSubject createEntity(EntityManager em) {
        CoursesSubject coursesSubject = new CoursesSubject();
        return coursesSubject;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CoursesSubject createUpdatedEntity(EntityManager em) {
        CoursesSubject coursesSubject = new CoursesSubject();
        return coursesSubject;
    }

    @BeforeEach
    public void initTest() {
        coursesSubject = createEntity(em);
    }

    @Test
    @Transactional
    void createCoursesSubject() throws Exception {
        int databaseSizeBeforeCreate = coursesSubjectRepository.findAll().size();
        // Create the CoursesSubject
        restCoursesSubjectMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(coursesSubject))
            )
            .andExpect(status().isCreated());

        // Validate the CoursesSubject in the database
        List<CoursesSubject> coursesSubjectList = coursesSubjectRepository.findAll();
        assertThat(coursesSubjectList).hasSize(databaseSizeBeforeCreate + 1);
        CoursesSubject testCoursesSubject = coursesSubjectList.get(coursesSubjectList.size() - 1);
    }

    @Test
    @Transactional
    void createCoursesSubjectWithExistingId() throws Exception {
        // Create the CoursesSubject with an existing ID
        coursesSubject.setId(1L);

        int databaseSizeBeforeCreate = coursesSubjectRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restCoursesSubjectMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(coursesSubject))
            )
            .andExpect(status().isBadRequest());

        // Validate the CoursesSubject in the database
        List<CoursesSubject> coursesSubjectList = coursesSubjectRepository.findAll();
        assertThat(coursesSubjectList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllCoursesSubjects() throws Exception {
        // Initialize the database
        coursesSubjectRepository.saveAndFlush(coursesSubject);

        // Get all the coursesSubjectList
        restCoursesSubjectMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(coursesSubject.getId().intValue())));
    }

    @Test
    @Transactional
    void getCoursesSubject() throws Exception {
        // Initialize the database
        coursesSubjectRepository.saveAndFlush(coursesSubject);

        // Get the coursesSubject
        restCoursesSubjectMockMvc
            .perform(get(ENTITY_API_URL_ID, coursesSubject.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(coursesSubject.getId().intValue()));
    }

    @Test
    @Transactional
    void getNonExistingCoursesSubject() throws Exception {
        // Get the coursesSubject
        restCoursesSubjectMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewCoursesSubject() throws Exception {
        // Initialize the database
        coursesSubjectRepository.saveAndFlush(coursesSubject);

        int databaseSizeBeforeUpdate = coursesSubjectRepository.findAll().size();

        // Update the coursesSubject
        CoursesSubject updatedCoursesSubject = coursesSubjectRepository.findById(coursesSubject.getId()).get();
        // Disconnect from session so that the updates on updatedCoursesSubject are not directly saved in db
        em.detach(updatedCoursesSubject);

        restCoursesSubjectMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedCoursesSubject.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedCoursesSubject))
            )
            .andExpect(status().isOk());

        // Validate the CoursesSubject in the database
        List<CoursesSubject> coursesSubjectList = coursesSubjectRepository.findAll();
        assertThat(coursesSubjectList).hasSize(databaseSizeBeforeUpdate);
        CoursesSubject testCoursesSubject = coursesSubjectList.get(coursesSubjectList.size() - 1);
    }

    @Test
    @Transactional
    void putNonExistingCoursesSubject() throws Exception {
        int databaseSizeBeforeUpdate = coursesSubjectRepository.findAll().size();
        coursesSubject.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCoursesSubjectMockMvc
            .perform(
                put(ENTITY_API_URL_ID, coursesSubject.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(coursesSubject))
            )
            .andExpect(status().isBadRequest());

        // Validate the CoursesSubject in the database
        List<CoursesSubject> coursesSubjectList = coursesSubjectRepository.findAll();
        assertThat(coursesSubjectList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchCoursesSubject() throws Exception {
        int databaseSizeBeforeUpdate = coursesSubjectRepository.findAll().size();
        coursesSubject.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCoursesSubjectMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(coursesSubject))
            )
            .andExpect(status().isBadRequest());

        // Validate the CoursesSubject in the database
        List<CoursesSubject> coursesSubjectList = coursesSubjectRepository.findAll();
        assertThat(coursesSubjectList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamCoursesSubject() throws Exception {
        int databaseSizeBeforeUpdate = coursesSubjectRepository.findAll().size();
        coursesSubject.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCoursesSubjectMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(coursesSubject)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the CoursesSubject in the database
        List<CoursesSubject> coursesSubjectList = coursesSubjectRepository.findAll();
        assertThat(coursesSubjectList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateCoursesSubjectWithPatch() throws Exception {
        // Initialize the database
        coursesSubjectRepository.saveAndFlush(coursesSubject);

        int databaseSizeBeforeUpdate = coursesSubjectRepository.findAll().size();

        // Update the coursesSubject using partial update
        CoursesSubject partialUpdatedCoursesSubject = new CoursesSubject();
        partialUpdatedCoursesSubject.setId(coursesSubject.getId());

        restCoursesSubjectMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCoursesSubject.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCoursesSubject))
            )
            .andExpect(status().isOk());

        // Validate the CoursesSubject in the database
        List<CoursesSubject> coursesSubjectList = coursesSubjectRepository.findAll();
        assertThat(coursesSubjectList).hasSize(databaseSizeBeforeUpdate);
        CoursesSubject testCoursesSubject = coursesSubjectList.get(coursesSubjectList.size() - 1);
    }

    @Test
    @Transactional
    void fullUpdateCoursesSubjectWithPatch() throws Exception {
        // Initialize the database
        coursesSubjectRepository.saveAndFlush(coursesSubject);

        int databaseSizeBeforeUpdate = coursesSubjectRepository.findAll().size();

        // Update the coursesSubject using partial update
        CoursesSubject partialUpdatedCoursesSubject = new CoursesSubject();
        partialUpdatedCoursesSubject.setId(coursesSubject.getId());

        restCoursesSubjectMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCoursesSubject.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCoursesSubject))
            )
            .andExpect(status().isOk());

        // Validate the CoursesSubject in the database
        List<CoursesSubject> coursesSubjectList = coursesSubjectRepository.findAll();
        assertThat(coursesSubjectList).hasSize(databaseSizeBeforeUpdate);
        CoursesSubject testCoursesSubject = coursesSubjectList.get(coursesSubjectList.size() - 1);
    }

    @Test
    @Transactional
    void patchNonExistingCoursesSubject() throws Exception {
        int databaseSizeBeforeUpdate = coursesSubjectRepository.findAll().size();
        coursesSubject.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCoursesSubjectMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, coursesSubject.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(coursesSubject))
            )
            .andExpect(status().isBadRequest());

        // Validate the CoursesSubject in the database
        List<CoursesSubject> coursesSubjectList = coursesSubjectRepository.findAll();
        assertThat(coursesSubjectList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchCoursesSubject() throws Exception {
        int databaseSizeBeforeUpdate = coursesSubjectRepository.findAll().size();
        coursesSubject.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCoursesSubjectMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(coursesSubject))
            )
            .andExpect(status().isBadRequest());

        // Validate the CoursesSubject in the database
        List<CoursesSubject> coursesSubjectList = coursesSubjectRepository.findAll();
        assertThat(coursesSubjectList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamCoursesSubject() throws Exception {
        int databaseSizeBeforeUpdate = coursesSubjectRepository.findAll().size();
        coursesSubject.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCoursesSubjectMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(coursesSubject))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the CoursesSubject in the database
        List<CoursesSubject> coursesSubjectList = coursesSubjectRepository.findAll();
        assertThat(coursesSubjectList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteCoursesSubject() throws Exception {
        // Initialize the database
        coursesSubjectRepository.saveAndFlush(coursesSubject);

        int databaseSizeBeforeDelete = coursesSubjectRepository.findAll().size();

        // Delete the coursesSubject
        restCoursesSubjectMockMvc
            .perform(delete(ENTITY_API_URL_ID, coursesSubject.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CoursesSubject> coursesSubjectList = coursesSubjectRepository.findAll();
        assertThat(coursesSubjectList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
