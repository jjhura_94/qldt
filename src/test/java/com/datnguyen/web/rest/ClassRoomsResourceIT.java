package com.datnguyen.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.datnguyen.IntegrationTest;
import com.datnguyen.domain.ClassRooms;
import com.datnguyen.repository.ClassRoomsRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ClassRoomsResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ClassRoomsResourceIT {

    private static final String DEFAULT_ROOM_NAME = "AAAAAAAAAA";
    private static final String UPDATED_ROOM_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_ROOM_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_ROOM_ADDRESS = "BBBBBBBBBB";

    private static final Long DEFAULT_TOTAL_SEATS = 1L;
    private static final Long UPDATED_TOTAL_SEATS = 2L;

    private static final String DEFAULT_ROOM_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_ROOM_DESCRIPTION = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/class-rooms";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ClassRoomsRepository classRoomsRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restClassRoomsMockMvc;

    private ClassRooms classRooms;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ClassRooms createEntity(EntityManager em) {
        ClassRooms classRooms = new ClassRooms()
            .roomName(DEFAULT_ROOM_NAME)
            .roomAddress(DEFAULT_ROOM_ADDRESS)
            .totalSeats(DEFAULT_TOTAL_SEATS)
            .roomDescription(DEFAULT_ROOM_DESCRIPTION);
        return classRooms;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ClassRooms createUpdatedEntity(EntityManager em) {
        ClassRooms classRooms = new ClassRooms()
            .roomName(UPDATED_ROOM_NAME)
            .roomAddress(UPDATED_ROOM_ADDRESS)
            .totalSeats(UPDATED_TOTAL_SEATS)
            .roomDescription(UPDATED_ROOM_DESCRIPTION);
        return classRooms;
    }

    @BeforeEach
    public void initTest() {
        classRooms = createEntity(em);
    }

    @Test
    @Transactional
    void createClassRooms() throws Exception {
        int databaseSizeBeforeCreate = classRoomsRepository.findAll().size();
        // Create the ClassRooms
        restClassRoomsMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(classRooms)))
            .andExpect(status().isCreated());

        // Validate the ClassRooms in the database
        List<ClassRooms> classRoomsList = classRoomsRepository.findAll();
        assertThat(classRoomsList).hasSize(databaseSizeBeforeCreate + 1);
        ClassRooms testClassRooms = classRoomsList.get(classRoomsList.size() - 1);
        assertThat(testClassRooms.getRoomName()).isEqualTo(DEFAULT_ROOM_NAME);
        assertThat(testClassRooms.getRoomAddress()).isEqualTo(DEFAULT_ROOM_ADDRESS);
        assertThat(testClassRooms.getTotalSeats()).isEqualTo(DEFAULT_TOTAL_SEATS);
        assertThat(testClassRooms.getRoomDescription()).isEqualTo(DEFAULT_ROOM_DESCRIPTION);
    }

    @Test
    @Transactional
    void createClassRoomsWithExistingId() throws Exception {
        // Create the ClassRooms with an existing ID
        classRooms.setId(1L);

        int databaseSizeBeforeCreate = classRoomsRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restClassRoomsMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(classRooms)))
            .andExpect(status().isBadRequest());

        // Validate the ClassRooms in the database
        List<ClassRooms> classRoomsList = classRoomsRepository.findAll();
        assertThat(classRoomsList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllClassRooms() throws Exception {
        // Initialize the database
        classRoomsRepository.saveAndFlush(classRooms);

        // Get all the classRoomsList
        restClassRoomsMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(classRooms.getId().intValue())))
            .andExpect(jsonPath("$.[*].roomName").value(hasItem(DEFAULT_ROOM_NAME)))
            .andExpect(jsonPath("$.[*].roomAddress").value(hasItem(DEFAULT_ROOM_ADDRESS)))
            .andExpect(jsonPath("$.[*].totalSeats").value(hasItem(DEFAULT_TOTAL_SEATS.intValue())))
            .andExpect(jsonPath("$.[*].roomDescription").value(hasItem(DEFAULT_ROOM_DESCRIPTION)));
    }

    @Test
    @Transactional
    void getClassRooms() throws Exception {
        // Initialize the database
        classRoomsRepository.saveAndFlush(classRooms);

        // Get the classRooms
        restClassRoomsMockMvc
            .perform(get(ENTITY_API_URL_ID, classRooms.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(classRooms.getId().intValue()))
            .andExpect(jsonPath("$.roomName").value(DEFAULT_ROOM_NAME))
            .andExpect(jsonPath("$.roomAddress").value(DEFAULT_ROOM_ADDRESS))
            .andExpect(jsonPath("$.totalSeats").value(DEFAULT_TOTAL_SEATS.intValue()))
            .andExpect(jsonPath("$.roomDescription").value(DEFAULT_ROOM_DESCRIPTION));
    }

    @Test
    @Transactional
    void getNonExistingClassRooms() throws Exception {
        // Get the classRooms
        restClassRoomsMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewClassRooms() throws Exception {
        // Initialize the database
        classRoomsRepository.saveAndFlush(classRooms);

        int databaseSizeBeforeUpdate = classRoomsRepository.findAll().size();

        // Update the classRooms
        ClassRooms updatedClassRooms = classRoomsRepository.findById(classRooms.getId()).get();
        // Disconnect from session so that the updates on updatedClassRooms are not directly saved in db
        em.detach(updatedClassRooms);
        updatedClassRooms
            .roomName(UPDATED_ROOM_NAME)
            .roomAddress(UPDATED_ROOM_ADDRESS)
            .totalSeats(UPDATED_TOTAL_SEATS)
            .roomDescription(UPDATED_ROOM_DESCRIPTION);

        restClassRoomsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedClassRooms.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedClassRooms))
            )
            .andExpect(status().isOk());

        // Validate the ClassRooms in the database
        List<ClassRooms> classRoomsList = classRoomsRepository.findAll();
        assertThat(classRoomsList).hasSize(databaseSizeBeforeUpdate);
        ClassRooms testClassRooms = classRoomsList.get(classRoomsList.size() - 1);
        assertThat(testClassRooms.getRoomName()).isEqualTo(UPDATED_ROOM_NAME);
        assertThat(testClassRooms.getRoomAddress()).isEqualTo(UPDATED_ROOM_ADDRESS);
        assertThat(testClassRooms.getTotalSeats()).isEqualTo(UPDATED_TOTAL_SEATS);
        assertThat(testClassRooms.getRoomDescription()).isEqualTo(UPDATED_ROOM_DESCRIPTION);
    }

    @Test
    @Transactional
    void putNonExistingClassRooms() throws Exception {
        int databaseSizeBeforeUpdate = classRoomsRepository.findAll().size();
        classRooms.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restClassRoomsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, classRooms.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(classRooms))
            )
            .andExpect(status().isBadRequest());

        // Validate the ClassRooms in the database
        List<ClassRooms> classRoomsList = classRoomsRepository.findAll();
        assertThat(classRoomsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchClassRooms() throws Exception {
        int databaseSizeBeforeUpdate = classRoomsRepository.findAll().size();
        classRooms.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restClassRoomsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(classRooms))
            )
            .andExpect(status().isBadRequest());

        // Validate the ClassRooms in the database
        List<ClassRooms> classRoomsList = classRoomsRepository.findAll();
        assertThat(classRoomsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamClassRooms() throws Exception {
        int databaseSizeBeforeUpdate = classRoomsRepository.findAll().size();
        classRooms.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restClassRoomsMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(classRooms)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the ClassRooms in the database
        List<ClassRooms> classRoomsList = classRoomsRepository.findAll();
        assertThat(classRoomsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateClassRoomsWithPatch() throws Exception {
        // Initialize the database
        classRoomsRepository.saveAndFlush(classRooms);

        int databaseSizeBeforeUpdate = classRoomsRepository.findAll().size();

        // Update the classRooms using partial update
        ClassRooms partialUpdatedClassRooms = new ClassRooms();
        partialUpdatedClassRooms.setId(classRooms.getId());

        partialUpdatedClassRooms.roomAddress(UPDATED_ROOM_ADDRESS);

        restClassRoomsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedClassRooms.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedClassRooms))
            )
            .andExpect(status().isOk());

        // Validate the ClassRooms in the database
        List<ClassRooms> classRoomsList = classRoomsRepository.findAll();
        assertThat(classRoomsList).hasSize(databaseSizeBeforeUpdate);
        ClassRooms testClassRooms = classRoomsList.get(classRoomsList.size() - 1);
        assertThat(testClassRooms.getRoomName()).isEqualTo(DEFAULT_ROOM_NAME);
        assertThat(testClassRooms.getRoomAddress()).isEqualTo(UPDATED_ROOM_ADDRESS);
        assertThat(testClassRooms.getTotalSeats()).isEqualTo(DEFAULT_TOTAL_SEATS);
        assertThat(testClassRooms.getRoomDescription()).isEqualTo(DEFAULT_ROOM_DESCRIPTION);
    }

    @Test
    @Transactional
    void fullUpdateClassRoomsWithPatch() throws Exception {
        // Initialize the database
        classRoomsRepository.saveAndFlush(classRooms);

        int databaseSizeBeforeUpdate = classRoomsRepository.findAll().size();

        // Update the classRooms using partial update
        ClassRooms partialUpdatedClassRooms = new ClassRooms();
        partialUpdatedClassRooms.setId(classRooms.getId());

        partialUpdatedClassRooms
            .roomName(UPDATED_ROOM_NAME)
            .roomAddress(UPDATED_ROOM_ADDRESS)
            .totalSeats(UPDATED_TOTAL_SEATS)
            .roomDescription(UPDATED_ROOM_DESCRIPTION);

        restClassRoomsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedClassRooms.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedClassRooms))
            )
            .andExpect(status().isOk());

        // Validate the ClassRooms in the database
        List<ClassRooms> classRoomsList = classRoomsRepository.findAll();
        assertThat(classRoomsList).hasSize(databaseSizeBeforeUpdate);
        ClassRooms testClassRooms = classRoomsList.get(classRoomsList.size() - 1);
        assertThat(testClassRooms.getRoomName()).isEqualTo(UPDATED_ROOM_NAME);
        assertThat(testClassRooms.getRoomAddress()).isEqualTo(UPDATED_ROOM_ADDRESS);
        assertThat(testClassRooms.getTotalSeats()).isEqualTo(UPDATED_TOTAL_SEATS);
        assertThat(testClassRooms.getRoomDescription()).isEqualTo(UPDATED_ROOM_DESCRIPTION);
    }

    @Test
    @Transactional
    void patchNonExistingClassRooms() throws Exception {
        int databaseSizeBeforeUpdate = classRoomsRepository.findAll().size();
        classRooms.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restClassRoomsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, classRooms.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(classRooms))
            )
            .andExpect(status().isBadRequest());

        // Validate the ClassRooms in the database
        List<ClassRooms> classRoomsList = classRoomsRepository.findAll();
        assertThat(classRoomsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchClassRooms() throws Exception {
        int databaseSizeBeforeUpdate = classRoomsRepository.findAll().size();
        classRooms.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restClassRoomsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(classRooms))
            )
            .andExpect(status().isBadRequest());

        // Validate the ClassRooms in the database
        List<ClassRooms> classRoomsList = classRoomsRepository.findAll();
        assertThat(classRoomsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamClassRooms() throws Exception {
        int databaseSizeBeforeUpdate = classRoomsRepository.findAll().size();
        classRooms.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restClassRoomsMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(classRooms))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ClassRooms in the database
        List<ClassRooms> classRoomsList = classRoomsRepository.findAll();
        assertThat(classRoomsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteClassRooms() throws Exception {
        // Initialize the database
        classRoomsRepository.saveAndFlush(classRooms);

        int databaseSizeBeforeDelete = classRoomsRepository.findAll().size();

        // Delete the classRooms
        restClassRoomsMockMvc
            .perform(delete(ENTITY_API_URL_ID, classRooms.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ClassRooms> classRoomsList = classRoomsRepository.findAll();
        assertThat(classRoomsList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
