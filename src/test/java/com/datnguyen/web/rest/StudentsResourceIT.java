package com.datnguyen.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.datnguyen.IntegrationTest;
import com.datnguyen.domain.Students;
import com.datnguyen.repository.StudentsRepository;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link StudentsResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class StudentsResourceIT {

    private static final String DEFAULT_STU_CODE = "AAAAAAAAAA";
    private static final String UPDATED_STU_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_STU_NAME = "AAAAAAAAAA";
    private static final String UPDATED_STU_NAME = "BBBBBBBBBB";

    private static final Instant DEFAULT_STU_DOB = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_STU_DOB = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_STU_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_STU_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_STU_PHONE = "AAAAAAAAAA";
    private static final String UPDATED_STU_PHONE = "BBBBBBBBBB";

    private static final String DEFAULT_STU_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_STU_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_STU_GENDER = "AAAAAAAAAA";
    private static final String UPDATED_STU_GENDER = "BBBBBBBBBB";

    private static final String DEFAULT_STU_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STU_STATUS = "BBBBBBBBBB";

    private static final Instant DEFAULT_STU_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_STU_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String ENTITY_API_URL = "/api/students";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private StudentsRepository studentsRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restStudentsMockMvc;

    private Students students;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Students createEntity(EntityManager em) {
        Students students = new Students()
            .stuCode(DEFAULT_STU_CODE)
            .stuName(DEFAULT_STU_NAME)
            .stuDob(DEFAULT_STU_DOB)
            .stuEmail(DEFAULT_STU_EMAIL)
            .stuPhone(DEFAULT_STU_PHONE)
            .stuAddress(DEFAULT_STU_ADDRESS)
            .stuGender(DEFAULT_STU_GENDER)
            .stuStatus(DEFAULT_STU_STATUS)
            .stuDate(DEFAULT_STU_DATE);
        return students;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Students createUpdatedEntity(EntityManager em) {
        Students students = new Students()
            .stuCode(UPDATED_STU_CODE)
            .stuName(UPDATED_STU_NAME)
            .stuDob(UPDATED_STU_DOB)
            .stuEmail(UPDATED_STU_EMAIL)
            .stuPhone(UPDATED_STU_PHONE)
            .stuAddress(UPDATED_STU_ADDRESS)
            .stuGender(UPDATED_STU_GENDER)
            .stuStatus(UPDATED_STU_STATUS)
            .stuDate(UPDATED_STU_DATE);
        return students;
    }

    @BeforeEach
    public void initTest() {
        students = createEntity(em);
    }

    @Test
    @Transactional
    void createStudents() throws Exception {
        int databaseSizeBeforeCreate = studentsRepository.findAll().size();
        // Create the Students
        restStudentsMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(students)))
            .andExpect(status().isCreated());

        // Validate the Students in the database
        List<Students> studentsList = studentsRepository.findAll();
        assertThat(studentsList).hasSize(databaseSizeBeforeCreate + 1);
        Students testStudents = studentsList.get(studentsList.size() - 1);
        assertThat(testStudents.getStuCode()).isEqualTo(DEFAULT_STU_CODE);
        assertThat(testStudents.getStuName()).isEqualTo(DEFAULT_STU_NAME);
        assertThat(testStudents.getStuDob()).isEqualTo(DEFAULT_STU_DOB);
        assertThat(testStudents.getStuEmail()).isEqualTo(DEFAULT_STU_EMAIL);
        assertThat(testStudents.getStuPhone()).isEqualTo(DEFAULT_STU_PHONE);
        assertThat(testStudents.getStuAddress()).isEqualTo(DEFAULT_STU_ADDRESS);
        assertThat(testStudents.getStuGender()).isEqualTo(DEFAULT_STU_GENDER);
        assertThat(testStudents.getStuStatus()).isEqualTo(DEFAULT_STU_STATUS);
        assertThat(testStudents.getStuDate()).isEqualTo(DEFAULT_STU_DATE);
    }

    @Test
    @Transactional
    void createStudentsWithExistingId() throws Exception {
        // Create the Students with an existing ID
        students.setId(1L);

        int databaseSizeBeforeCreate = studentsRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restStudentsMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(students)))
            .andExpect(status().isBadRequest());

        // Validate the Students in the database
        List<Students> studentsList = studentsRepository.findAll();
        assertThat(studentsList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllStudents() throws Exception {
        // Initialize the database
        studentsRepository.saveAndFlush(students);

        // Get all the studentsList
        restStudentsMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(students.getId().intValue())))
            .andExpect(jsonPath("$.[*].stuCode").value(hasItem(DEFAULT_STU_CODE)))
            .andExpect(jsonPath("$.[*].stuName").value(hasItem(DEFAULT_STU_NAME)))
            .andExpect(jsonPath("$.[*].stuDob").value(hasItem(DEFAULT_STU_DOB.toString())))
            .andExpect(jsonPath("$.[*].stuEmail").value(hasItem(DEFAULT_STU_EMAIL)))
            .andExpect(jsonPath("$.[*].stuPhone").value(hasItem(DEFAULT_STU_PHONE)))
            .andExpect(jsonPath("$.[*].stuAddress").value(hasItem(DEFAULT_STU_ADDRESS)))
            .andExpect(jsonPath("$.[*].stuGender").value(hasItem(DEFAULT_STU_GENDER)))
            .andExpect(jsonPath("$.[*].stuStatus").value(hasItem(DEFAULT_STU_STATUS)))
            .andExpect(jsonPath("$.[*].stuDate").value(hasItem(DEFAULT_STU_DATE.toString())));
    }

    @Test
    @Transactional
    void getStudents() throws Exception {
        // Initialize the database
        studentsRepository.saveAndFlush(students);

        // Get the students
        restStudentsMockMvc
            .perform(get(ENTITY_API_URL_ID, students.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(students.getId().intValue()))
            .andExpect(jsonPath("$.stuCode").value(DEFAULT_STU_CODE))
            .andExpect(jsonPath("$.stuName").value(DEFAULT_STU_NAME))
            .andExpect(jsonPath("$.stuDob").value(DEFAULT_STU_DOB.toString()))
            .andExpect(jsonPath("$.stuEmail").value(DEFAULT_STU_EMAIL))
            .andExpect(jsonPath("$.stuPhone").value(DEFAULT_STU_PHONE))
            .andExpect(jsonPath("$.stuAddress").value(DEFAULT_STU_ADDRESS))
            .andExpect(jsonPath("$.stuGender").value(DEFAULT_STU_GENDER))
            .andExpect(jsonPath("$.stuStatus").value(DEFAULT_STU_STATUS))
            .andExpect(jsonPath("$.stuDate").value(DEFAULT_STU_DATE.toString()));
    }

    @Test
    @Transactional
    void getNonExistingStudents() throws Exception {
        // Get the students
        restStudentsMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewStudents() throws Exception {
        // Initialize the database
        studentsRepository.saveAndFlush(students);

        int databaseSizeBeforeUpdate = studentsRepository.findAll().size();

        // Update the students
        Students updatedStudents = studentsRepository.findById(students.getId()).get();
        // Disconnect from session so that the updates on updatedStudents are not directly saved in db
        em.detach(updatedStudents);
        updatedStudents
            .stuCode(UPDATED_STU_CODE)
            .stuName(UPDATED_STU_NAME)
            .stuDob(UPDATED_STU_DOB)
            .stuEmail(UPDATED_STU_EMAIL)
            .stuPhone(UPDATED_STU_PHONE)
            .stuAddress(UPDATED_STU_ADDRESS)
            .stuGender(UPDATED_STU_GENDER)
            .stuStatus(UPDATED_STU_STATUS)
            .stuDate(UPDATED_STU_DATE);

        restStudentsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedStudents.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedStudents))
            )
            .andExpect(status().isOk());

        // Validate the Students in the database
        List<Students> studentsList = studentsRepository.findAll();
        assertThat(studentsList).hasSize(databaseSizeBeforeUpdate);
        Students testStudents = studentsList.get(studentsList.size() - 1);
        assertThat(testStudents.getStuCode()).isEqualTo(UPDATED_STU_CODE);
        assertThat(testStudents.getStuName()).isEqualTo(UPDATED_STU_NAME);
        assertThat(testStudents.getStuDob()).isEqualTo(UPDATED_STU_DOB);
        assertThat(testStudents.getStuEmail()).isEqualTo(UPDATED_STU_EMAIL);
        assertThat(testStudents.getStuPhone()).isEqualTo(UPDATED_STU_PHONE);
        assertThat(testStudents.getStuAddress()).isEqualTo(UPDATED_STU_ADDRESS);
        assertThat(testStudents.getStuGender()).isEqualTo(UPDATED_STU_GENDER);
        assertThat(testStudents.getStuStatus()).isEqualTo(UPDATED_STU_STATUS);
        assertThat(testStudents.getStuDate()).isEqualTo(UPDATED_STU_DATE);
    }

    @Test
    @Transactional
    void putNonExistingStudents() throws Exception {
        int databaseSizeBeforeUpdate = studentsRepository.findAll().size();
        students.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restStudentsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, students.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(students))
            )
            .andExpect(status().isBadRequest());

        // Validate the Students in the database
        List<Students> studentsList = studentsRepository.findAll();
        assertThat(studentsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchStudents() throws Exception {
        int databaseSizeBeforeUpdate = studentsRepository.findAll().size();
        students.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restStudentsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(students))
            )
            .andExpect(status().isBadRequest());

        // Validate the Students in the database
        List<Students> studentsList = studentsRepository.findAll();
        assertThat(studentsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamStudents() throws Exception {
        int databaseSizeBeforeUpdate = studentsRepository.findAll().size();
        students.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restStudentsMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(students)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Students in the database
        List<Students> studentsList = studentsRepository.findAll();
        assertThat(studentsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateStudentsWithPatch() throws Exception {
        // Initialize the database
        studentsRepository.saveAndFlush(students);

        int databaseSizeBeforeUpdate = studentsRepository.findAll().size();

        // Update the students using partial update
        Students partialUpdatedStudents = new Students();
        partialUpdatedStudents.setId(students.getId());

        partialUpdatedStudents
            .stuCode(UPDATED_STU_CODE)
            .stuName(UPDATED_STU_NAME)
            .stuDob(UPDATED_STU_DOB)
            .stuEmail(UPDATED_STU_EMAIL)
            .stuPhone(UPDATED_STU_PHONE)
            .stuStatus(UPDATED_STU_STATUS);

        restStudentsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedStudents.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedStudents))
            )
            .andExpect(status().isOk());

        // Validate the Students in the database
        List<Students> studentsList = studentsRepository.findAll();
        assertThat(studentsList).hasSize(databaseSizeBeforeUpdate);
        Students testStudents = studentsList.get(studentsList.size() - 1);
        assertThat(testStudents.getStuCode()).isEqualTo(UPDATED_STU_CODE);
        assertThat(testStudents.getStuName()).isEqualTo(UPDATED_STU_NAME);
        assertThat(testStudents.getStuDob()).isEqualTo(UPDATED_STU_DOB);
        assertThat(testStudents.getStuEmail()).isEqualTo(UPDATED_STU_EMAIL);
        assertThat(testStudents.getStuPhone()).isEqualTo(UPDATED_STU_PHONE);
        assertThat(testStudents.getStuAddress()).isEqualTo(DEFAULT_STU_ADDRESS);
        assertThat(testStudents.getStuGender()).isEqualTo(DEFAULT_STU_GENDER);
        assertThat(testStudents.getStuStatus()).isEqualTo(UPDATED_STU_STATUS);
        assertThat(testStudents.getStuDate()).isEqualTo(DEFAULT_STU_DATE);
    }

    @Test
    @Transactional
    void fullUpdateStudentsWithPatch() throws Exception {
        // Initialize the database
        studentsRepository.saveAndFlush(students);

        int databaseSizeBeforeUpdate = studentsRepository.findAll().size();

        // Update the students using partial update
        Students partialUpdatedStudents = new Students();
        partialUpdatedStudents.setId(students.getId());

        partialUpdatedStudents
            .stuCode(UPDATED_STU_CODE)
            .stuName(UPDATED_STU_NAME)
            .stuDob(UPDATED_STU_DOB)
            .stuEmail(UPDATED_STU_EMAIL)
            .stuPhone(UPDATED_STU_PHONE)
            .stuAddress(UPDATED_STU_ADDRESS)
            .stuGender(UPDATED_STU_GENDER)
            .stuStatus(UPDATED_STU_STATUS)
            .stuDate(UPDATED_STU_DATE);

        restStudentsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedStudents.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedStudents))
            )
            .andExpect(status().isOk());

        // Validate the Students in the database
        List<Students> studentsList = studentsRepository.findAll();
        assertThat(studentsList).hasSize(databaseSizeBeforeUpdate);
        Students testStudents = studentsList.get(studentsList.size() - 1);
        assertThat(testStudents.getStuCode()).isEqualTo(UPDATED_STU_CODE);
        assertThat(testStudents.getStuName()).isEqualTo(UPDATED_STU_NAME);
        assertThat(testStudents.getStuDob()).isEqualTo(UPDATED_STU_DOB);
        assertThat(testStudents.getStuEmail()).isEqualTo(UPDATED_STU_EMAIL);
        assertThat(testStudents.getStuPhone()).isEqualTo(UPDATED_STU_PHONE);
        assertThat(testStudents.getStuAddress()).isEqualTo(UPDATED_STU_ADDRESS);
        assertThat(testStudents.getStuGender()).isEqualTo(UPDATED_STU_GENDER);
        assertThat(testStudents.getStuStatus()).isEqualTo(UPDATED_STU_STATUS);
        assertThat(testStudents.getStuDate()).isEqualTo(UPDATED_STU_DATE);
    }

    @Test
    @Transactional
    void patchNonExistingStudents() throws Exception {
        int databaseSizeBeforeUpdate = studentsRepository.findAll().size();
        students.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restStudentsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, students.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(students))
            )
            .andExpect(status().isBadRequest());

        // Validate the Students in the database
        List<Students> studentsList = studentsRepository.findAll();
        assertThat(studentsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchStudents() throws Exception {
        int databaseSizeBeforeUpdate = studentsRepository.findAll().size();
        students.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restStudentsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(students))
            )
            .andExpect(status().isBadRequest());

        // Validate the Students in the database
        List<Students> studentsList = studentsRepository.findAll();
        assertThat(studentsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamStudents() throws Exception {
        int databaseSizeBeforeUpdate = studentsRepository.findAll().size();
        students.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restStudentsMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(students)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Students in the database
        List<Students> studentsList = studentsRepository.findAll();
        assertThat(studentsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteStudents() throws Exception {
        // Initialize the database
        studentsRepository.saveAndFlush(students);

        int databaseSizeBeforeDelete = studentsRepository.findAll().size();

        // Delete the students
        restStudentsMockMvc
            .perform(delete(ENTITY_API_URL_ID, students.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Students> studentsList = studentsRepository.findAll();
        assertThat(studentsList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
