package com.datnguyen.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.datnguyen.IntegrationTest;
import com.datnguyen.domain.PositionSalary;
import com.datnguyen.repository.PositionSalaryRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link PositionSalaryResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class PositionSalaryResourceIT {

    private static final String DEFAULT_POS_CODE = "AAAAAAAAAA";
    private static final String UPDATED_POS_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_POS_NAME = "AAAAAAAAAA";
    private static final String UPDATED_POS_NAME = "BBBBBBBBBB";

    private static final Long DEFAULT_SAL_BASE = 1L;
    private static final Long UPDATED_SAL_BASE = 2L;

    private static final Long DEFAULT_SAL_ALLOWANCE = 1L;
    private static final Long UPDATED_SAL_ALLOWANCE = 2L;

    private static final String ENTITY_API_URL = "/api/position-salaries";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PositionSalaryRepository positionSalaryRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPositionSalaryMockMvc;

    private PositionSalary positionSalary;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PositionSalary createEntity(EntityManager em) {
        PositionSalary positionSalary = new PositionSalary()
            .posCode(DEFAULT_POS_CODE)
            .posName(DEFAULT_POS_NAME)
            .salBase(DEFAULT_SAL_BASE)
            .salAllowance(DEFAULT_SAL_ALLOWANCE);
        return positionSalary;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PositionSalary createUpdatedEntity(EntityManager em) {
        PositionSalary positionSalary = new PositionSalary()
            .posCode(UPDATED_POS_CODE)
            .posName(UPDATED_POS_NAME)
            .salBase(UPDATED_SAL_BASE)
            .salAllowance(UPDATED_SAL_ALLOWANCE);
        return positionSalary;
    }

    @BeforeEach
    public void initTest() {
        positionSalary = createEntity(em);
    }

    @Test
    @Transactional
    void createPositionSalary() throws Exception {
        int databaseSizeBeforeCreate = positionSalaryRepository.findAll().size();
        // Create the PositionSalary
        restPositionSalaryMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(positionSalary))
            )
            .andExpect(status().isCreated());

        // Validate the PositionSalary in the database
        List<PositionSalary> positionSalaryList = positionSalaryRepository.findAll();
        assertThat(positionSalaryList).hasSize(databaseSizeBeforeCreate + 1);
        PositionSalary testPositionSalary = positionSalaryList.get(positionSalaryList.size() - 1);
        assertThat(testPositionSalary.getPosCode()).isEqualTo(DEFAULT_POS_CODE);
        assertThat(testPositionSalary.getPosName()).isEqualTo(DEFAULT_POS_NAME);
        assertThat(testPositionSalary.getSalBase()).isEqualTo(DEFAULT_SAL_BASE);
        assertThat(testPositionSalary.getSalAllowance()).isEqualTo(DEFAULT_SAL_ALLOWANCE);
    }

    @Test
    @Transactional
    void createPositionSalaryWithExistingId() throws Exception {
        // Create the PositionSalary with an existing ID
        positionSalary.setId(1L);

        int databaseSizeBeforeCreate = positionSalaryRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restPositionSalaryMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(positionSalary))
            )
            .andExpect(status().isBadRequest());

        // Validate the PositionSalary in the database
        List<PositionSalary> positionSalaryList = positionSalaryRepository.findAll();
        assertThat(positionSalaryList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllPositionSalaries() throws Exception {
        // Initialize the database
        positionSalaryRepository.saveAndFlush(positionSalary);

        // Get all the positionSalaryList
        restPositionSalaryMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(positionSalary.getId().intValue())))
            .andExpect(jsonPath("$.[*].posCode").value(hasItem(DEFAULT_POS_CODE)))
            .andExpect(jsonPath("$.[*].posName").value(hasItem(DEFAULT_POS_NAME)))
            .andExpect(jsonPath("$.[*].salBase").value(hasItem(DEFAULT_SAL_BASE.intValue())))
            .andExpect(jsonPath("$.[*].salAllowance").value(hasItem(DEFAULT_SAL_ALLOWANCE.intValue())));
    }

    @Test
    @Transactional
    void getPositionSalary() throws Exception {
        // Initialize the database
        positionSalaryRepository.saveAndFlush(positionSalary);

        // Get the positionSalary
        restPositionSalaryMockMvc
            .perform(get(ENTITY_API_URL_ID, positionSalary.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(positionSalary.getId().intValue()))
            .andExpect(jsonPath("$.posCode").value(DEFAULT_POS_CODE))
            .andExpect(jsonPath("$.posName").value(DEFAULT_POS_NAME))
            .andExpect(jsonPath("$.salBase").value(DEFAULT_SAL_BASE.intValue()))
            .andExpect(jsonPath("$.salAllowance").value(DEFAULT_SAL_ALLOWANCE.intValue()));
    }

    @Test
    @Transactional
    void getNonExistingPositionSalary() throws Exception {
        // Get the positionSalary
        restPositionSalaryMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewPositionSalary() throws Exception {
        // Initialize the database
        positionSalaryRepository.saveAndFlush(positionSalary);

        int databaseSizeBeforeUpdate = positionSalaryRepository.findAll().size();

        // Update the positionSalary
        PositionSalary updatedPositionSalary = positionSalaryRepository.findById(positionSalary.getId()).get();
        // Disconnect from session so that the updates on updatedPositionSalary are not directly saved in db
        em.detach(updatedPositionSalary);
        updatedPositionSalary
            .posCode(UPDATED_POS_CODE)
            .posName(UPDATED_POS_NAME)
            .salBase(UPDATED_SAL_BASE)
            .salAllowance(UPDATED_SAL_ALLOWANCE);

        restPositionSalaryMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedPositionSalary.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedPositionSalary))
            )
            .andExpect(status().isOk());

        // Validate the PositionSalary in the database
        List<PositionSalary> positionSalaryList = positionSalaryRepository.findAll();
        assertThat(positionSalaryList).hasSize(databaseSizeBeforeUpdate);
        PositionSalary testPositionSalary = positionSalaryList.get(positionSalaryList.size() - 1);
        assertThat(testPositionSalary.getPosCode()).isEqualTo(UPDATED_POS_CODE);
        assertThat(testPositionSalary.getPosName()).isEqualTo(UPDATED_POS_NAME);
        assertThat(testPositionSalary.getSalBase()).isEqualTo(UPDATED_SAL_BASE);
        assertThat(testPositionSalary.getSalAllowance()).isEqualTo(UPDATED_SAL_ALLOWANCE);
    }

    @Test
    @Transactional
    void putNonExistingPositionSalary() throws Exception {
        int databaseSizeBeforeUpdate = positionSalaryRepository.findAll().size();
        positionSalary.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPositionSalaryMockMvc
            .perform(
                put(ENTITY_API_URL_ID, positionSalary.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(positionSalary))
            )
            .andExpect(status().isBadRequest());

        // Validate the PositionSalary in the database
        List<PositionSalary> positionSalaryList = positionSalaryRepository.findAll();
        assertThat(positionSalaryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchPositionSalary() throws Exception {
        int databaseSizeBeforeUpdate = positionSalaryRepository.findAll().size();
        positionSalary.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPositionSalaryMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(positionSalary))
            )
            .andExpect(status().isBadRequest());

        // Validate the PositionSalary in the database
        List<PositionSalary> positionSalaryList = positionSalaryRepository.findAll();
        assertThat(positionSalaryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamPositionSalary() throws Exception {
        int databaseSizeBeforeUpdate = positionSalaryRepository.findAll().size();
        positionSalary.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPositionSalaryMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(positionSalary)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the PositionSalary in the database
        List<PositionSalary> positionSalaryList = positionSalaryRepository.findAll();
        assertThat(positionSalaryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdatePositionSalaryWithPatch() throws Exception {
        // Initialize the database
        positionSalaryRepository.saveAndFlush(positionSalary);

        int databaseSizeBeforeUpdate = positionSalaryRepository.findAll().size();

        // Update the positionSalary using partial update
        PositionSalary partialUpdatedPositionSalary = new PositionSalary();
        partialUpdatedPositionSalary.setId(positionSalary.getId());

        partialUpdatedPositionSalary.posName(UPDATED_POS_NAME);

        restPositionSalaryMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPositionSalary.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPositionSalary))
            )
            .andExpect(status().isOk());

        // Validate the PositionSalary in the database
        List<PositionSalary> positionSalaryList = positionSalaryRepository.findAll();
        assertThat(positionSalaryList).hasSize(databaseSizeBeforeUpdate);
        PositionSalary testPositionSalary = positionSalaryList.get(positionSalaryList.size() - 1);
        assertThat(testPositionSalary.getPosCode()).isEqualTo(DEFAULT_POS_CODE);
        assertThat(testPositionSalary.getPosName()).isEqualTo(UPDATED_POS_NAME);
        assertThat(testPositionSalary.getSalBase()).isEqualTo(DEFAULT_SAL_BASE);
        assertThat(testPositionSalary.getSalAllowance()).isEqualTo(DEFAULT_SAL_ALLOWANCE);
    }

    @Test
    @Transactional
    void fullUpdatePositionSalaryWithPatch() throws Exception {
        // Initialize the database
        positionSalaryRepository.saveAndFlush(positionSalary);

        int databaseSizeBeforeUpdate = positionSalaryRepository.findAll().size();

        // Update the positionSalary using partial update
        PositionSalary partialUpdatedPositionSalary = new PositionSalary();
        partialUpdatedPositionSalary.setId(positionSalary.getId());

        partialUpdatedPositionSalary
            .posCode(UPDATED_POS_CODE)
            .posName(UPDATED_POS_NAME)
            .salBase(UPDATED_SAL_BASE)
            .salAllowance(UPDATED_SAL_ALLOWANCE);

        restPositionSalaryMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPositionSalary.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPositionSalary))
            )
            .andExpect(status().isOk());

        // Validate the PositionSalary in the database
        List<PositionSalary> positionSalaryList = positionSalaryRepository.findAll();
        assertThat(positionSalaryList).hasSize(databaseSizeBeforeUpdate);
        PositionSalary testPositionSalary = positionSalaryList.get(positionSalaryList.size() - 1);
        assertThat(testPositionSalary.getPosCode()).isEqualTo(UPDATED_POS_CODE);
        assertThat(testPositionSalary.getPosName()).isEqualTo(UPDATED_POS_NAME);
        assertThat(testPositionSalary.getSalBase()).isEqualTo(UPDATED_SAL_BASE);
        assertThat(testPositionSalary.getSalAllowance()).isEqualTo(UPDATED_SAL_ALLOWANCE);
    }

    @Test
    @Transactional
    void patchNonExistingPositionSalary() throws Exception {
        int databaseSizeBeforeUpdate = positionSalaryRepository.findAll().size();
        positionSalary.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPositionSalaryMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, positionSalary.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(positionSalary))
            )
            .andExpect(status().isBadRequest());

        // Validate the PositionSalary in the database
        List<PositionSalary> positionSalaryList = positionSalaryRepository.findAll();
        assertThat(positionSalaryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchPositionSalary() throws Exception {
        int databaseSizeBeforeUpdate = positionSalaryRepository.findAll().size();
        positionSalary.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPositionSalaryMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(positionSalary))
            )
            .andExpect(status().isBadRequest());

        // Validate the PositionSalary in the database
        List<PositionSalary> positionSalaryList = positionSalaryRepository.findAll();
        assertThat(positionSalaryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamPositionSalary() throws Exception {
        int databaseSizeBeforeUpdate = positionSalaryRepository.findAll().size();
        positionSalary.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPositionSalaryMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(positionSalary))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the PositionSalary in the database
        List<PositionSalary> positionSalaryList = positionSalaryRepository.findAll();
        assertThat(positionSalaryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deletePositionSalary() throws Exception {
        // Initialize the database
        positionSalaryRepository.saveAndFlush(positionSalary);

        int databaseSizeBeforeDelete = positionSalaryRepository.findAll().size();

        // Delete the positionSalary
        restPositionSalaryMockMvc
            .perform(delete(ENTITY_API_URL_ID, positionSalary.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PositionSalary> positionSalaryList = positionSalaryRepository.findAll();
        assertThat(positionSalaryList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
