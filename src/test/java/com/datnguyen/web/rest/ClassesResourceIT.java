package com.datnguyen.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.datnguyen.IntegrationTest;
import com.datnguyen.domain.Classes;
import com.datnguyen.repository.ClassesRepository;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ClassesResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ClassesResourceIT {

    private static final String DEFAULT_CLASS_NAME = "AAAAAAAAAA";
    private static final String UPDATED_CLASS_NAME = "BBBBBBBBBB";

    private static final Instant DEFAULT_START_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_START_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_END_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_END_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_START_END_TIME = "AAAAAAAAAA";
    private static final String UPDATED_START_END_TIME = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/classes";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ClassesRepository classesRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restClassesMockMvc;

    private Classes classes;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Classes createEntity(EntityManager em) {
        Classes classes = new Classes()
            .className(DEFAULT_CLASS_NAME)
            .startDate(DEFAULT_START_DATE)
            .endDate(DEFAULT_END_DATE)
            .startEndTime(DEFAULT_START_END_TIME);
        return classes;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Classes createUpdatedEntity(EntityManager em) {
        Classes classes = new Classes()
            .className(UPDATED_CLASS_NAME)
            .startDate(UPDATED_START_DATE)
            .endDate(UPDATED_END_DATE)
            .startEndTime(UPDATED_START_END_TIME);
        return classes;
    }

    @BeforeEach
    public void initTest() {
        classes = createEntity(em);
    }

    @Test
    @Transactional
    void createClasses() throws Exception {
        int databaseSizeBeforeCreate = classesRepository.findAll().size();
        // Create the Classes
        restClassesMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(classes)))
            .andExpect(status().isCreated());

        // Validate the Classes in the database
        List<Classes> classesList = classesRepository.findAll();
        assertThat(classesList).hasSize(databaseSizeBeforeCreate + 1);
        Classes testClasses = classesList.get(classesList.size() - 1);
        assertThat(testClasses.getClassName()).isEqualTo(DEFAULT_CLASS_NAME);
        assertThat(testClasses.getStartDate()).isEqualTo(DEFAULT_START_DATE);
        assertThat(testClasses.getEndDate()).isEqualTo(DEFAULT_END_DATE);
        assertThat(testClasses.getStartEndTime()).isEqualTo(DEFAULT_START_END_TIME);
    }

    @Test
    @Transactional
    void createClassesWithExistingId() throws Exception {
        // Create the Classes with an existing ID
        classes.setId(1L);

        int databaseSizeBeforeCreate = classesRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restClassesMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(classes)))
            .andExpect(status().isBadRequest());

        // Validate the Classes in the database
        List<Classes> classesList = classesRepository.findAll();
        assertThat(classesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllClasses() throws Exception {
        // Initialize the database
        classesRepository.saveAndFlush(classes);

        // Get all the classesList
        restClassesMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(classes.getId().intValue())))
            .andExpect(jsonPath("$.[*].className").value(hasItem(DEFAULT_CLASS_NAME)))
            .andExpect(jsonPath("$.[*].startDate").value(hasItem(DEFAULT_START_DATE.toString())))
            .andExpect(jsonPath("$.[*].endDate").value(hasItem(DEFAULT_END_DATE.toString())))
            .andExpect(jsonPath("$.[*].startEndTime").value(hasItem(DEFAULT_START_END_TIME)));
    }

    @Test
    @Transactional
    void getClasses() throws Exception {
        // Initialize the database
        classesRepository.saveAndFlush(classes);

        // Get the classes
        restClassesMockMvc
            .perform(get(ENTITY_API_URL_ID, classes.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(classes.getId().intValue()))
            .andExpect(jsonPath("$.className").value(DEFAULT_CLASS_NAME))
            .andExpect(jsonPath("$.startDate").value(DEFAULT_START_DATE.toString()))
            .andExpect(jsonPath("$.endDate").value(DEFAULT_END_DATE.toString()))
            .andExpect(jsonPath("$.startEndTime").value(DEFAULT_START_END_TIME));
    }

    @Test
    @Transactional
    void getNonExistingClasses() throws Exception {
        // Get the classes
        restClassesMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewClasses() throws Exception {
        // Initialize the database
        classesRepository.saveAndFlush(classes);

        int databaseSizeBeforeUpdate = classesRepository.findAll().size();

        // Update the classes
        Classes updatedClasses = classesRepository.findById(classes.getId()).get();
        // Disconnect from session so that the updates on updatedClasses are not directly saved in db
        em.detach(updatedClasses);
        updatedClasses
            .className(UPDATED_CLASS_NAME)
            .startDate(UPDATED_START_DATE)
            .endDate(UPDATED_END_DATE)
            .startEndTime(UPDATED_START_END_TIME);

        restClassesMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedClasses.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedClasses))
            )
            .andExpect(status().isOk());

        // Validate the Classes in the database
        List<Classes> classesList = classesRepository.findAll();
        assertThat(classesList).hasSize(databaseSizeBeforeUpdate);
        Classes testClasses = classesList.get(classesList.size() - 1);
        assertThat(testClasses.getClassName()).isEqualTo(UPDATED_CLASS_NAME);
        assertThat(testClasses.getStartDate()).isEqualTo(UPDATED_START_DATE);
        assertThat(testClasses.getEndDate()).isEqualTo(UPDATED_END_DATE);
        assertThat(testClasses.getStartEndTime()).isEqualTo(UPDATED_START_END_TIME);
    }

    @Test
    @Transactional
    void putNonExistingClasses() throws Exception {
        int databaseSizeBeforeUpdate = classesRepository.findAll().size();
        classes.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restClassesMockMvc
            .perform(
                put(ENTITY_API_URL_ID, classes.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(classes))
            )
            .andExpect(status().isBadRequest());

        // Validate the Classes in the database
        List<Classes> classesList = classesRepository.findAll();
        assertThat(classesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchClasses() throws Exception {
        int databaseSizeBeforeUpdate = classesRepository.findAll().size();
        classes.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restClassesMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(classes))
            )
            .andExpect(status().isBadRequest());

        // Validate the Classes in the database
        List<Classes> classesList = classesRepository.findAll();
        assertThat(classesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamClasses() throws Exception {
        int databaseSizeBeforeUpdate = classesRepository.findAll().size();
        classes.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restClassesMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(classes)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Classes in the database
        List<Classes> classesList = classesRepository.findAll();
        assertThat(classesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateClassesWithPatch() throws Exception {
        // Initialize the database
        classesRepository.saveAndFlush(classes);

        int databaseSizeBeforeUpdate = classesRepository.findAll().size();

        // Update the classes using partial update
        Classes partialUpdatedClasses = new Classes();
        partialUpdatedClasses.setId(classes.getId());

        partialUpdatedClasses.startDate(UPDATED_START_DATE);

        restClassesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedClasses.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedClasses))
            )
            .andExpect(status().isOk());

        // Validate the Classes in the database
        List<Classes> classesList = classesRepository.findAll();
        assertThat(classesList).hasSize(databaseSizeBeforeUpdate);
        Classes testClasses = classesList.get(classesList.size() - 1);
        assertThat(testClasses.getClassName()).isEqualTo(DEFAULT_CLASS_NAME);
        assertThat(testClasses.getStartDate()).isEqualTo(UPDATED_START_DATE);
        assertThat(testClasses.getEndDate()).isEqualTo(DEFAULT_END_DATE);
        assertThat(testClasses.getStartEndTime()).isEqualTo(DEFAULT_START_END_TIME);
    }

    @Test
    @Transactional
    void fullUpdateClassesWithPatch() throws Exception {
        // Initialize the database
        classesRepository.saveAndFlush(classes);

        int databaseSizeBeforeUpdate = classesRepository.findAll().size();

        // Update the classes using partial update
        Classes partialUpdatedClasses = new Classes();
        partialUpdatedClasses.setId(classes.getId());

        partialUpdatedClasses
            .className(UPDATED_CLASS_NAME)
            .startDate(UPDATED_START_DATE)
            .endDate(UPDATED_END_DATE)
            .startEndTime(UPDATED_START_END_TIME);

        restClassesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedClasses.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedClasses))
            )
            .andExpect(status().isOk());

        // Validate the Classes in the database
        List<Classes> classesList = classesRepository.findAll();
        assertThat(classesList).hasSize(databaseSizeBeforeUpdate);
        Classes testClasses = classesList.get(classesList.size() - 1);
        assertThat(testClasses.getClassName()).isEqualTo(UPDATED_CLASS_NAME);
        assertThat(testClasses.getStartDate()).isEqualTo(UPDATED_START_DATE);
        assertThat(testClasses.getEndDate()).isEqualTo(UPDATED_END_DATE);
        assertThat(testClasses.getStartEndTime()).isEqualTo(UPDATED_START_END_TIME);
    }

    @Test
    @Transactional
    void patchNonExistingClasses() throws Exception {
        int databaseSizeBeforeUpdate = classesRepository.findAll().size();
        classes.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restClassesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, classes.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(classes))
            )
            .andExpect(status().isBadRequest());

        // Validate the Classes in the database
        List<Classes> classesList = classesRepository.findAll();
        assertThat(classesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchClasses() throws Exception {
        int databaseSizeBeforeUpdate = classesRepository.findAll().size();
        classes.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restClassesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(classes))
            )
            .andExpect(status().isBadRequest());

        // Validate the Classes in the database
        List<Classes> classesList = classesRepository.findAll();
        assertThat(classesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamClasses() throws Exception {
        int databaseSizeBeforeUpdate = classesRepository.findAll().size();
        classes.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restClassesMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(classes)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Classes in the database
        List<Classes> classesList = classesRepository.findAll();
        assertThat(classesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteClasses() throws Exception {
        // Initialize the database
        classesRepository.saveAndFlush(classes);

        int databaseSizeBeforeDelete = classesRepository.findAll().size();

        // Delete the classes
        restClassesMockMvc
            .perform(delete(ENTITY_API_URL_ID, classes.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Classes> classesList = classesRepository.findAll();
        assertThat(classesList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
