package com.datnguyen.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.datnguyen.IntegrationTest;
import com.datnguyen.domain.SubjectResults;
import com.datnguyen.repository.SubjectResultsRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link SubjectResultsResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class SubjectResultsResourceIT {

    private static final Boolean DEFAULT_IS_PASS = false;
    private static final Boolean UPDATED_IS_PASS = true;

    private static final String ENTITY_API_URL = "/api/subject-results";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private SubjectResultsRepository subjectResultsRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restSubjectResultsMockMvc;

    private SubjectResults subjectResults;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SubjectResults createEntity(EntityManager em) {
        SubjectResults subjectResults = new SubjectResults().isPass(DEFAULT_IS_PASS);
        return subjectResults;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SubjectResults createUpdatedEntity(EntityManager em) {
        SubjectResults subjectResults = new SubjectResults().isPass(UPDATED_IS_PASS);
        return subjectResults;
    }

    @BeforeEach
    public void initTest() {
        subjectResults = createEntity(em);
    }

    @Test
    @Transactional
    void createSubjectResults() throws Exception {
        int databaseSizeBeforeCreate = subjectResultsRepository.findAll().size();
        // Create the SubjectResults
        restSubjectResultsMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(subjectResults))
            )
            .andExpect(status().isCreated());

        // Validate the SubjectResults in the database
        List<SubjectResults> subjectResultsList = subjectResultsRepository.findAll();
        assertThat(subjectResultsList).hasSize(databaseSizeBeforeCreate + 1);
        SubjectResults testSubjectResults = subjectResultsList.get(subjectResultsList.size() - 1);
        assertThat(testSubjectResults.getIsPass()).isEqualTo(DEFAULT_IS_PASS);
    }

    @Test
    @Transactional
    void createSubjectResultsWithExistingId() throws Exception {
        // Create the SubjectResults with an existing ID
        subjectResults.setId(1L);

        int databaseSizeBeforeCreate = subjectResultsRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restSubjectResultsMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(subjectResults))
            )
            .andExpect(status().isBadRequest());

        // Validate the SubjectResults in the database
        List<SubjectResults> subjectResultsList = subjectResultsRepository.findAll();
        assertThat(subjectResultsList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllSubjectResults() throws Exception {
        // Initialize the database
        subjectResultsRepository.saveAndFlush(subjectResults);

        // Get all the subjectResultsList
        restSubjectResultsMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(subjectResults.getId().intValue())))
            .andExpect(jsonPath("$.[*].isPass").value(hasItem(DEFAULT_IS_PASS.booleanValue())));
    }

    @Test
    @Transactional
    void getSubjectResults() throws Exception {
        // Initialize the database
        subjectResultsRepository.saveAndFlush(subjectResults);

        // Get the subjectResults
        restSubjectResultsMockMvc
            .perform(get(ENTITY_API_URL_ID, subjectResults.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(subjectResults.getId().intValue()))
            .andExpect(jsonPath("$.isPass").value(DEFAULT_IS_PASS.booleanValue()));
    }

    @Test
    @Transactional
    void getNonExistingSubjectResults() throws Exception {
        // Get the subjectResults
        restSubjectResultsMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewSubjectResults() throws Exception {
        // Initialize the database
        subjectResultsRepository.saveAndFlush(subjectResults);

        int databaseSizeBeforeUpdate = subjectResultsRepository.findAll().size();

        // Update the subjectResults
        SubjectResults updatedSubjectResults = subjectResultsRepository.findById(subjectResults.getId()).get();
        // Disconnect from session so that the updates on updatedSubjectResults are not directly saved in db
        em.detach(updatedSubjectResults);
        updatedSubjectResults.isPass(UPDATED_IS_PASS);

        restSubjectResultsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedSubjectResults.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedSubjectResults))
            )
            .andExpect(status().isOk());

        // Validate the SubjectResults in the database
        List<SubjectResults> subjectResultsList = subjectResultsRepository.findAll();
        assertThat(subjectResultsList).hasSize(databaseSizeBeforeUpdate);
        SubjectResults testSubjectResults = subjectResultsList.get(subjectResultsList.size() - 1);
        assertThat(testSubjectResults.getIsPass()).isEqualTo(UPDATED_IS_PASS);
    }

    @Test
    @Transactional
    void putNonExistingSubjectResults() throws Exception {
        int databaseSizeBeforeUpdate = subjectResultsRepository.findAll().size();
        subjectResults.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSubjectResultsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, subjectResults.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(subjectResults))
            )
            .andExpect(status().isBadRequest());

        // Validate the SubjectResults in the database
        List<SubjectResults> subjectResultsList = subjectResultsRepository.findAll();
        assertThat(subjectResultsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchSubjectResults() throws Exception {
        int databaseSizeBeforeUpdate = subjectResultsRepository.findAll().size();
        subjectResults.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSubjectResultsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(subjectResults))
            )
            .andExpect(status().isBadRequest());

        // Validate the SubjectResults in the database
        List<SubjectResults> subjectResultsList = subjectResultsRepository.findAll();
        assertThat(subjectResultsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamSubjectResults() throws Exception {
        int databaseSizeBeforeUpdate = subjectResultsRepository.findAll().size();
        subjectResults.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSubjectResultsMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(subjectResults)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the SubjectResults in the database
        List<SubjectResults> subjectResultsList = subjectResultsRepository.findAll();
        assertThat(subjectResultsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateSubjectResultsWithPatch() throws Exception {
        // Initialize the database
        subjectResultsRepository.saveAndFlush(subjectResults);

        int databaseSizeBeforeUpdate = subjectResultsRepository.findAll().size();

        // Update the subjectResults using partial update
        SubjectResults partialUpdatedSubjectResults = new SubjectResults();
        partialUpdatedSubjectResults.setId(subjectResults.getId());

        restSubjectResultsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedSubjectResults.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedSubjectResults))
            )
            .andExpect(status().isOk());

        // Validate the SubjectResults in the database
        List<SubjectResults> subjectResultsList = subjectResultsRepository.findAll();
        assertThat(subjectResultsList).hasSize(databaseSizeBeforeUpdate);
        SubjectResults testSubjectResults = subjectResultsList.get(subjectResultsList.size() - 1);
        assertThat(testSubjectResults.getIsPass()).isEqualTo(DEFAULT_IS_PASS);
    }

    @Test
    @Transactional
    void fullUpdateSubjectResultsWithPatch() throws Exception {
        // Initialize the database
        subjectResultsRepository.saveAndFlush(subjectResults);

        int databaseSizeBeforeUpdate = subjectResultsRepository.findAll().size();

        // Update the subjectResults using partial update
        SubjectResults partialUpdatedSubjectResults = new SubjectResults();
        partialUpdatedSubjectResults.setId(subjectResults.getId());

        partialUpdatedSubjectResults.isPass(UPDATED_IS_PASS);

        restSubjectResultsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedSubjectResults.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedSubjectResults))
            )
            .andExpect(status().isOk());

        // Validate the SubjectResults in the database
        List<SubjectResults> subjectResultsList = subjectResultsRepository.findAll();
        assertThat(subjectResultsList).hasSize(databaseSizeBeforeUpdate);
        SubjectResults testSubjectResults = subjectResultsList.get(subjectResultsList.size() - 1);
        assertThat(testSubjectResults.getIsPass()).isEqualTo(UPDATED_IS_PASS);
    }

    @Test
    @Transactional
    void patchNonExistingSubjectResults() throws Exception {
        int databaseSizeBeforeUpdate = subjectResultsRepository.findAll().size();
        subjectResults.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSubjectResultsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, subjectResults.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(subjectResults))
            )
            .andExpect(status().isBadRequest());

        // Validate the SubjectResults in the database
        List<SubjectResults> subjectResultsList = subjectResultsRepository.findAll();
        assertThat(subjectResultsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchSubjectResults() throws Exception {
        int databaseSizeBeforeUpdate = subjectResultsRepository.findAll().size();
        subjectResults.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSubjectResultsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(subjectResults))
            )
            .andExpect(status().isBadRequest());

        // Validate the SubjectResults in the database
        List<SubjectResults> subjectResultsList = subjectResultsRepository.findAll();
        assertThat(subjectResultsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamSubjectResults() throws Exception {
        int databaseSizeBeforeUpdate = subjectResultsRepository.findAll().size();
        subjectResults.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSubjectResultsMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(subjectResults))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the SubjectResults in the database
        List<SubjectResults> subjectResultsList = subjectResultsRepository.findAll();
        assertThat(subjectResultsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteSubjectResults() throws Exception {
        // Initialize the database
        subjectResultsRepository.saveAndFlush(subjectResults);

        int databaseSizeBeforeDelete = subjectResultsRepository.findAll().size();

        // Delete the subjectResults
        restSubjectResultsMockMvc
            .perform(delete(ENTITY_API_URL_ID, subjectResults.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<SubjectResults> subjectResultsList = subjectResultsRepository.findAll();
        assertThat(subjectResultsList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
