package com.datnguyen.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.datnguyen.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class PositionSalaryTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PositionSalary.class);
        PositionSalary positionSalary1 = new PositionSalary();
        positionSalary1.setId(1L);
        PositionSalary positionSalary2 = new PositionSalary();
        positionSalary2.setId(positionSalary1.getId());
        assertThat(positionSalary1).isEqualTo(positionSalary2);
        positionSalary2.setId(2L);
        assertThat(positionSalary1).isNotEqualTo(positionSalary2);
        positionSalary1.setId(null);
        assertThat(positionSalary1).isNotEqualTo(positionSalary2);
    }
}
