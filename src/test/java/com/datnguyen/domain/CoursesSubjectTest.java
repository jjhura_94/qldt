package com.datnguyen.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.datnguyen.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class CoursesSubjectTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CoursesSubject.class);
        CoursesSubject coursesSubject1 = new CoursesSubject();
        coursesSubject1.setId(1L);
        CoursesSubject coursesSubject2 = new CoursesSubject();
        coursesSubject2.setId(coursesSubject1.getId());
        assertThat(coursesSubject1).isEqualTo(coursesSubject2);
        coursesSubject2.setId(2L);
        assertThat(coursesSubject1).isNotEqualTo(coursesSubject2);
        coursesSubject1.setId(null);
        assertThat(coursesSubject1).isNotEqualTo(coursesSubject2);
    }
}
