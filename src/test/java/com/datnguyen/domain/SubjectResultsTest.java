package com.datnguyen.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.datnguyen.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class SubjectResultsTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SubjectResults.class);
        SubjectResults subjectResults1 = new SubjectResults();
        subjectResults1.setId(1L);
        SubjectResults subjectResults2 = new SubjectResults();
        subjectResults2.setId(subjectResults1.getId());
        assertThat(subjectResults1).isEqualTo(subjectResults2);
        subjectResults2.setId(2L);
        assertThat(subjectResults1).isNotEqualTo(subjectResults2);
        subjectResults1.setId(null);
        assertThat(subjectResults1).isNotEqualTo(subjectResults2);
    }
}
