package com.datnguyen.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.datnguyen.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ClassRoomsTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ClassRooms.class);
        ClassRooms classRooms1 = new ClassRooms();
        classRooms1.setId(1L);
        ClassRooms classRooms2 = new ClassRooms();
        classRooms2.setId(classRooms1.getId());
        assertThat(classRooms1).isEqualTo(classRooms2);
        classRooms2.setId(2L);
        assertThat(classRooms1).isNotEqualTo(classRooms2);
        classRooms1.setId(null);
        assertThat(classRooms1).isNotEqualTo(classRooms2);
    }
}
